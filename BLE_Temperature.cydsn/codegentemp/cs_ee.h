/*******************************************************************************
* File Name: cs_ee.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_cs_ee_H) /* Pins cs_ee_H */
#define CY_PINS_cs_ee_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cs_ee_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} cs_ee_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   cs_ee_Read(void);
void    cs_ee_Write(uint8 value);
uint8   cs_ee_ReadDataReg(void);
#if defined(cs_ee__PC) || (CY_PSOC4_4200L) 
    void    cs_ee_SetDriveMode(uint8 mode);
#endif
void    cs_ee_SetInterruptMode(uint16 position, uint16 mode);
uint8   cs_ee_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void cs_ee_Sleep(void); 
void cs_ee_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(cs_ee__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define cs_ee_DRIVE_MODE_BITS        (3)
    #define cs_ee_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - cs_ee_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the cs_ee_SetDriveMode() function.
         *  @{
         */
        #define cs_ee_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define cs_ee_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define cs_ee_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define cs_ee_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define cs_ee_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define cs_ee_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define cs_ee_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define cs_ee_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define cs_ee_MASK               cs_ee__MASK
#define cs_ee_SHIFT              cs_ee__SHIFT
#define cs_ee_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in cs_ee_SetInterruptMode() function.
     *  @{
     */
        #define cs_ee_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define cs_ee_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define cs_ee_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define cs_ee_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(cs_ee__SIO)
    #define cs_ee_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(cs_ee__PC) && (CY_PSOC4_4200L)
    #define cs_ee_USBIO_ENABLE               ((uint32)0x80000000u)
    #define cs_ee_USBIO_DISABLE              ((uint32)(~cs_ee_USBIO_ENABLE))
    #define cs_ee_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define cs_ee_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define cs_ee_USBIO_ENTER_SLEEP          ((uint32)((1u << cs_ee_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << cs_ee_USBIO_SUSPEND_DEL_SHIFT)))
    #define cs_ee_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << cs_ee_USBIO_SUSPEND_SHIFT)))
    #define cs_ee_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << cs_ee_USBIO_SUSPEND_DEL_SHIFT)))
    #define cs_ee_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(cs_ee__PC)
    /* Port Configuration */
    #define cs_ee_PC                 (* (reg32 *) cs_ee__PC)
#endif
/* Pin State */
#define cs_ee_PS                     (* (reg32 *) cs_ee__PS)
/* Data Register */
#define cs_ee_DR                     (* (reg32 *) cs_ee__DR)
/* Input Buffer Disable Override */
#define cs_ee_INP_DIS                (* (reg32 *) cs_ee__PC2)

/* Interrupt configuration Registers */
#define cs_ee_INTCFG                 (* (reg32 *) cs_ee__INTCFG)
#define cs_ee_INTSTAT                (* (reg32 *) cs_ee__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define cs_ee_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(cs_ee__SIO)
    #define cs_ee_SIO_REG            (* (reg32 *) cs_ee__SIO)
#endif /* (cs_ee__SIO_CFG) */

/* USBIO registers */
#if !defined(cs_ee__PC) && (CY_PSOC4_4200L)
    #define cs_ee_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define cs_ee_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define cs_ee_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define cs_ee_DRIVE_MODE_SHIFT       (0x00u)
#define cs_ee_DRIVE_MODE_MASK        (0x07u << cs_ee_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins cs_ee_H */


/* [] END OF FILE */
