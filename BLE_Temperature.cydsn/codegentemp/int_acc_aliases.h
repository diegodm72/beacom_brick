/*******************************************************************************
* File Name: int_acc.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_int_acc_ALIASES_H) /* Pins int_acc_ALIASES_H */
#define CY_PINS_int_acc_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define int_acc_0			(int_acc__0__PC)
#define int_acc_0_PS		(int_acc__0__PS)
#define int_acc_0_PC		(int_acc__0__PC)
#define int_acc_0_DR		(int_acc__0__DR)
#define int_acc_0_SHIFT	(int_acc__0__SHIFT)
#define int_acc_0_INTR	((uint16)((uint16)0x0003u << (int_acc__0__SHIFT*2u)))

#define int_acc_INTR_ALL	 ((uint16)(int_acc_0_INTR))


#endif /* End Pins int_acc_ALIASES_H */


/* [] END OF FILE */
