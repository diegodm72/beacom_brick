/*******************************************************************************
* File Name: out_releay.c  
* Version 2.20
*
* Description:
*  This file contains APIs to set up the Pins component for low power modes.
*
* Note:
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "out_releay.h"

static out_releay_BACKUP_STRUCT  out_releay_backup = {0u, 0u, 0u};


/*******************************************************************************
* Function Name: out_releay_Sleep
****************************************************************************//**
*
* \brief Stores the pin configuration and prepares the pin for entering chip 
*  deep-sleep/hibernate modes. This function applies only to SIO and USBIO pins.
*  It should not be called for GPIO or GPIO_OVT pins.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None 
*  
* \sideeffect
*  For SIO pins, this function configures the pin input threshold to CMOS and
*  drive level to Vddio. This is needed for SIO pins when in device 
*  deep-sleep/hibernate modes.
*
* \funcusage
*  \snippet out_releay_SUT.c usage_out_releay_Sleep_Wakeup
*******************************************************************************/
void out_releay_Sleep(void)
{
    #if defined(out_releay__PC)
        out_releay_backup.pcState = out_releay_PC;
    #else
        #if (CY_PSOC4_4200L)
            /* Save the regulator state and put the PHY into suspend mode */
            out_releay_backup.usbState = out_releay_CR1_REG;
            out_releay_USB_POWER_REG |= out_releay_USBIO_ENTER_SLEEP;
            out_releay_CR1_REG &= out_releay_USBIO_CR1_OFF;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(out_releay__SIO)
        out_releay_backup.sioState = out_releay_SIO_REG;
        /* SIO requires unregulated output buffer and single ended input buffer */
        out_releay_SIO_REG &= (uint32)(~out_releay_SIO_LPM_MASK);
    #endif  
}


/*******************************************************************************
* Function Name: out_releay_Wakeup
****************************************************************************//**
*
* \brief Restores the pin configuration that was saved during Pin_Sleep(). This 
* function applies only to SIO and USBIO pins. It should not be called for
* GPIO or GPIO_OVT pins.
*
* For USBIO pins, the wakeup is only triggered for falling edge interrupts.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None
*  
* \funcusage
*  Refer to out_releay_Sleep() for an example usage.
*******************************************************************************/
void out_releay_Wakeup(void)
{
    #if defined(out_releay__PC)
        out_releay_PC = out_releay_backup.pcState;
    #else
        #if (CY_PSOC4_4200L)
            /* Restore the regulator state and come out of suspend mode */
            out_releay_USB_POWER_REG &= out_releay_USBIO_EXIT_SLEEP_PH1;
            out_releay_CR1_REG = out_releay_backup.usbState;
            out_releay_USB_POWER_REG &= out_releay_USBIO_EXIT_SLEEP_PH2;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(out_releay__SIO)
        out_releay_SIO_REG = out_releay_backup.sioState;
    #endif
}


/* [] END OF FILE */
