/*******************************************************************************
* File Name: Vt.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Vt_H) /* Pins Vt_H */
#define CY_PINS_Vt_H

#include "cytypes.h"
#include "cyfitter.h"
#include "Vt_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} Vt_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   Vt_Read(void);
void    Vt_Write(uint8 value);
uint8   Vt_ReadDataReg(void);
#if defined(Vt__PC) || (CY_PSOC4_4200L) 
    void    Vt_SetDriveMode(uint8 mode);
#endif
void    Vt_SetInterruptMode(uint16 position, uint16 mode);
uint8   Vt_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void Vt_Sleep(void); 
void Vt_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(Vt__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define Vt_DRIVE_MODE_BITS        (3)
    #define Vt_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - Vt_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the Vt_SetDriveMode() function.
         *  @{
         */
        #define Vt_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define Vt_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define Vt_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define Vt_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define Vt_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define Vt_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define Vt_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define Vt_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define Vt_MASK               Vt__MASK
#define Vt_SHIFT              Vt__SHIFT
#define Vt_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in Vt_SetInterruptMode() function.
     *  @{
     */
        #define Vt_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define Vt_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define Vt_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define Vt_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(Vt__SIO)
    #define Vt_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(Vt__PC) && (CY_PSOC4_4200L)
    #define Vt_USBIO_ENABLE               ((uint32)0x80000000u)
    #define Vt_USBIO_DISABLE              ((uint32)(~Vt_USBIO_ENABLE))
    #define Vt_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define Vt_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define Vt_USBIO_ENTER_SLEEP          ((uint32)((1u << Vt_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << Vt_USBIO_SUSPEND_DEL_SHIFT)))
    #define Vt_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << Vt_USBIO_SUSPEND_SHIFT)))
    #define Vt_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << Vt_USBIO_SUSPEND_DEL_SHIFT)))
    #define Vt_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(Vt__PC)
    /* Port Configuration */
    #define Vt_PC                 (* (reg32 *) Vt__PC)
#endif
/* Pin State */
#define Vt_PS                     (* (reg32 *) Vt__PS)
/* Data Register */
#define Vt_DR                     (* (reg32 *) Vt__DR)
/* Input Buffer Disable Override */
#define Vt_INP_DIS                (* (reg32 *) Vt__PC2)

/* Interrupt configuration Registers */
#define Vt_INTCFG                 (* (reg32 *) Vt__INTCFG)
#define Vt_INTSTAT                (* (reg32 *) Vt__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define Vt_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(Vt__SIO)
    #define Vt_SIO_REG            (* (reg32 *) Vt__SIO)
#endif /* (Vt__SIO_CFG) */

/* USBIO registers */
#if !defined(Vt__PC) && (CY_PSOC4_4200L)
    #define Vt_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define Vt_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define Vt_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define Vt_DRIVE_MODE_SHIFT       (0x00u)
#define Vt_DRIVE_MODE_MASK        (0x07u << Vt_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins Vt_H */


/* [] END OF FILE */
