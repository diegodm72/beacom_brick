/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "stdint.h"


#define STATUS_REG_AUX  0x07
#define OUT_TEMP_L      0x0c
#define OUT_TEMP_H      0x0d
#define WHO_AM_I        0x0f
#define CTRL_REG0       0x1e
#define TEMP_CFG_REG    0x1f
#define CTRL_REG1       0x20
#define CTRL_REG2       0x21
#define CTRL_REG3       0x22
#define CTRL_REG4       0x23
#define CTRL_REG5       0x24
#define CTRL_REG6       0x25
#define REFERENCE       0x26
#define STATUS_REG      0x27
#define OUT_X_L         0x28
#define OUT_X_H         0x29
#define OUT_Y_L         0x2a
#define OUT_Y_H         0x2b
#define OUT_Z_L         0x2c
#define OUT_Z_H         0x2d
#define FIFO_CTRL_REG   0x2e
#define FIFO_SRC_REG    0x2f
#define INT1_CFG        0x30
#define INT1_SRC        0x31
#define INT1_THS        0x32
#define INT1_DURATION   0x33
#define INT2_CFG        0x34
#define INT2_SRC        0x35
#define INT2_THS        0x36
#define INT2_DURATION   0x37
#define CLICK_CFG       0x38
#define CLICK_SRC       0x39
#define CLICK_THS       0x3a
#define TIME_LIMIT      0x3b
#define TIME_LATENCY    0x3c
#define TIME_WINDOW     0x3d
#define ACT_THS         0x3e
#define ACT_DUR         0x3f

uint8_t lis2dh12_init();
uint8_t lis2dh12_read_reg( unsigned char reg );
uint8_t lis2dh12_write_reg( unsigned char reg , unsigned char val );
uint8_t lis2dh12_read_all();
uint8_t lis2dh12_read_temp();

void lis2dh12_power_on();
void lis2dh12_power_down();

typedef struct
{
    int16_t axis_x;
    int16_t axis_y;
    int16_t axis_z;
    
    int16_t temp;
}mems_s;


typedef struct
{
    unsigned nc2 :2;
    unsigned tda :1;    // Temperature new data
    unsigned nc3 :3;
    unsigned tor :1;    // Temperature data overrun
    unsigned nc1 :1;    
}status_reg_aux_s;

typedef union 
{
    status_reg_aux_s b;
    uint8_t val;
}status_reg_aux_u;

typedef struct
{
    unsigned xen  :1;
    unsigned yen  :1;
    unsigned zen  :1;
    unsigned lpen :1;
    unsigned odr  :4;
}ctrl_reg1_s;

typedef union
{
    ctrl_reg1_s b;
    uint8_t val;
}ctrl_reg1_u;

typedef struct
{
    unsigned hp_ia1   :1;
    unsigned hp_ia2   :1;
    unsigned hp_click :1;
    unsigned fds      :1;
    unsigned hpcf     :2;
    unsigned hpm      :2;
}ctrl_reg2_s;

typedef union
{
    ctrl_reg2_s b;
    uint8_t val;
}ctrl_reg2_u;

typedef struct
{
    unsigned nc1        :1;
    unsigned i1_overrun :1;
    unsigned i1_wtm     :1;
    unsigned nc0        :1;
    unsigned i1_zyxda   :1;
    unsigned i1_ia2     :1;
    unsigned i1_ia1     :1;
    unsigned i1_click   :1;
}ctrl_reg3_s;

typedef union
{
    ctrl_reg3_s b;
    uint8_t val;
}ctrl_reg3_u;

typedef struct
{
    unsigned sim :1;
    unsigned st0 :1;
    unsigned st1 :1;
    unsigned hr  :1;
    unsigned fs  :2;
    unsigned ble :1;
    unsigned bdu :1;    
}ctrl_reg4_s;

typedef union
{
    ctrl_reg4_s b;
    uint8_t val;
}ctrl_reg4_u;

typedef struct
{
    unsigned d4d_int2 :1;
    unsigned lir_int2 :1;
    unsigned d4d_int1 :1;
    unsigned lir_int1 :1;
    unsigned nc       :2;
    unsigned fifo_en  :1;
    unsigned boot     :1;
}ctrl_reg5_s;

typedef union
{
    ctrl_reg5_s b;
    uint8_t val;
}ctrl_reg5_u;

typedef struct
{
    unsigned nc1          :1;
    unsigned int_polarity :1;
    unsigned nc2          :1;
    unsigned i2_act       :1;
    unsigned i2_boot      :1;
    unsigned i2_ia2       :1;
    unsigned i2_ia1       :1;
    unsigned i2_click     :1;
}ctrl_reg6_s;

typedef union
{
    ctrl_reg6_s b;
    uint8_t val;
}ctrl_reg6_u;

typedef struct
{
    unsigned xda :1;
    unsigned yda :1;
    unsigned zda :1;
    unsigned xyzda :1;
    unsigned xor :1;
    unsigned yor :1;
    unsigned zor :1;
    unsigned xyzor :1;
}status_reg_s;

typedef union
{
    status_reg_s b;
    uint8_t val;
}status_reg_u;

typedef struct
{
    unsigned xlie :1;
    unsigned xhie :1;
    unsigned ylie :1;
    unsigned yhie :1;
    unsigned zlie :1;
    unsigned zhie :1;
    unsigned _6d  :1;
    unsigned aoi  :1;    
}int1_cfg_s;

typedef union
{
    int1_cfg_s b;
    uint8_t val;
}int1_cfg_u;

typedef struct
{
    unsigned xl :1;
    unsigned xh :1;
    unsigned yl :1;
    unsigned yh :1;
    unsigned zl :1;
    unsigned zh :1;
    unsigned ia :1;
    unsigned nc :1;
}int_src_s;

typedef union
{
    int_src_s b;
    uint8_t val;
}int_src_u;

typedef struct
{
    unsigned xs :1;
    unsigned xd :1;
    unsigned ys :1;
    unsigned yd :1;
    unsigned zs :1;
    unsigned zd :1;
    unsigned nc :2;
}click_cfg_s;

typedef union
{
    click_cfg_s b;
    uint8_t val;
}click_cfg_u;

typedef struct
{
    unsigned x      :1;
    unsigned y      :1;
    unsigned z      :1;
    unsigned sign   :1;
    unsigned sclick :1;
    unsigned dclick :1;
    unsigned ia     :1;
    unsigned nc     :1;
}click_src_s;

typedef union
{
    click_src_s b;
    uint8_t val;
}click_src_u;



/* [] END OF FILE */
