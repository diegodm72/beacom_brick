/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* [] END OF FILE */

#include "m95020.h"

#include "SPIM.h"
#include "SPIM_SPI_UART.h"
#include "SPIM_PINS.h"
#include "SPIM_SPI_UART_PVT.h"
#include "SPIM_PVT.h"
#include "SPIM_BOOT.h"

#include "cs_ee.h"

void    fram_write_enable();
uint8_t fram_write_isenable();
uint8_t fram_read_statusreg();

statusreg_u status_reg;

uint8_t arr[100];

uint8_t fram_init()
{
    
    uint8_t test = 0xaa;
    
    fram_write_byte(0,test);
    
    test = 0;
    
    fram_read_byte(0,&test);

    
//    for( test = 0; test < 100; test++)
//    {
//        if( (test %2) == 0 )
//            arr[test] = 0x55;    
//        else arr[test] = 0xaa;    
//    }
//    
//    fram_write_array( 1, arr , 100 );
//    
//    for( test = 0; test < 100; test++)
//    {
//        arr[test] = 0;    
//    }
//
//    fram_read_array(1,arr,100);
    
    if( test == 0xAA ) return 1;
    else               return 0;
    
}


void fram_write_byte( uint8_t address, uint8_t val )
{
    uint8_t buf[3];
    fram_write_enable();

    buf[0] = WRITE;
    buf[1] = address;
    buf[2] = val;
    
    SPIM_SpiUartClearRxBuffer();
    
    cs_ee_Write(0);
    
    CyDelayUs(5);
    
    SPIM_SpiUartPutArray(buf,3);
        
    while (SPIM_SpiUartGetRxBufferSize() != 3 )
    {
    }

    cs_ee_Write(1);
    
    SPIM_SpiUartClearTxBuffer();
    
    do 
    {
        fram_read_statusreg();
        CyDelay(1);
    }while( status_reg.b.WIP );

}

void fram_read_byte( uint8_t address, uint8_t* val )
{
    uint8_t buf[3];
    fram_write_enable();

    buf[0] = READ;
    buf[1] = address;
    buf[2] = 0;
    
    SPIM_SpiUartClearRxBuffer();
    
    cs_ee_Write(0);
    
    CyDelayUs(5);
    
    SPIM_SpiUartPutArray(buf,3);
        
    while (SPIM_SpiUartGetRxBufferSize() != 3 )
    {
    }
    
    cs_ee_Write(1);

    buf[0] = SPIM_SpiUartReadRxData();
    buf[1] = SPIM_SpiUartReadRxData();

    *val = SPIM_SpiUartReadRxData();
    
    SPIM_SpiUartClearTxBuffer();
    
}

uint8_t fram_write_array( uint8_t address, uint8_t* array, uint8_t len)
{
    uint8_t buf[2];
    
    if( ((uint16_t)address + (uint16_t)len) > 254 ) return 0;
    
//    fram_write_enable();
//    
//    buf[0] = WRITE;
//    buf[1] = address;
//    
//    SPIM_SpiUartClearRxBuffer();
    
    while(len)
    {
        fram_write_byte(address,*array);
        ++address;
        ++array;
        --len;        
    }
    
//    cs_ee_Write(0);
//    
//    CyDelayUs(5);
//    
//    SPIM_SpiUartPutArray( buf, 2);  
//    SPIM_SpiUartPutArray( array, len);
//
//    cs_ee_Write(1);
//
//    SPIM_SpiUartClearTxBuffer();
    
//    do 
//    {
//        fram_read_statusreg();
//        CyDelay(1);
//    }while( status_reg.b.WIP );
    
    return 1;
    
}

uint8_t fram_read_array( uint8_t address, uint8_t* array, uint8_t len)
{
    uint8_t buf[2];
    
    if( ((uint16_t)address + (uint16_t)len) > 254 ) return 0;
    
    while(len)
    {
        fram_read_byte(address,array);
        ++address;
        ++array;
        --len;    
    }    
    return 1;
}

void fram_write_enable()
{
    uint8_t reg = WREN;    
    
    SPIM_SpiUartClearRxBuffer();
    
    cs_ee_Write(0);
    
    CyDelayUs(5);
    
    SPIM_SpiUartWriteTxData( (uint32_t)reg );
    
    CyDelayUs(5);

    cs_ee_Write(1);
}

uint8_t fram_write_isenable()
{
    fram_read_statusreg();
    
    return status_reg.b.WEL;
}

uint8_t fram_write_inprogress()
{
    fram_read_statusreg();
    
    return status_reg.b.WIP;    
}

uint8_t fram_read_statusreg()
{
    uint8_t buf[2];
    buf[0] = RDSR;    
    
    SPIM_SpiUartClearRxBuffer();
    
    cs_ee_Write(0);
    
    CyDelayUs(5);
    
    SPIM_SpiUartPutArray(buf,2);
        
    while (SPIM_SpiUartGetRxBufferSize() != 2 )
    {
    }

    SPIM_SpiUartClearTxBuffer();
    
    buf[0] = SPIM_SpiUartReadRxData();
    buf[1] = SPIM_SpiUartReadRxData();
    
    cs_ee_Write(1);

    SPIM_SpiUartClearTxBuffer();
    
    status_reg.val = buf[1];
    
    return 1;
    
}



