/***************************************************************************//**
* \file scb_PINS.h
* \version 4.0
*
* \brief
*  This file provides constants and parameter values for the pin components
*  buried into SCB Component.
*
* Note:
*
********************************************************************************
* \copyright
* Copyright 2013-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SCB_PINS_scb_H)
#define CY_SCB_PINS_scb_H

#include "cydevice_trm.h"
#include "cyfitter.h"
#include "cytypes.h"


/***************************************
*   Conditional Compilation Parameters
****************************************/

/* Unconfigured pins */
#define scb_REMOVE_RX_WAKE_SDA_MOSI_PIN  (1u)
#define scb_REMOVE_RX_SDA_MOSI_PIN      (1u)
#define scb_REMOVE_TX_SCL_MISO_PIN      (1u)
#define scb_REMOVE_CTS_SCLK_PIN      (1u)
#define scb_REMOVE_RTS_SS0_PIN      (1u)
#define scb_REMOVE_SS1_PIN                 (1u)
#define scb_REMOVE_SS2_PIN                 (1u)
#define scb_REMOVE_SS3_PIN                 (1u)

/* Mode defined pins */
#define scb_REMOVE_I2C_PINS                (1u)
#define scb_REMOVE_SPI_MASTER_PINS         (0u)
#define scb_REMOVE_SPI_MASTER_SCLK_PIN     (0u)
#define scb_REMOVE_SPI_MASTER_MOSI_PIN     (0u)
#define scb_REMOVE_SPI_MASTER_MISO_PIN     (0u)
#define scb_REMOVE_SPI_MASTER_SS0_PIN      (1u)
#define scb_REMOVE_SPI_MASTER_SS1_PIN      (1u)
#define scb_REMOVE_SPI_MASTER_SS2_PIN      (1u)
#define scb_REMOVE_SPI_MASTER_SS3_PIN      (1u)
#define scb_REMOVE_SPI_SLAVE_PINS          (1u)
#define scb_REMOVE_SPI_SLAVE_MOSI_PIN      (1u)
#define scb_REMOVE_SPI_SLAVE_MISO_PIN      (1u)
#define scb_REMOVE_UART_TX_PIN             (1u)
#define scb_REMOVE_UART_RX_TX_PIN          (1u)
#define scb_REMOVE_UART_RX_PIN             (1u)
#define scb_REMOVE_UART_RX_WAKE_PIN        (1u)
#define scb_REMOVE_UART_RTS_PIN            (1u)
#define scb_REMOVE_UART_CTS_PIN            (1u)

/* Unconfigured pins */
#define scb_RX_WAKE_SDA_MOSI_PIN (0u == scb_REMOVE_RX_WAKE_SDA_MOSI_PIN)
#define scb_RX_SDA_MOSI_PIN     (0u == scb_REMOVE_RX_SDA_MOSI_PIN)
#define scb_TX_SCL_MISO_PIN     (0u == scb_REMOVE_TX_SCL_MISO_PIN)
#define scb_CTS_SCLK_PIN     (0u == scb_REMOVE_CTS_SCLK_PIN)
#define scb_RTS_SS0_PIN     (0u == scb_REMOVE_RTS_SS0_PIN)
#define scb_SS1_PIN                (0u == scb_REMOVE_SS1_PIN)
#define scb_SS2_PIN                (0u == scb_REMOVE_SS2_PIN)
#define scb_SS3_PIN                (0u == scb_REMOVE_SS3_PIN)

/* Mode defined pins */
#define scb_I2C_PINS               (0u == scb_REMOVE_I2C_PINS)
#define scb_SPI_MASTER_PINS        (0u == scb_REMOVE_SPI_MASTER_PINS)
#define scb_SPI_MASTER_SCLK_PIN    (0u == scb_REMOVE_SPI_MASTER_SCLK_PIN)
#define scb_SPI_MASTER_MOSI_PIN    (0u == scb_REMOVE_SPI_MASTER_MOSI_PIN)
#define scb_SPI_MASTER_MISO_PIN    (0u == scb_REMOVE_SPI_MASTER_MISO_PIN)
#define scb_SPI_MASTER_SS0_PIN     (0u == scb_REMOVE_SPI_MASTER_SS0_PIN)
#define scb_SPI_MASTER_SS1_PIN     (0u == scb_REMOVE_SPI_MASTER_SS1_PIN)
#define scb_SPI_MASTER_SS2_PIN     (0u == scb_REMOVE_SPI_MASTER_SS2_PIN)
#define scb_SPI_MASTER_SS3_PIN     (0u == scb_REMOVE_SPI_MASTER_SS3_PIN)
#define scb_SPI_SLAVE_PINS         (0u == scb_REMOVE_SPI_SLAVE_PINS)
#define scb_SPI_SLAVE_MOSI_PIN     (0u == scb_REMOVE_SPI_SLAVE_MOSI_PIN)
#define scb_SPI_SLAVE_MISO_PIN     (0u == scb_REMOVE_SPI_SLAVE_MISO_PIN)
#define scb_UART_TX_PIN            (0u == scb_REMOVE_UART_TX_PIN)
#define scb_UART_RX_TX_PIN         (0u == scb_REMOVE_UART_RX_TX_PIN)
#define scb_UART_RX_PIN            (0u == scb_REMOVE_UART_RX_PIN)
#define scb_UART_RX_WAKE_PIN       (0u == scb_REMOVE_UART_RX_WAKE_PIN)
#define scb_UART_RTS_PIN           (0u == scb_REMOVE_UART_RTS_PIN)
#define scb_UART_CTS_PIN           (0u == scb_REMOVE_UART_CTS_PIN)


/***************************************
*             Includes
****************************************/

#if (scb_RX_WAKE_SDA_MOSI_PIN)
    #include "scb_uart_rx_wake_i2c_sda_spi_mosi.h"
#endif /* (scb_RX_SDA_MOSI) */

#if (scb_RX_SDA_MOSI_PIN)
    #include "scb_uart_rx_i2c_sda_spi_mosi.h"
#endif /* (scb_RX_SDA_MOSI) */

#if (scb_TX_SCL_MISO_PIN)
    #include "scb_uart_tx_i2c_scl_spi_miso.h"
#endif /* (scb_TX_SCL_MISO) */

#if (scb_CTS_SCLK_PIN)
    #include "scb_uart_cts_spi_sclk.h"
#endif /* (scb_CTS_SCLK) */

#if (scb_RTS_SS0_PIN)
    #include "scb_uart_rts_spi_ss0.h"
#endif /* (scb_RTS_SS0_PIN) */

#if (scb_SS1_PIN)
    #include "scb_spi_ss1.h"
#endif /* (scb_SS1_PIN) */

#if (scb_SS2_PIN)
    #include "scb_spi_ss2.h"
#endif /* (scb_SS2_PIN) */

#if (scb_SS3_PIN)
    #include "scb_spi_ss3.h"
#endif /* (scb_SS3_PIN) */

#if (scb_I2C_PINS)
    #include "scb_scl.h"
    #include "scb_sda.h"
#endif /* (scb_I2C_PINS) */

#if (scb_SPI_MASTER_PINS)
#if (scb_SPI_MASTER_SCLK_PIN)
    #include "scb_sclk_m.h"
#endif /* (scb_SPI_MASTER_SCLK_PIN) */

#if (scb_SPI_MASTER_MOSI_PIN)
    #include "scb_mosi_m.h"
#endif /* (scb_SPI_MASTER_MOSI_PIN) */

#if (scb_SPI_MASTER_MISO_PIN)
    #include "scb_miso_m.h"
#endif /*(scb_SPI_MASTER_MISO_PIN) */
#endif /* (scb_SPI_MASTER_PINS) */

#if (scb_SPI_SLAVE_PINS)
    #include "scb_sclk_s.h"
    #include "scb_ss_s.h"

#if (scb_SPI_SLAVE_MOSI_PIN)
    #include "scb_mosi_s.h"
#endif /* (scb_SPI_SLAVE_MOSI_PIN) */

#if (scb_SPI_SLAVE_MISO_PIN)
    #include "scb_miso_s.h"
#endif /*(scb_SPI_SLAVE_MISO_PIN) */
#endif /* (scb_SPI_SLAVE_PINS) */

#if (scb_SPI_MASTER_SS0_PIN)
    #include "scb_ss0_m.h"
#endif /* (scb_SPI_MASTER_SS0_PIN) */

#if (scb_SPI_MASTER_SS1_PIN)
    #include "scb_ss1_m.h"
#endif /* (scb_SPI_MASTER_SS1_PIN) */

#if (scb_SPI_MASTER_SS2_PIN)
    #include "scb_ss2_m.h"
#endif /* (scb_SPI_MASTER_SS2_PIN) */

#if (scb_SPI_MASTER_SS3_PIN)
    #include "scb_ss3_m.h"
#endif /* (scb_SPI_MASTER_SS3_PIN) */

#if (scb_UART_TX_PIN)
    #include "scb_tx.h"
#endif /* (scb_UART_TX_PIN) */

#if (scb_UART_RX_TX_PIN)
    #include "scb_rx_tx.h"
#endif /* (scb_UART_RX_TX_PIN) */

#if (scb_UART_RX_PIN)
    #include "scb_rx.h"
#endif /* (scb_UART_RX_PIN) */

#if (scb_UART_RX_WAKE_PIN)
    #include "scb_rx_wake.h"
#endif /* (scb_UART_RX_WAKE_PIN) */

#if (scb_UART_RTS_PIN)
    #include "scb_rts.h"
#endif /* (scb_UART_RTS_PIN) */

#if (scb_UART_CTS_PIN)
    #include "scb_cts.h"
#endif /* (scb_UART_CTS_PIN) */


/***************************************
*              Registers
***************************************/

#if (scb_RX_SDA_MOSI_PIN)
    #define scb_RX_SDA_MOSI_HSIOM_REG   (*(reg32 *) scb_uart_rx_i2c_sda_spi_mosi__0__HSIOM)
    #define scb_RX_SDA_MOSI_HSIOM_PTR   ( (reg32 *) scb_uart_rx_i2c_sda_spi_mosi__0__HSIOM)
    
    #define scb_RX_SDA_MOSI_HSIOM_MASK      (scb_uart_rx_i2c_sda_spi_mosi__0__HSIOM_MASK)
    #define scb_RX_SDA_MOSI_HSIOM_POS       (scb_uart_rx_i2c_sda_spi_mosi__0__HSIOM_SHIFT)
    #define scb_RX_SDA_MOSI_HSIOM_SEL_GPIO  (scb_uart_rx_i2c_sda_spi_mosi__0__HSIOM_GPIO)
    #define scb_RX_SDA_MOSI_HSIOM_SEL_I2C   (scb_uart_rx_i2c_sda_spi_mosi__0__HSIOM_I2C)
    #define scb_RX_SDA_MOSI_HSIOM_SEL_SPI   (scb_uart_rx_i2c_sda_spi_mosi__0__HSIOM_SPI)
    #define scb_RX_SDA_MOSI_HSIOM_SEL_UART  (scb_uart_rx_i2c_sda_spi_mosi__0__HSIOM_UART)
    
#elif (scb_RX_WAKE_SDA_MOSI_PIN)
    #define scb_RX_WAKE_SDA_MOSI_HSIOM_REG   (*(reg32 *) scb_uart_rx_wake_i2c_sda_spi_mosi__0__HSIOM)
    #define scb_RX_WAKE_SDA_MOSI_HSIOM_PTR   ( (reg32 *) scb_uart_rx_wake_i2c_sda_spi_mosi__0__HSIOM)
    
    #define scb_RX_WAKE_SDA_MOSI_HSIOM_MASK      (scb_uart_rx_wake_i2c_sda_spi_mosi__0__HSIOM_MASK)
    #define scb_RX_WAKE_SDA_MOSI_HSIOM_POS       (scb_uart_rx_wake_i2c_sda_spi_mosi__0__HSIOM_SHIFT)
    #define scb_RX_WAKE_SDA_MOSI_HSIOM_SEL_GPIO  (scb_uart_rx_wake_i2c_sda_spi_mosi__0__HSIOM_GPIO)
    #define scb_RX_WAKE_SDA_MOSI_HSIOM_SEL_I2C   (scb_uart_rx_wake_i2c_sda_spi_mosi__0__HSIOM_I2C)
    #define scb_RX_WAKE_SDA_MOSI_HSIOM_SEL_SPI   (scb_uart_rx_wake_i2c_sda_spi_mosi__0__HSIOM_SPI)
    #define scb_RX_WAKE_SDA_MOSI_HSIOM_SEL_UART  (scb_uart_rx_wake_i2c_sda_spi_mosi__0__HSIOM_UART)    
   
    #define scb_RX_WAKE_SDA_MOSI_INTCFG_REG (*(reg32 *) scb_uart_rx_wake_i2c_sda_spi_mosi__0__INTCFG)
    #define scb_RX_WAKE_SDA_MOSI_INTCFG_PTR ( (reg32 *) scb_uart_rx_wake_i2c_sda_spi_mosi__0__INTCFG)
    #define scb_RX_WAKE_SDA_MOSI_INTCFG_TYPE_POS  (scb_uart_rx_wake_i2c_sda_spi_mosi__SHIFT)
    #define scb_RX_WAKE_SDA_MOSI_INTCFG_TYPE_MASK ((uint32) scb_INTCFG_TYPE_MASK << \
                                                                           scb_RX_WAKE_SDA_MOSI_INTCFG_TYPE_POS)
#else
    /* None of pins scb_RX_SDA_MOSI_PIN or scb_RX_WAKE_SDA_MOSI_PIN present.*/
#endif /* (scb_RX_SDA_MOSI_PIN) */

#if (scb_TX_SCL_MISO_PIN)
    #define scb_TX_SCL_MISO_HSIOM_REG   (*(reg32 *) scb_uart_tx_i2c_scl_spi_miso__0__HSIOM)
    #define scb_TX_SCL_MISO_HSIOM_PTR   ( (reg32 *) scb_uart_tx_i2c_scl_spi_miso__0__HSIOM)
    
    #define scb_TX_SCL_MISO_HSIOM_MASK      (scb_uart_tx_i2c_scl_spi_miso__0__HSIOM_MASK)
    #define scb_TX_SCL_MISO_HSIOM_POS       (scb_uart_tx_i2c_scl_spi_miso__0__HSIOM_SHIFT)
    #define scb_TX_SCL_MISO_HSIOM_SEL_GPIO  (scb_uart_tx_i2c_scl_spi_miso__0__HSIOM_GPIO)
    #define scb_TX_SCL_MISO_HSIOM_SEL_I2C   (scb_uart_tx_i2c_scl_spi_miso__0__HSIOM_I2C)
    #define scb_TX_SCL_MISO_HSIOM_SEL_SPI   (scb_uart_tx_i2c_scl_spi_miso__0__HSIOM_SPI)
    #define scb_TX_SCL_MISO_HSIOM_SEL_UART  (scb_uart_tx_i2c_scl_spi_miso__0__HSIOM_UART)
#endif /* (scb_TX_SCL_MISO_PIN) */

#if (scb_CTS_SCLK_PIN)
    #define scb_CTS_SCLK_HSIOM_REG   (*(reg32 *) scb_uart_cts_spi_sclk__0__HSIOM)
    #define scb_CTS_SCLK_HSIOM_PTR   ( (reg32 *) scb_uart_cts_spi_sclk__0__HSIOM)
    
    #define scb_CTS_SCLK_HSIOM_MASK      (scb_uart_cts_spi_sclk__0__HSIOM_MASK)
    #define scb_CTS_SCLK_HSIOM_POS       (scb_uart_cts_spi_sclk__0__HSIOM_SHIFT)
    #define scb_CTS_SCLK_HSIOM_SEL_GPIO  (scb_uart_cts_spi_sclk__0__HSIOM_GPIO)
    #define scb_CTS_SCLK_HSIOM_SEL_I2C   (scb_uart_cts_spi_sclk__0__HSIOM_I2C)
    #define scb_CTS_SCLK_HSIOM_SEL_SPI   (scb_uart_cts_spi_sclk__0__HSIOM_SPI)
    #define scb_CTS_SCLK_HSIOM_SEL_UART  (scb_uart_cts_spi_sclk__0__HSIOM_UART)
#endif /* (scb_CTS_SCLK_PIN) */

#if (scb_RTS_SS0_PIN)
    #define scb_RTS_SS0_HSIOM_REG   (*(reg32 *) scb_uart_rts_spi_ss0__0__HSIOM)
    #define scb_RTS_SS0_HSIOM_PTR   ( (reg32 *) scb_uart_rts_spi_ss0__0__HSIOM)
    
    #define scb_RTS_SS0_HSIOM_MASK      (scb_uart_rts_spi_ss0__0__HSIOM_MASK)
    #define scb_RTS_SS0_HSIOM_POS       (scb_uart_rts_spi_ss0__0__HSIOM_SHIFT)
    #define scb_RTS_SS0_HSIOM_SEL_GPIO  (scb_uart_rts_spi_ss0__0__HSIOM_GPIO)
    #define scb_RTS_SS0_HSIOM_SEL_I2C   (scb_uart_rts_spi_ss0__0__HSIOM_I2C)
    #define scb_RTS_SS0_HSIOM_SEL_SPI   (scb_uart_rts_spi_ss0__0__HSIOM_SPI)
#if !(scb_CY_SCBIP_V0 || scb_CY_SCBIP_V1)
    #define scb_RTS_SS0_HSIOM_SEL_UART  (scb_uart_rts_spi_ss0__0__HSIOM_UART)
#endif /* !(scb_CY_SCBIP_V0 || scb_CY_SCBIP_V1) */
#endif /* (scb_RTS_SS0_PIN) */

#if (scb_SS1_PIN)
    #define scb_SS1_HSIOM_REG  (*(reg32 *) scb_spi_ss1__0__HSIOM)
    #define scb_SS1_HSIOM_PTR  ( (reg32 *) scb_spi_ss1__0__HSIOM)
    
    #define scb_SS1_HSIOM_MASK     (scb_spi_ss1__0__HSIOM_MASK)
    #define scb_SS1_HSIOM_POS      (scb_spi_ss1__0__HSIOM_SHIFT)
    #define scb_SS1_HSIOM_SEL_GPIO (scb_spi_ss1__0__HSIOM_GPIO)
    #define scb_SS1_HSIOM_SEL_I2C  (scb_spi_ss1__0__HSIOM_I2C)
    #define scb_SS1_HSIOM_SEL_SPI  (scb_spi_ss1__0__HSIOM_SPI)
#endif /* (scb_SS1_PIN) */

#if (scb_SS2_PIN)
    #define scb_SS2_HSIOM_REG     (*(reg32 *) scb_spi_ss2__0__HSIOM)
    #define scb_SS2_HSIOM_PTR     ( (reg32 *) scb_spi_ss2__0__HSIOM)
    
    #define scb_SS2_HSIOM_MASK     (scb_spi_ss2__0__HSIOM_MASK)
    #define scb_SS2_HSIOM_POS      (scb_spi_ss2__0__HSIOM_SHIFT)
    #define scb_SS2_HSIOM_SEL_GPIO (scb_spi_ss2__0__HSIOM_GPIO)
    #define scb_SS2_HSIOM_SEL_I2C  (scb_spi_ss2__0__HSIOM_I2C)
    #define scb_SS2_HSIOM_SEL_SPI  (scb_spi_ss2__0__HSIOM_SPI)
#endif /* (scb_SS2_PIN) */

#if (scb_SS3_PIN)
    #define scb_SS3_HSIOM_REG     (*(reg32 *) scb_spi_ss3__0__HSIOM)
    #define scb_SS3_HSIOM_PTR     ( (reg32 *) scb_spi_ss3__0__HSIOM)
    
    #define scb_SS3_HSIOM_MASK     (scb_spi_ss3__0__HSIOM_MASK)
    #define scb_SS3_HSIOM_POS      (scb_spi_ss3__0__HSIOM_SHIFT)
    #define scb_SS3_HSIOM_SEL_GPIO (scb_spi_ss3__0__HSIOM_GPIO)
    #define scb_SS3_HSIOM_SEL_I2C  (scb_spi_ss3__0__HSIOM_I2C)
    #define scb_SS3_HSIOM_SEL_SPI  (scb_spi_ss3__0__HSIOM_SPI)
#endif /* (scb_SS3_PIN) */

#if (scb_I2C_PINS)
    #define scb_SCL_HSIOM_REG  (*(reg32 *) scb_scl__0__HSIOM)
    #define scb_SCL_HSIOM_PTR  ( (reg32 *) scb_scl__0__HSIOM)
    
    #define scb_SCL_HSIOM_MASK     (scb_scl__0__HSIOM_MASK)
    #define scb_SCL_HSIOM_POS      (scb_scl__0__HSIOM_SHIFT)
    #define scb_SCL_HSIOM_SEL_GPIO (scb_sda__0__HSIOM_GPIO)
    #define scb_SCL_HSIOM_SEL_I2C  (scb_sda__0__HSIOM_I2C)
    
    #define scb_SDA_HSIOM_REG  (*(reg32 *) scb_sda__0__HSIOM)
    #define scb_SDA_HSIOM_PTR  ( (reg32 *) scb_sda__0__HSIOM)
    
    #define scb_SDA_HSIOM_MASK     (scb_sda__0__HSIOM_MASK)
    #define scb_SDA_HSIOM_POS      (scb_sda__0__HSIOM_SHIFT)
    #define scb_SDA_HSIOM_SEL_GPIO (scb_sda__0__HSIOM_GPIO)
    #define scb_SDA_HSIOM_SEL_I2C  (scb_sda__0__HSIOM_I2C)
#endif /* (scb_I2C_PINS) */

#if (scb_SPI_SLAVE_PINS)
    #define scb_SCLK_S_HSIOM_REG   (*(reg32 *) scb_sclk_s__0__HSIOM)
    #define scb_SCLK_S_HSIOM_PTR   ( (reg32 *) scb_sclk_s__0__HSIOM)
    
    #define scb_SCLK_S_HSIOM_MASK      (scb_sclk_s__0__HSIOM_MASK)
    #define scb_SCLK_S_HSIOM_POS       (scb_sclk_s__0__HSIOM_SHIFT)
    #define scb_SCLK_S_HSIOM_SEL_GPIO  (scb_sclk_s__0__HSIOM_GPIO)
    #define scb_SCLK_S_HSIOM_SEL_SPI   (scb_sclk_s__0__HSIOM_SPI)
    
    #define scb_SS0_S_HSIOM_REG    (*(reg32 *) scb_ss0_s__0__HSIOM)
    #define scb_SS0_S_HSIOM_PTR    ( (reg32 *) scb_ss0_s__0__HSIOM)
    
    #define scb_SS0_S_HSIOM_MASK       (scb_ss0_s__0__HSIOM_MASK)
    #define scb_SS0_S_HSIOM_POS        (scb_ss0_s__0__HSIOM_SHIFT)
    #define scb_SS0_S_HSIOM_SEL_GPIO   (scb_ss0_s__0__HSIOM_GPIO)  
    #define scb_SS0_S_HSIOM_SEL_SPI    (scb_ss0_s__0__HSIOM_SPI)
#endif /* (scb_SPI_SLAVE_PINS) */

#if (scb_SPI_SLAVE_MOSI_PIN)
    #define scb_MOSI_S_HSIOM_REG   (*(reg32 *) scb_mosi_s__0__HSIOM)
    #define scb_MOSI_S_HSIOM_PTR   ( (reg32 *) scb_mosi_s__0__HSIOM)
    
    #define scb_MOSI_S_HSIOM_MASK      (scb_mosi_s__0__HSIOM_MASK)
    #define scb_MOSI_S_HSIOM_POS       (scb_mosi_s__0__HSIOM_SHIFT)
    #define scb_MOSI_S_HSIOM_SEL_GPIO  (scb_mosi_s__0__HSIOM_GPIO)
    #define scb_MOSI_S_HSIOM_SEL_SPI   (scb_mosi_s__0__HSIOM_SPI)
#endif /* (scb_SPI_SLAVE_MOSI_PIN) */

#if (scb_SPI_SLAVE_MISO_PIN)
    #define scb_MISO_S_HSIOM_REG   (*(reg32 *) scb_miso_s__0__HSIOM)
    #define scb_MISO_S_HSIOM_PTR   ( (reg32 *) scb_miso_s__0__HSIOM)
    
    #define scb_MISO_S_HSIOM_MASK      (scb_miso_s__0__HSIOM_MASK)
    #define scb_MISO_S_HSIOM_POS       (scb_miso_s__0__HSIOM_SHIFT)
    #define scb_MISO_S_HSIOM_SEL_GPIO  (scb_miso_s__0__HSIOM_GPIO)
    #define scb_MISO_S_HSIOM_SEL_SPI   (scb_miso_s__0__HSIOM_SPI)
#endif /* (scb_SPI_SLAVE_MISO_PIN) */

#if (scb_SPI_MASTER_MISO_PIN)
    #define scb_MISO_M_HSIOM_REG   (*(reg32 *) scb_miso_m__0__HSIOM)
    #define scb_MISO_M_HSIOM_PTR   ( (reg32 *) scb_miso_m__0__HSIOM)
    
    #define scb_MISO_M_HSIOM_MASK      (scb_miso_m__0__HSIOM_MASK)
    #define scb_MISO_M_HSIOM_POS       (scb_miso_m__0__HSIOM_SHIFT)
    #define scb_MISO_M_HSIOM_SEL_GPIO  (scb_miso_m__0__HSIOM_GPIO)
    #define scb_MISO_M_HSIOM_SEL_SPI   (scb_miso_m__0__HSIOM_SPI)
#endif /* (scb_SPI_MASTER_MISO_PIN) */

#if (scb_SPI_MASTER_MOSI_PIN)
    #define scb_MOSI_M_HSIOM_REG   (*(reg32 *) scb_mosi_m__0__HSIOM)
    #define scb_MOSI_M_HSIOM_PTR   ( (reg32 *) scb_mosi_m__0__HSIOM)
    
    #define scb_MOSI_M_HSIOM_MASK      (scb_mosi_m__0__HSIOM_MASK)
    #define scb_MOSI_M_HSIOM_POS       (scb_mosi_m__0__HSIOM_SHIFT)
    #define scb_MOSI_M_HSIOM_SEL_GPIO  (scb_mosi_m__0__HSIOM_GPIO)
    #define scb_MOSI_M_HSIOM_SEL_SPI   (scb_mosi_m__0__HSIOM_SPI)
#endif /* (scb_SPI_MASTER_MOSI_PIN) */

#if (scb_SPI_MASTER_SCLK_PIN)
    #define scb_SCLK_M_HSIOM_REG   (*(reg32 *) scb_sclk_m__0__HSIOM)
    #define scb_SCLK_M_HSIOM_PTR   ( (reg32 *) scb_sclk_m__0__HSIOM)
    
    #define scb_SCLK_M_HSIOM_MASK      (scb_sclk_m__0__HSIOM_MASK)
    #define scb_SCLK_M_HSIOM_POS       (scb_sclk_m__0__HSIOM_SHIFT)
    #define scb_SCLK_M_HSIOM_SEL_GPIO  (scb_sclk_m__0__HSIOM_GPIO)
    #define scb_SCLK_M_HSIOM_SEL_SPI   (scb_sclk_m__0__HSIOM_SPI)
#endif /* (scb_SPI_MASTER_SCLK_PIN) */

#if (scb_SPI_MASTER_SS0_PIN)
    #define scb_SS0_M_HSIOM_REG    (*(reg32 *) scb_ss0_m__0__HSIOM)
    #define scb_SS0_M_HSIOM_PTR    ( (reg32 *) scb_ss0_m__0__HSIOM)
    
    #define scb_SS0_M_HSIOM_MASK       (scb_ss0_m__0__HSIOM_MASK)
    #define scb_SS0_M_HSIOM_POS        (scb_ss0_m__0__HSIOM_SHIFT)
    #define scb_SS0_M_HSIOM_SEL_GPIO   (scb_ss0_m__0__HSIOM_GPIO)
    #define scb_SS0_M_HSIOM_SEL_SPI    (scb_ss0_m__0__HSIOM_SPI)
#endif /* (scb_SPI_MASTER_SS0_PIN) */

#if (scb_SPI_MASTER_SS1_PIN)
    #define scb_SS1_M_HSIOM_REG    (*(reg32 *) scb_ss1_m__0__HSIOM)
    #define scb_SS1_M_HSIOM_PTR    ( (reg32 *) scb_ss1_m__0__HSIOM)
    
    #define scb_SS1_M_HSIOM_MASK       (scb_ss1_m__0__HSIOM_MASK)
    #define scb_SS1_M_HSIOM_POS        (scb_ss1_m__0__HSIOM_SHIFT)
    #define scb_SS1_M_HSIOM_SEL_GPIO   (scb_ss1_m__0__HSIOM_GPIO)
    #define scb_SS1_M_HSIOM_SEL_SPI    (scb_ss1_m__0__HSIOM_SPI)
#endif /* (scb_SPI_MASTER_SS1_PIN) */

#if (scb_SPI_MASTER_SS2_PIN)
    #define scb_SS2_M_HSIOM_REG    (*(reg32 *) scb_ss2_m__0__HSIOM)
    #define scb_SS2_M_HSIOM_PTR    ( (reg32 *) scb_ss2_m__0__HSIOM)
    
    #define scb_SS2_M_HSIOM_MASK       (scb_ss2_m__0__HSIOM_MASK)
    #define scb_SS2_M_HSIOM_POS        (scb_ss2_m__0__HSIOM_SHIFT)
    #define scb_SS2_M_HSIOM_SEL_GPIO   (scb_ss2_m__0__HSIOM_GPIO)
    #define scb_SS2_M_HSIOM_SEL_SPI    (scb_ss2_m__0__HSIOM_SPI)
#endif /* (scb_SPI_MASTER_SS2_PIN) */

#if (scb_SPI_MASTER_SS3_PIN)
    #define scb_SS3_M_HSIOM_REG    (*(reg32 *) scb_ss3_m__0__HSIOM)
    #define scb_SS3_M_HSIOM_PTR    ( (reg32 *) scb_ss3_m__0__HSIOM)
    
    #define scb_SS3_M_HSIOM_MASK      (scb_ss3_m__0__HSIOM_MASK)
    #define scb_SS3_M_HSIOM_POS       (scb_ss3_m__0__HSIOM_SHIFT)
    #define scb_SS3_M_HSIOM_SEL_GPIO  (scb_ss3_m__0__HSIOM_GPIO)
    #define scb_SS3_M_HSIOM_SEL_SPI   (scb_ss3_m__0__HSIOM_SPI)
#endif /* (scb_SPI_MASTER_SS3_PIN) */

#if (scb_UART_RX_PIN)
    #define scb_RX_HSIOM_REG   (*(reg32 *) scb_rx__0__HSIOM)
    #define scb_RX_HSIOM_PTR   ( (reg32 *) scb_rx__0__HSIOM)
    
    #define scb_RX_HSIOM_MASK      (scb_rx__0__HSIOM_MASK)
    #define scb_RX_HSIOM_POS       (scb_rx__0__HSIOM_SHIFT)
    #define scb_RX_HSIOM_SEL_GPIO  (scb_rx__0__HSIOM_GPIO)
    #define scb_RX_HSIOM_SEL_UART  (scb_rx__0__HSIOM_UART)
#endif /* (scb_UART_RX_PIN) */

#if (scb_UART_RX_WAKE_PIN)
    #define scb_RX_WAKE_HSIOM_REG   (*(reg32 *) scb_rx_wake__0__HSIOM)
    #define scb_RX_WAKE_HSIOM_PTR   ( (reg32 *) scb_rx_wake__0__HSIOM)
    
    #define scb_RX_WAKE_HSIOM_MASK      (scb_rx_wake__0__HSIOM_MASK)
    #define scb_RX_WAKE_HSIOM_POS       (scb_rx_wake__0__HSIOM_SHIFT)
    #define scb_RX_WAKE_HSIOM_SEL_GPIO  (scb_rx_wake__0__HSIOM_GPIO)
    #define scb_RX_WAKE_HSIOM_SEL_UART  (scb_rx_wake__0__HSIOM_UART)
#endif /* (scb_UART_WAKE_RX_PIN) */

#if (scb_UART_CTS_PIN)
    #define scb_CTS_HSIOM_REG   (*(reg32 *) scb_cts__0__HSIOM)
    #define scb_CTS_HSIOM_PTR   ( (reg32 *) scb_cts__0__HSIOM)
    
    #define scb_CTS_HSIOM_MASK      (scb_cts__0__HSIOM_MASK)
    #define scb_CTS_HSIOM_POS       (scb_cts__0__HSIOM_SHIFT)
    #define scb_CTS_HSIOM_SEL_GPIO  (scb_cts__0__HSIOM_GPIO)
    #define scb_CTS_HSIOM_SEL_UART  (scb_cts__0__HSIOM_UART)
#endif /* (scb_UART_CTS_PIN) */

#if (scb_UART_TX_PIN)
    #define scb_TX_HSIOM_REG   (*(reg32 *) scb_tx__0__HSIOM)
    #define scb_TX_HSIOM_PTR   ( (reg32 *) scb_tx__0__HSIOM)
    
    #define scb_TX_HSIOM_MASK      (scb_tx__0__HSIOM_MASK)
    #define scb_TX_HSIOM_POS       (scb_tx__0__HSIOM_SHIFT)
    #define scb_TX_HSIOM_SEL_GPIO  (scb_tx__0__HSIOM_GPIO)
    #define scb_TX_HSIOM_SEL_UART  (scb_tx__0__HSIOM_UART)
#endif /* (scb_UART_TX_PIN) */

#if (scb_UART_RX_TX_PIN)
    #define scb_RX_TX_HSIOM_REG   (*(reg32 *) scb_rx_tx__0__HSIOM)
    #define scb_RX_TX_HSIOM_PTR   ( (reg32 *) scb_rx_tx__0__HSIOM)
    
    #define scb_RX_TX_HSIOM_MASK      (scb_rx_tx__0__HSIOM_MASK)
    #define scb_RX_TX_HSIOM_POS       (scb_rx_tx__0__HSIOM_SHIFT)
    #define scb_RX_TX_HSIOM_SEL_GPIO  (scb_rx_tx__0__HSIOM_GPIO)
    #define scb_RX_TX_HSIOM_SEL_UART  (scb_rx_tx__0__HSIOM_UART)
#endif /* (scb_UART_RX_TX_PIN) */

#if (scb_UART_RTS_PIN)
    #define scb_RTS_HSIOM_REG      (*(reg32 *) scb_rts__0__HSIOM)
    #define scb_RTS_HSIOM_PTR      ( (reg32 *) scb_rts__0__HSIOM)
    
    #define scb_RTS_HSIOM_MASK     (scb_rts__0__HSIOM_MASK)
    #define scb_RTS_HSIOM_POS      (scb_rts__0__HSIOM_SHIFT)    
    #define scb_RTS_HSIOM_SEL_GPIO (scb_rts__0__HSIOM_GPIO)
    #define scb_RTS_HSIOM_SEL_UART (scb_rts__0__HSIOM_UART)    
#endif /* (scb_UART_RTS_PIN) */


/***************************************
*        Registers Constants
***************************************/

/* HSIOM switch values. */ 
#define scb_HSIOM_DEF_SEL      (0x00u)
#define scb_HSIOM_GPIO_SEL     (0x00u)
/* The HSIOM values provided below are valid only for scb_CY_SCBIP_V0 
* and scb_CY_SCBIP_V1. It is not recommended to use them for 
* scb_CY_SCBIP_V2. Use pin name specific HSIOM constants provided 
* above instead for any SCB IP block version.
*/
#define scb_HSIOM_UART_SEL     (0x09u)
#define scb_HSIOM_I2C_SEL      (0x0Eu)
#define scb_HSIOM_SPI_SEL      (0x0Fu)

/* Pins settings index. */
#define scb_RX_WAKE_SDA_MOSI_PIN_INDEX   (0u)
#define scb_RX_SDA_MOSI_PIN_INDEX       (0u)
#define scb_TX_SCL_MISO_PIN_INDEX       (1u)
#define scb_CTS_SCLK_PIN_INDEX       (2u)
#define scb_RTS_SS0_PIN_INDEX       (3u)
#define scb_SS1_PIN_INDEX                  (4u)
#define scb_SS2_PIN_INDEX                  (5u)
#define scb_SS3_PIN_INDEX                  (6u)

/* Pins settings mask. */
#define scb_RX_WAKE_SDA_MOSI_PIN_MASK ((uint32) 0x01u << scb_RX_WAKE_SDA_MOSI_PIN_INDEX)
#define scb_RX_SDA_MOSI_PIN_MASK     ((uint32) 0x01u << scb_RX_SDA_MOSI_PIN_INDEX)
#define scb_TX_SCL_MISO_PIN_MASK     ((uint32) 0x01u << scb_TX_SCL_MISO_PIN_INDEX)
#define scb_CTS_SCLK_PIN_MASK     ((uint32) 0x01u << scb_CTS_SCLK_PIN_INDEX)
#define scb_RTS_SS0_PIN_MASK     ((uint32) 0x01u << scb_RTS_SS0_PIN_INDEX)
#define scb_SS1_PIN_MASK                ((uint32) 0x01u << scb_SS1_PIN_INDEX)
#define scb_SS2_PIN_MASK                ((uint32) 0x01u << scb_SS2_PIN_INDEX)
#define scb_SS3_PIN_MASK                ((uint32) 0x01u << scb_SS3_PIN_INDEX)

/* Pin interrupt constants. */
#define scb_INTCFG_TYPE_MASK           (0x03u)
#define scb_INTCFG_TYPE_FALLING_EDGE   (0x02u)

/* Pin Drive Mode constants. */
#define scb_PIN_DM_ALG_HIZ  (0u)
#define scb_PIN_DM_DIG_HIZ  (1u)
#define scb_PIN_DM_OD_LO    (4u)
#define scb_PIN_DM_STRONG   (6u)


/***************************************
*          Macro Definitions
***************************************/

/* Return drive mode of the pin */
#define scb_DM_MASK    (0x7u)
#define scb_DM_SIZE    (3u)
#define scb_GET_P4_PIN_DM(reg, pos) \
    ( ((reg) & (uint32) ((uint32) scb_DM_MASK << (scb_DM_SIZE * (pos)))) >> \
                                                              (scb_DM_SIZE * (pos)) )

#if (scb_TX_SCL_MISO_PIN)
    #define scb_CHECK_TX_SCL_MISO_PIN_USED \
                (scb_PIN_DM_ALG_HIZ != \
                    scb_GET_P4_PIN_DM(scb_uart_tx_i2c_scl_spi_miso_PC, \
                                                   scb_uart_tx_i2c_scl_spi_miso_SHIFT))
#endif /* (scb_TX_SCL_MISO_PIN) */

#if (scb_RTS_SS0_PIN)
    #define scb_CHECK_RTS_SS0_PIN_USED \
                (scb_PIN_DM_ALG_HIZ != \
                    scb_GET_P4_PIN_DM(scb_uart_rts_spi_ss0_PC, \
                                                   scb_uart_rts_spi_ss0_SHIFT))
#endif /* (scb_RTS_SS0_PIN) */

/* Set bits-mask in register */
#define scb_SET_REGISTER_BITS(reg, mask, pos, mode) \
                    do                                           \
                    {                                            \
                        (reg) = (((reg) & ((uint32) ~(uint32) (mask))) | ((uint32) ((uint32) (mode) << (pos)))); \
                    }while(0)

/* Set bit in the register */
#define scb_SET_REGISTER_BIT(reg, mask, val) \
                    ((val) ? ((reg) |= (mask)) : ((reg) &= ((uint32) ~((uint32) (mask)))))

#define scb_SET_HSIOM_SEL(reg, mask, pos, sel) scb_SET_REGISTER_BITS(reg, mask, pos, sel)
#define scb_SET_INCFG_TYPE(reg, mask, pos, intType) \
                                                        scb_SET_REGISTER_BITS(reg, mask, pos, intType)
#define scb_SET_INP_DIS(reg, mask, val) scb_SET_REGISTER_BIT(reg, mask, val)

/* scb_SET_I2C_SCL_DR(val) - Sets I2C SCL DR register.
*  scb_SET_I2C_SCL_HSIOM_SEL(sel) - Sets I2C SCL HSIOM settings.
*/
/* SCB I2C: scl signal */
#if (scb_CY_SCBIP_V0)
#if (scb_I2C_PINS)
    #define scb_SET_I2C_SCL_DR(val) scb_scl_Write(val)

    #define scb_SET_I2C_SCL_HSIOM_SEL(sel) \
                          scb_SET_HSIOM_SEL(scb_SCL_HSIOM_REG,  \
                                                         scb_SCL_HSIOM_MASK, \
                                                         scb_SCL_HSIOM_POS,  \
                                                         (sel))
    #define scb_WAIT_SCL_SET_HIGH  (0u == scb_scl_Read())

/* Unconfigured SCB: scl signal */
#elif (scb_RX_WAKE_SDA_MOSI_PIN)
    #define scb_SET_I2C_SCL_DR(val) \
                            scb_uart_rx_wake_i2c_sda_spi_mosi_Write(val)

    #define scb_SET_I2C_SCL_HSIOM_SEL(sel) \
                    scb_SET_HSIOM_SEL(scb_RX_WAKE_SDA_MOSI_HSIOM_REG,  \
                                                   scb_RX_WAKE_SDA_MOSI_HSIOM_MASK, \
                                                   scb_RX_WAKE_SDA_MOSI_HSIOM_POS,  \
                                                   (sel))

    #define scb_WAIT_SCL_SET_HIGH  (0u == scb_uart_rx_wake_i2c_sda_spi_mosi_Read())

#elif (scb_RX_SDA_MOSI_PIN)
    #define scb_SET_I2C_SCL_DR(val) \
                            scb_uart_rx_i2c_sda_spi_mosi_Write(val)


    #define scb_SET_I2C_SCL_HSIOM_SEL(sel) \
                            scb_SET_HSIOM_SEL(scb_RX_SDA_MOSI_HSIOM_REG,  \
                                                           scb_RX_SDA_MOSI_HSIOM_MASK, \
                                                           scb_RX_SDA_MOSI_HSIOM_POS,  \
                                                           (sel))

    #define scb_WAIT_SCL_SET_HIGH  (0u == scb_uart_rx_i2c_sda_spi_mosi_Read())

#else
    #define scb_SET_I2C_SCL_DR(val)        do{ /* Does nothing */ }while(0)
    #define scb_SET_I2C_SCL_HSIOM_SEL(sel) do{ /* Does nothing */ }while(0)

    #define scb_WAIT_SCL_SET_HIGH  (0u)
#endif /* (scb_I2C_PINS) */

/* SCB I2C: sda signal */
#if (scb_I2C_PINS)
    #define scb_WAIT_SDA_SET_HIGH  (0u == scb_sda_Read())
/* Unconfigured SCB: sda signal */
#elif (scb_TX_SCL_MISO_PIN)
    #define scb_WAIT_SDA_SET_HIGH  (0u == scb_uart_tx_i2c_scl_spi_miso_Read())
#else
    #define scb_WAIT_SDA_SET_HIGH  (0u)
#endif /* (scb_MOSI_SCL_RX_PIN) */
#endif /* (scb_CY_SCBIP_V0) */

/* Clear UART wakeup source */
#if (scb_RX_SDA_MOSI_PIN)
    #define scb_CLEAR_UART_RX_WAKE_INTR        do{ /* Does nothing */ }while(0)
    
#elif (scb_RX_WAKE_SDA_MOSI_PIN)
    #define scb_CLEAR_UART_RX_WAKE_INTR \
            do{                                      \
                (void) scb_uart_rx_wake_i2c_sda_spi_mosi_ClearInterrupt(); \
            }while(0)

#elif(scb_UART_RX_WAKE_PIN)
    #define scb_CLEAR_UART_RX_WAKE_INTR \
            do{                                      \
                (void) scb_rx_wake_ClearInterrupt(); \
            }while(0)
#else
#endif /* (scb_RX_SDA_MOSI_PIN) */


/***************************************
* The following code is DEPRECATED and
* must not be used.
***************************************/

/* Unconfigured pins */
#define scb_REMOVE_MOSI_SCL_RX_WAKE_PIN    scb_REMOVE_RX_WAKE_SDA_MOSI_PIN
#define scb_REMOVE_MOSI_SCL_RX_PIN         scb_REMOVE_RX_SDA_MOSI_PIN
#define scb_REMOVE_MISO_SDA_TX_PIN         scb_REMOVE_TX_SCL_MISO_PIN
#ifndef scb_REMOVE_SCLK_PIN
#define scb_REMOVE_SCLK_PIN                scb_REMOVE_CTS_SCLK_PIN
#endif /* scb_REMOVE_SCLK_PIN */
#ifndef scb_REMOVE_SS0_PIN
#define scb_REMOVE_SS0_PIN                 scb_REMOVE_RTS_SS0_PIN
#endif /* scb_REMOVE_SS0_PIN */

/* Unconfigured pins */
#define scb_MOSI_SCL_RX_WAKE_PIN   scb_RX_WAKE_SDA_MOSI_PIN
#define scb_MOSI_SCL_RX_PIN        scb_RX_SDA_MOSI_PIN
#define scb_MISO_SDA_TX_PIN        scb_TX_SCL_MISO_PIN
#ifndef scb_SCLK_PIN
#define scb_SCLK_PIN               scb_CTS_SCLK_PIN
#endif /* scb_SCLK_PIN */
#ifndef scb_SS0_PIN
#define scb_SS0_PIN                scb_RTS_SS0_PIN
#endif /* scb_SS0_PIN */

#if (scb_MOSI_SCL_RX_WAKE_PIN)
    #define scb_MOSI_SCL_RX_WAKE_HSIOM_REG     scb_RX_WAKE_SDA_MOSI_HSIOM_REG
    #define scb_MOSI_SCL_RX_WAKE_HSIOM_PTR     scb_RX_WAKE_SDA_MOSI_HSIOM_REG
    #define scb_MOSI_SCL_RX_WAKE_HSIOM_MASK    scb_RX_WAKE_SDA_MOSI_HSIOM_REG
    #define scb_MOSI_SCL_RX_WAKE_HSIOM_POS     scb_RX_WAKE_SDA_MOSI_HSIOM_REG

    #define scb_MOSI_SCL_RX_WAKE_INTCFG_REG    scb_RX_WAKE_SDA_MOSI_HSIOM_REG
    #define scb_MOSI_SCL_RX_WAKE_INTCFG_PTR    scb_RX_WAKE_SDA_MOSI_HSIOM_REG

    #define scb_MOSI_SCL_RX_WAKE_INTCFG_TYPE_POS   scb_RX_WAKE_SDA_MOSI_HSIOM_REG
    #define scb_MOSI_SCL_RX_WAKE_INTCFG_TYPE_MASK  scb_RX_WAKE_SDA_MOSI_HSIOM_REG
#endif /* (scb_RX_WAKE_SDA_MOSI_PIN) */

#if (scb_MOSI_SCL_RX_PIN)
    #define scb_MOSI_SCL_RX_HSIOM_REG      scb_RX_SDA_MOSI_HSIOM_REG
    #define scb_MOSI_SCL_RX_HSIOM_PTR      scb_RX_SDA_MOSI_HSIOM_PTR
    #define scb_MOSI_SCL_RX_HSIOM_MASK     scb_RX_SDA_MOSI_HSIOM_MASK
    #define scb_MOSI_SCL_RX_HSIOM_POS      scb_RX_SDA_MOSI_HSIOM_POS
#endif /* (scb_MOSI_SCL_RX_PIN) */

#if (scb_MISO_SDA_TX_PIN)
    #define scb_MISO_SDA_TX_HSIOM_REG      scb_TX_SCL_MISO_HSIOM_REG
    #define scb_MISO_SDA_TX_HSIOM_PTR      scb_TX_SCL_MISO_HSIOM_REG
    #define scb_MISO_SDA_TX_HSIOM_MASK     scb_TX_SCL_MISO_HSIOM_REG
    #define scb_MISO_SDA_TX_HSIOM_POS      scb_TX_SCL_MISO_HSIOM_REG
#endif /* (scb_MISO_SDA_TX_PIN_PIN) */

#if (scb_SCLK_PIN)
    #ifndef scb_SCLK_HSIOM_REG
    #define scb_SCLK_HSIOM_REG     scb_CTS_SCLK_HSIOM_REG
    #define scb_SCLK_HSIOM_PTR     scb_CTS_SCLK_HSIOM_PTR
    #define scb_SCLK_HSIOM_MASK    scb_CTS_SCLK_HSIOM_MASK
    #define scb_SCLK_HSIOM_POS     scb_CTS_SCLK_HSIOM_POS
    #endif /* scb_SCLK_HSIOM_REG */
#endif /* (scb_SCLK_PIN) */

#if (scb_SS0_PIN)
    #ifndef scb_SS0_HSIOM_REG
    #define scb_SS0_HSIOM_REG      scb_RTS_SS0_HSIOM_REG
    #define scb_SS0_HSIOM_PTR      scb_RTS_SS0_HSIOM_PTR
    #define scb_SS0_HSIOM_MASK     scb_RTS_SS0_HSIOM_MASK
    #define scb_SS0_HSIOM_POS      scb_RTS_SS0_HSIOM_POS
    #endif /* scb_SS0_HSIOM_REG */
#endif /* (scb_SS0_PIN) */

#define scb_MOSI_SCL_RX_WAKE_PIN_INDEX scb_RX_WAKE_SDA_MOSI_PIN_INDEX
#define scb_MOSI_SCL_RX_PIN_INDEX      scb_RX_SDA_MOSI_PIN_INDEX
#define scb_MISO_SDA_TX_PIN_INDEX      scb_TX_SCL_MISO_PIN_INDEX
#ifndef scb_SCLK_PIN_INDEX
#define scb_SCLK_PIN_INDEX             scb_CTS_SCLK_PIN_INDEX
#endif /* scb_SCLK_PIN_INDEX */
#ifndef scb_SS0_PIN_INDEX
#define scb_SS0_PIN_INDEX              scb_RTS_SS0_PIN_INDEX
#endif /* scb_SS0_PIN_INDEX */

#define scb_MOSI_SCL_RX_WAKE_PIN_MASK scb_RX_WAKE_SDA_MOSI_PIN_MASK
#define scb_MOSI_SCL_RX_PIN_MASK      scb_RX_SDA_MOSI_PIN_MASK
#define scb_MISO_SDA_TX_PIN_MASK      scb_TX_SCL_MISO_PIN_MASK
#ifndef scb_SCLK_PIN_MASK
#define scb_SCLK_PIN_MASK             scb_CTS_SCLK_PIN_MASK
#endif /* scb_SCLK_PIN_MASK */
#ifndef scb_SS0_PIN_MASK
#define scb_SS0_PIN_MASK              scb_RTS_SS0_PIN_MASK
#endif /* scb_SS0_PIN_MASK */

#endif /* (CY_SCB_PINS_scb_H) */


/* [] END OF FILE */
