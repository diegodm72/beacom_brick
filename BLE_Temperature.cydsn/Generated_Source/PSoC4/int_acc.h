/*******************************************************************************
* File Name: int_acc.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_int_acc_H) /* Pins int_acc_H */
#define CY_PINS_int_acc_H

#include "cytypes.h"
#include "cyfitter.h"
#include "int_acc_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} int_acc_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   int_acc_Read(void);
void    int_acc_Write(uint8 value);
uint8   int_acc_ReadDataReg(void);
#if defined(int_acc__PC) || (CY_PSOC4_4200L) 
    void    int_acc_SetDriveMode(uint8 mode);
#endif
void    int_acc_SetInterruptMode(uint16 position, uint16 mode);
uint8   int_acc_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void int_acc_Sleep(void); 
void int_acc_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(int_acc__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define int_acc_DRIVE_MODE_BITS        (3)
    #define int_acc_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - int_acc_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the int_acc_SetDriveMode() function.
         *  @{
         */
        #define int_acc_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define int_acc_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define int_acc_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define int_acc_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define int_acc_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define int_acc_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define int_acc_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define int_acc_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define int_acc_MASK               int_acc__MASK
#define int_acc_SHIFT              int_acc__SHIFT
#define int_acc_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in int_acc_SetInterruptMode() function.
     *  @{
     */
        #define int_acc_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define int_acc_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define int_acc_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define int_acc_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(int_acc__SIO)
    #define int_acc_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(int_acc__PC) && (CY_PSOC4_4200L)
    #define int_acc_USBIO_ENABLE               ((uint32)0x80000000u)
    #define int_acc_USBIO_DISABLE              ((uint32)(~int_acc_USBIO_ENABLE))
    #define int_acc_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define int_acc_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define int_acc_USBIO_ENTER_SLEEP          ((uint32)((1u << int_acc_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << int_acc_USBIO_SUSPEND_DEL_SHIFT)))
    #define int_acc_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << int_acc_USBIO_SUSPEND_SHIFT)))
    #define int_acc_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << int_acc_USBIO_SUSPEND_DEL_SHIFT)))
    #define int_acc_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(int_acc__PC)
    /* Port Configuration */
    #define int_acc_PC                 (* (reg32 *) int_acc__PC)
#endif
/* Pin State */
#define int_acc_PS                     (* (reg32 *) int_acc__PS)
/* Data Register */
#define int_acc_DR                     (* (reg32 *) int_acc__DR)
/* Input Buffer Disable Override */
#define int_acc_INP_DIS                (* (reg32 *) int_acc__PC2)

/* Interrupt configuration Registers */
#define int_acc_INTCFG                 (* (reg32 *) int_acc__INTCFG)
#define int_acc_INTSTAT                (* (reg32 *) int_acc__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define int_acc_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(int_acc__SIO)
    #define int_acc_SIO_REG            (* (reg32 *) int_acc__SIO)
#endif /* (int_acc__SIO_CFG) */

/* USBIO registers */
#if !defined(int_acc__PC) && (CY_PSOC4_4200L)
    #define int_acc_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define int_acc_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define int_acc_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define int_acc_DRIVE_MODE_SHIFT       (0x00u)
#define int_acc_DRIVE_MODE_MASK        (0x07u << int_acc_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins int_acc_H */


/* [] END OF FILE */
