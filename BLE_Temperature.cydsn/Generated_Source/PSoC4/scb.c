/***************************************************************************//**
* \file scb.c
* \version 4.0
*
* \brief
*  This file provides the source code to the API for the SCB Component.
*
* Note:
*
*******************************************************************************
* \copyright
* Copyright 2013-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "scb_PVT.h"

#if (scb_SCB_MODE_I2C_INC)
    #include "scb_I2C_PVT.h"
#endif /* (scb_SCB_MODE_I2C_INC) */

#if (scb_SCB_MODE_EZI2C_INC)
    #include "scb_EZI2C_PVT.h"
#endif /* (scb_SCB_MODE_EZI2C_INC) */

#if (scb_SCB_MODE_SPI_INC || scb_SCB_MODE_UART_INC)
    #include "scb_SPI_UART_PVT.h"
#endif /* (scb_SCB_MODE_SPI_INC || scb_SCB_MODE_UART_INC) */


/***************************************
*    Run Time Configuration Vars
***************************************/

/* Stores internal component configuration for Unconfigured mode */
#if (scb_SCB_MODE_UNCONFIG_CONST_CFG)
    /* Common configuration variables */
    uint8 scb_scbMode = scb_SCB_MODE_UNCONFIG;
    uint8 scb_scbEnableWake;
    uint8 scb_scbEnableIntr;

    /* I2C configuration variables */
    uint8 scb_mode;
    uint8 scb_acceptAddr;

    /* SPI/UART configuration variables */
    volatile uint8 * scb_rxBuffer;
    uint8  scb_rxDataBits;
    uint32 scb_rxBufferSize;

    volatile uint8 * scb_txBuffer;
    uint8  scb_txDataBits;
    uint32 scb_txBufferSize;

    /* EZI2C configuration variables */
    uint8 scb_numberOfAddr;
    uint8 scb_subAddrSize;
#endif /* (scb_SCB_MODE_UNCONFIG_CONST_CFG) */


/***************************************
*     Common SCB Vars
***************************************/
/**
* \addtogroup group_general
* \{
*/

/** scb_initVar indicates whether the scb 
*  component has been initialized. The variable is initialized to 0 
*  and set to 1 the first time SCB_Start() is called. This allows 
*  the component to restart without reinitialization after the first 
*  call to the scb_Start() routine.
*
*  If re-initialization of the component is required, then the 
*  scb_Init() function can be called before the 
*  scb_Start() or scb_Enable() function.
*/
uint8 scb_initVar = 0u;


#if (! (scb_SCB_MODE_I2C_CONST_CFG || \
        scb_SCB_MODE_EZI2C_CONST_CFG))
    /** This global variable stores TX interrupt sources after 
    * scb_Stop() is called. Only these TX interrupt sources 
    * will be restored on a subsequent scb_Enable() call.
    */
    uint16 scb_IntrTxMask = 0u;
#endif /* (! (scb_SCB_MODE_I2C_CONST_CFG || \
              scb_SCB_MODE_EZI2C_CONST_CFG)) */
/** \} globals */

#if (scb_SCB_IRQ_INTERNAL)
#if !defined (CY_REMOVE_scb_CUSTOM_INTR_HANDLER)
    void (*scb_customIntrHandler)(void) = NULL;
#endif /* !defined (CY_REMOVE_scb_CUSTOM_INTR_HANDLER) */
#endif /* (scb_SCB_IRQ_INTERNAL) */


/***************************************
*    Private Function Prototypes
***************************************/

static void scb_ScbEnableIntr(void);
static void scb_ScbModeStop(void);
static void scb_ScbModePostEnable(void);


/*******************************************************************************
* Function Name: scb_Init
****************************************************************************//**
*
*  Initializes the scb component to operate in one of the selected
*  configurations: I2C, SPI, UART or EZI2C.
*  When the configuration is set to "Unconfigured SCB", this function does
*  not do any initialization. Use mode-specific initialization APIs instead:
*  scb_I2CInit, scb_SpiInit, 
*  scb_UartInit or scb_EzI2CInit.
*
*******************************************************************************/
void scb_Init(void)
{
#if (scb_SCB_MODE_UNCONFIG_CONST_CFG)
    if (scb_SCB_MODE_UNCONFIG_RUNTM_CFG)
    {
        scb_initVar = 0u;
    }
    else
    {
        /* Initialization was done before this function call */
    }

#elif (scb_SCB_MODE_I2C_CONST_CFG)
    scb_I2CInit();

#elif (scb_SCB_MODE_SPI_CONST_CFG)
    scb_SpiInit();

#elif (scb_SCB_MODE_UART_CONST_CFG)
    scb_UartInit();

#elif (scb_SCB_MODE_EZI2C_CONST_CFG)
    scb_EzI2CInit();

#endif /* (scb_SCB_MODE_UNCONFIG_CONST_CFG) */
}


/*******************************************************************************
* Function Name: scb_Enable
****************************************************************************//**
*
*  Enables scb component operation: activates the hardware and 
*  internal interrupt. It also restores TX interrupt sources disabled after the 
*  scb_Stop() function was called (note that level-triggered TX 
*  interrupt sources remain disabled to not cause code lock-up).
*  For I2C and EZI2C modes the interrupt is internal and mandatory for 
*  operation. For SPI and UART modes the interrupt can be configured as none, 
*  internal or external.
*  The scb configuration should be not changed when the component
*  is enabled. Any configuration changes should be made after disabling the 
*  component.
*  When configuration is set to “Unconfigured scb”, the component 
*  must first be initialized to operate in one of the following configurations: 
*  I2C, SPI, UART or EZ I2C, using the mode-specific initialization API. 
*  Otherwise this function does not enable the component.
*
*******************************************************************************/
void scb_Enable(void)
{
#if (scb_SCB_MODE_UNCONFIG_CONST_CFG)
    /* Enable SCB block, only if it is already configured */
    if (!scb_SCB_MODE_UNCONFIG_RUNTM_CFG)
    {
        scb_CTRL_REG |= scb_CTRL_ENABLED;

        scb_ScbEnableIntr();

        /* Call PostEnable function specific to current operation mode */
        scb_ScbModePostEnable();
    }
#else
    scb_CTRL_REG |= scb_CTRL_ENABLED;

    scb_ScbEnableIntr();

    /* Call PostEnable function specific to current operation mode */
    scb_ScbModePostEnable();
#endif /* (scb_SCB_MODE_UNCONFIG_CONST_CFG) */
}


/*******************************************************************************
* Function Name: scb_Start
****************************************************************************//**
*
*  Invokes scb_Init() and scb_Enable().
*  After this function call, the component is enabled and ready for operation.
*  When configuration is set to "Unconfigured SCB", the component must first be
*  initialized to operate in one of the following configurations: I2C, SPI, UART
*  or EZI2C. Otherwise this function does not enable the component.
*
* \globalvars
*  scb_initVar - used to check initial configuration, modified
*  on first function call.
*
*******************************************************************************/
void scb_Start(void)
{
    if (0u == scb_initVar)
    {
        scb_Init();
        scb_initVar = 1u; /* Component was initialized */
    }

    scb_Enable();
}


/*******************************************************************************
* Function Name: scb_Stop
****************************************************************************//**
*
*  Disables the scb component: disable the hardware and internal 
*  interrupt. It also disables all TX interrupt sources so as not to cause an 
*  unexpected interrupt trigger because after the component is enabled, the 
*  TX FIFO is empty.
*  Refer to the function scb_Enable() for the interrupt 
*  configuration details.
*  This function disables the SCB component without checking to see if 
*  communication is in progress. Before calling this function it may be 
*  necessary to check the status of communication to make sure communication 
*  is complete. If this is not done then communication could be stopped mid 
*  byte and corrupted data could result.
*
*******************************************************************************/
void scb_Stop(void)
{
#if (scb_SCB_IRQ_INTERNAL)
    scb_DisableInt();
#endif /* (scb_SCB_IRQ_INTERNAL) */

    /* Call Stop function specific to current operation mode */
    scb_ScbModeStop();

    /* Disable SCB IP */
    scb_CTRL_REG &= (uint32) ~scb_CTRL_ENABLED;

    /* Disable all TX interrupt sources so as not to cause an unexpected
    * interrupt trigger after the component will be enabled because the 
    * TX FIFO is empty.
    * For SCB IP v0, it is critical as it does not mask-out interrupt
    * sources when it is disabled. This can cause a code lock-up in the
    * interrupt handler because TX FIFO cannot be loaded after the block
    * is disabled.
    */
    scb_SetTxInterruptMode(scb_NO_INTR_SOURCES);

#if (scb_SCB_IRQ_INTERNAL)
    scb_ClearPendingInt();
#endif /* (scb_SCB_IRQ_INTERNAL) */
}


/*******************************************************************************
* Function Name: scb_SetRxFifoLevel
****************************************************************************//**
*
*  Sets level in the RX FIFO to generate a RX level interrupt.
*  When the RX FIFO has more entries than the RX FIFO level an RX level
*  interrupt request is generated.
*
*  \param level: Level in the RX FIFO to generate RX level interrupt.
*   The range of valid level values is between 0 and RX FIFO depth - 1.
*
*******************************************************************************/
void scb_SetRxFifoLevel(uint32 level)
{
    uint32 rxFifoCtrl;

    rxFifoCtrl = scb_RX_FIFO_CTRL_REG;

    rxFifoCtrl &= ((uint32) ~scb_RX_FIFO_CTRL_TRIGGER_LEVEL_MASK); /* Clear level mask bits */
    rxFifoCtrl |= ((uint32) (scb_RX_FIFO_CTRL_TRIGGER_LEVEL_MASK & level));

    scb_RX_FIFO_CTRL_REG = rxFifoCtrl;
}


/*******************************************************************************
* Function Name: scb_SetTxFifoLevel
****************************************************************************//**
*
*  Sets level in the TX FIFO to generate a TX level interrupt.
*  When the TX FIFO has less entries than the TX FIFO level an TX level
*  interrupt request is generated.
*
*  \param level: Level in the TX FIFO to generate TX level interrupt.
*   The range of valid level values is between 0 and TX FIFO depth - 1.
*
*******************************************************************************/
void scb_SetTxFifoLevel(uint32 level)
{
    uint32 txFifoCtrl;

    txFifoCtrl = scb_TX_FIFO_CTRL_REG;

    txFifoCtrl &= ((uint32) ~scb_TX_FIFO_CTRL_TRIGGER_LEVEL_MASK); /* Clear level mask bits */
    txFifoCtrl |= ((uint32) (scb_TX_FIFO_CTRL_TRIGGER_LEVEL_MASK & level));

    scb_TX_FIFO_CTRL_REG = txFifoCtrl;
}


#if (scb_SCB_IRQ_INTERNAL)
    /*******************************************************************************
    * Function Name: scb_SetCustomInterruptHandler
    ****************************************************************************//**
    *
    *  Registers a function to be called by the internal interrupt handler.
    *  First the function that is registered is called, then the internal interrupt
    *  handler performs any operation such as software buffer management functions
    *  before the interrupt returns.  It is the user's responsibility not to break
    *  the software buffer operations. Only one custom handler is supported, which
    *  is the function provided by the most recent call.
    *  At the initialization time no custom handler is registered.
    *
    *  \param func: Pointer to the function to register.
    *        The value NULL indicates to remove the current custom interrupt
    *        handler.
    *
    *******************************************************************************/
    void scb_SetCustomInterruptHandler(void (*func)(void))
    {
    #if !defined (CY_REMOVE_scb_CUSTOM_INTR_HANDLER)
        scb_customIntrHandler = func; /* Register interrupt handler */
    #else
        if (NULL != func)
        {
            /* Suppress compiler warning */
        }
    #endif /* !defined (CY_REMOVE_scb_CUSTOM_INTR_HANDLER) */
    }
#endif /* (scb_SCB_IRQ_INTERNAL) */


/*******************************************************************************
* Function Name: scb_ScbModeEnableIntr
****************************************************************************//**
*
*  Enables an interrupt for a specific mode.
*
*******************************************************************************/
static void scb_ScbEnableIntr(void)
{
#if (scb_SCB_IRQ_INTERNAL)
    /* Enable interrupt in NVIC */
    #if (scb_SCB_MODE_UNCONFIG_CONST_CFG)
        if (0u != scb_scbEnableIntr)
        {
            scb_EnableInt();
        }

    #else
        scb_EnableInt();

    #endif /* (scb_SCB_MODE_UNCONFIG_CONST_CFG) */
#endif /* (scb_SCB_IRQ_INTERNAL) */
}


/*******************************************************************************
* Function Name: scb_ScbModePostEnable
****************************************************************************//**
*
*  Calls the PostEnable function for a specific operation mode.
*
*******************************************************************************/
static void scb_ScbModePostEnable(void)
{
#if (scb_SCB_MODE_UNCONFIG_CONST_CFG)
#if (!scb_CY_SCBIP_V1)
    if (scb_SCB_MODE_SPI_RUNTM_CFG)
    {
        scb_SpiPostEnable();
    }
    else if (scb_SCB_MODE_UART_RUNTM_CFG)
    {
        scb_UartPostEnable();
    }
    else
    {
        /* Unknown mode: do nothing */
    }
#endif /* (!scb_CY_SCBIP_V1) */

#elif (scb_SCB_MODE_SPI_CONST_CFG)
    scb_SpiPostEnable();

#elif (scb_SCB_MODE_UART_CONST_CFG)
    scb_UartPostEnable();

#else
    /* Unknown mode: do nothing */
#endif /* (scb_SCB_MODE_UNCONFIG_CONST_CFG) */
}


/*******************************************************************************
* Function Name: scb_ScbModeStop
****************************************************************************//**
*
*  Calls the Stop function for a specific operation mode.
*
*******************************************************************************/
static void scb_ScbModeStop(void)
{
#if (scb_SCB_MODE_UNCONFIG_CONST_CFG)
    if (scb_SCB_MODE_I2C_RUNTM_CFG)
    {
        scb_I2CStop();
    }
    else if (scb_SCB_MODE_EZI2C_RUNTM_CFG)
    {
        scb_EzI2CStop();
    }
#if (!scb_CY_SCBIP_V1)
    else if (scb_SCB_MODE_SPI_RUNTM_CFG)
    {
        scb_SpiStop();
    }
    else if (scb_SCB_MODE_UART_RUNTM_CFG)
    {
        scb_UartStop();
    }
#endif /* (!scb_CY_SCBIP_V1) */
    else
    {
        /* Unknown mode: do nothing */
    }
#elif (scb_SCB_MODE_I2C_CONST_CFG)
    scb_I2CStop();

#elif (scb_SCB_MODE_EZI2C_CONST_CFG)
    scb_EzI2CStop();

#elif (scb_SCB_MODE_SPI_CONST_CFG)
    scb_SpiStop();

#elif (scb_SCB_MODE_UART_CONST_CFG)
    scb_UartStop();

#else
    /* Unknown mode: do nothing */
#endif /* (scb_SCB_MODE_UNCONFIG_CONST_CFG) */
}


#if (scb_SCB_MODE_UNCONFIG_CONST_CFG)
    /*******************************************************************************
    * Function Name: scb_SetPins
    ****************************************************************************//**
    *
    *  Sets the pins settings accordingly to the selected operation mode.
    *  Only available in the Unconfigured operation mode. The mode specific
    *  initialization function calls it.
    *  Pins configuration is set by PSoC Creator when a specific mode of operation
    *  is selected in design time.
    *
    *  \param mode:      Mode of SCB operation.
    *  \param subMode:   Sub-mode of SCB operation. It is only required for SPI and UART
    *             modes.
    *  \param uartEnableMask: enables TX or RX direction and RTS and CTS signals.
    *
    *******************************************************************************/
    void scb_SetPins(uint32 mode, uint32 subMode, uint32 uartEnableMask)
    {
        uint32 pinsDm[scb_SCB_PINS_NUMBER];
        uint32 i;
        
    #if (!scb_CY_SCBIP_V1)
        uint32 pinsInBuf = 0u;
    #endif /* (!scb_CY_SCBIP_V1) */
        
        uint32 hsiomSel[scb_SCB_PINS_NUMBER] = 
        {
            scb_RX_SDA_MOSI_HSIOM_SEL_GPIO,
            scb_TX_SCL_MISO_HSIOM_SEL_GPIO,
            0u,
            0u,
            0u,
            0u,
            0u,
        };

    #if (scb_CY_SCBIP_V1)
        /* Supress compiler warning. */
        if ((0u == subMode) || (0u == uartEnableMask))
        {
        }
    #endif /* (scb_CY_SCBIP_V1) */

        /* Set default HSIOM to GPIO and Drive Mode to Analog Hi-Z */
        for (i = 0u; i < scb_SCB_PINS_NUMBER; i++)
        {
            pinsDm[i] = scb_PIN_DM_ALG_HIZ;
        }

        if ((scb_SCB_MODE_I2C   == mode) ||
            (scb_SCB_MODE_EZI2C == mode))
        {
        #if (scb_RX_SDA_MOSI_PIN)
            hsiomSel[scb_RX_SDA_MOSI_PIN_INDEX] = scb_RX_SDA_MOSI_HSIOM_SEL_I2C;
            pinsDm  [scb_RX_SDA_MOSI_PIN_INDEX] = scb_PIN_DM_OD_LO;
        #elif (scb_RX_WAKE_SDA_MOSI_PIN)
            hsiomSel[scb_RX_WAKE_SDA_MOSI_PIN_INDEX] = scb_RX_WAKE_SDA_MOSI_HSIOM_SEL_I2C;
            pinsDm  [scb_RX_WAKE_SDA_MOSI_PIN_INDEX] = scb_PIN_DM_OD_LO;
        #else
        #endif /* (scb_RX_SDA_MOSI_PIN) */
        
        #if (scb_TX_SCL_MISO_PIN)
            hsiomSel[scb_TX_SCL_MISO_PIN_INDEX] = scb_TX_SCL_MISO_HSIOM_SEL_I2C;
            pinsDm  [scb_TX_SCL_MISO_PIN_INDEX] = scb_PIN_DM_OD_LO;
        #endif /* (scb_TX_SCL_MISO_PIN) */
        }
    #if (!scb_CY_SCBIP_V1)
        else if (scb_SCB_MODE_SPI == mode)
        {
        #if (scb_RX_SDA_MOSI_PIN)
            hsiomSel[scb_RX_SDA_MOSI_PIN_INDEX] = scb_RX_SDA_MOSI_HSIOM_SEL_SPI;
        #elif (scb_RX_WAKE_SDA_MOSI_PIN)
            hsiomSel[scb_RX_WAKE_SDA_MOSI_PIN_INDEX] = scb_RX_WAKE_SDA_MOSI_HSIOM_SEL_SPI;
        #else
        #endif /* (scb_RX_SDA_MOSI_PIN) */
        
        #if (scb_TX_SCL_MISO_PIN)
            hsiomSel[scb_TX_SCL_MISO_PIN_INDEX] = scb_TX_SCL_MISO_HSIOM_SEL_SPI;
        #endif /* (scb_TX_SCL_MISO_PIN) */
        
        #if (scb_CTS_SCLK_PIN)
            hsiomSel[scb_CTS_SCLK_PIN_INDEX] = scb_CTS_SCLK_HSIOM_SEL_SPI;
        #endif /* (scb_CTS_SCLK_PIN) */

            if (scb_SPI_SLAVE == subMode)
            {
                /* Slave */
                pinsDm[scb_RX_SDA_MOSI_PIN_INDEX] = scb_PIN_DM_DIG_HIZ;
                pinsDm[scb_TX_SCL_MISO_PIN_INDEX] = scb_PIN_DM_STRONG;
                pinsDm[scb_CTS_SCLK_PIN_INDEX] = scb_PIN_DM_DIG_HIZ;

            #if (scb_RTS_SS0_PIN)
                /* Only SS0 is valid choice for Slave */
                hsiomSel[scb_RTS_SS0_PIN_INDEX] = scb_RTS_SS0_HSIOM_SEL_SPI;
                pinsDm  [scb_RTS_SS0_PIN_INDEX] = scb_PIN_DM_DIG_HIZ;
            #endif /* (scb_RTS_SS0_PIN) */

            #if (scb_TX_SCL_MISO_PIN)
                /* Disable input buffer */
                 pinsInBuf |= scb_TX_SCL_MISO_PIN_MASK;
            #endif /* (scb_TX_SCL_MISO_PIN) */
            }
            else 
            {
                /* (Master) */
                pinsDm[scb_RX_SDA_MOSI_PIN_INDEX] = scb_PIN_DM_STRONG;
                pinsDm[scb_TX_SCL_MISO_PIN_INDEX] = scb_PIN_DM_DIG_HIZ;
                pinsDm[scb_CTS_SCLK_PIN_INDEX] = scb_PIN_DM_STRONG;

            #if (scb_RTS_SS0_PIN)
                hsiomSel [scb_RTS_SS0_PIN_INDEX] = scb_RTS_SS0_HSIOM_SEL_SPI;
                pinsDm   [scb_RTS_SS0_PIN_INDEX] = scb_PIN_DM_STRONG;
                pinsInBuf |= scb_RTS_SS0_PIN_MASK;
            #endif /* (scb_RTS_SS0_PIN) */

            #if (scb_SS1_PIN)
                hsiomSel [scb_SS1_PIN_INDEX] = scb_SS1_HSIOM_SEL_SPI;
                pinsDm   [scb_SS1_PIN_INDEX] = scb_PIN_DM_STRONG;
                pinsInBuf |= scb_SS1_PIN_MASK;
            #endif /* (scb_SS1_PIN) */

            #if (scb_SS2_PIN)
                hsiomSel [scb_SS2_PIN_INDEX] = scb_SS2_HSIOM_SEL_SPI;
                pinsDm   [scb_SS2_PIN_INDEX] = scb_PIN_DM_STRONG;
                pinsInBuf |= scb_SS2_PIN_MASK;
            #endif /* (scb_SS2_PIN) */

            #if (scb_SS3_PIN)
                hsiomSel [scb_SS3_PIN_INDEX] = scb_SS3_HSIOM_SEL_SPI;
                pinsDm   [scb_SS3_PIN_INDEX] = scb_PIN_DM_STRONG;
                pinsInBuf |= scb_SS3_PIN_MASK;
            #endif /* (scb_SS3_PIN) */

                /* Disable input buffers */
            #if (scb_RX_SDA_MOSI_PIN)
                pinsInBuf |= scb_RX_SDA_MOSI_PIN_MASK;
            #elif (scb_RX_WAKE_SDA_MOSI_PIN)
                pinsInBuf |= scb_RX_WAKE_SDA_MOSI_PIN_MASK;
            #else
            #endif /* (scb_RX_SDA_MOSI_PIN) */

            #if (scb_CTS_SCLK_PIN)
                pinsInBuf |= scb_CTS_SCLK_PIN_MASK;
            #endif /* (scb_CTS_SCLK_PIN) */
            }
        }
        else /* UART */
        {
            if (scb_UART_MODE_SMARTCARD == subMode)
            {
                /* SmartCard */
            #if (scb_TX_SCL_MISO_PIN)
                hsiomSel[scb_TX_SCL_MISO_PIN_INDEX] = scb_TX_SCL_MISO_HSIOM_SEL_UART;
                pinsDm  [scb_TX_SCL_MISO_PIN_INDEX] = scb_PIN_DM_OD_LO;
            #endif /* (scb_TX_SCL_MISO_PIN) */
            }
            else /* Standard or IrDA */
            {
                if (0u != (scb_UART_RX_PIN_ENABLE & uartEnableMask))
                {
                #if (scb_RX_SDA_MOSI_PIN)
                    hsiomSel[scb_RX_SDA_MOSI_PIN_INDEX] = scb_RX_SDA_MOSI_HSIOM_SEL_UART;
                    pinsDm  [scb_RX_SDA_MOSI_PIN_INDEX] = scb_PIN_DM_DIG_HIZ;
                #elif (scb_RX_WAKE_SDA_MOSI_PIN)
                    hsiomSel[scb_RX_WAKE_SDA_MOSI_PIN_INDEX] = scb_RX_WAKE_SDA_MOSI_HSIOM_SEL_UART;
                    pinsDm  [scb_RX_WAKE_SDA_MOSI_PIN_INDEX] = scb_PIN_DM_DIG_HIZ;
                #else
                #endif /* (scb_RX_SDA_MOSI_PIN) */
                }

                if (0u != (scb_UART_TX_PIN_ENABLE & uartEnableMask))
                {
                #if (scb_TX_SCL_MISO_PIN)
                    hsiomSel[scb_TX_SCL_MISO_PIN_INDEX] = scb_TX_SCL_MISO_HSIOM_SEL_UART;
                    pinsDm  [scb_TX_SCL_MISO_PIN_INDEX] = scb_PIN_DM_STRONG;
                    
                    /* Disable input buffer */
                    pinsInBuf |= scb_TX_SCL_MISO_PIN_MASK;
                #endif /* (scb_TX_SCL_MISO_PIN) */
                }

            #if !(scb_CY_SCBIP_V0 || scb_CY_SCBIP_V1)
                if (scb_UART_MODE_STD == subMode)
                {
                    if (0u != (scb_UART_CTS_PIN_ENABLE & uartEnableMask))
                    {
                        /* CTS input is multiplexed with SCLK */
                    #if (scb_CTS_SCLK_PIN)
                        hsiomSel[scb_CTS_SCLK_PIN_INDEX] = scb_CTS_SCLK_HSIOM_SEL_UART;
                        pinsDm  [scb_CTS_SCLK_PIN_INDEX] = scb_PIN_DM_DIG_HIZ;
                    #endif /* (scb_CTS_SCLK_PIN) */
                    }

                    if (0u != (scb_UART_RTS_PIN_ENABLE & uartEnableMask))
                    {
                        /* RTS output is multiplexed with SS0 */
                    #if (scb_RTS_SS0_PIN)
                        hsiomSel[scb_RTS_SS0_PIN_INDEX] = scb_RTS_SS0_HSIOM_SEL_UART;
                        pinsDm  [scb_RTS_SS0_PIN_INDEX] = scb_PIN_DM_STRONG;
                        
                        /* Disable input buffer */
                        pinsInBuf |= scb_RTS_SS0_PIN_MASK;
                    #endif /* (scb_RTS_SS0_PIN) */
                    }
                }
            #endif /* !(scb_CY_SCBIP_V0 || scb_CY_SCBIP_V1) */
            }
        }
    #endif /* (!scb_CY_SCBIP_V1) */

    /* Configure pins: set HSIOM, DM and InputBufEnable */
    /* Note: the DR register settings do not effect the pin output if HSIOM is other than GPIO */

    #if (scb_RX_SDA_MOSI_PIN)
        scb_SET_HSIOM_SEL(scb_RX_SDA_MOSI_HSIOM_REG,
                                       scb_RX_SDA_MOSI_HSIOM_MASK,
                                       scb_RX_SDA_MOSI_HSIOM_POS,
                                        hsiomSel[scb_RX_SDA_MOSI_PIN_INDEX]);

        scb_uart_rx_i2c_sda_spi_mosi_SetDriveMode((uint8) pinsDm[scb_RX_SDA_MOSI_PIN_INDEX]);

        #if (!scb_CY_SCBIP_V1)
            scb_SET_INP_DIS(scb_uart_rx_i2c_sda_spi_mosi_INP_DIS,
                                         scb_uart_rx_i2c_sda_spi_mosi_MASK,
                                         (0u != (pinsInBuf & scb_RX_SDA_MOSI_PIN_MASK)));
        #endif /* (!scb_CY_SCBIP_V1) */
    
    #elif (scb_RX_WAKE_SDA_MOSI_PIN)
        scb_SET_HSIOM_SEL(scb_RX_WAKE_SDA_MOSI_HSIOM_REG,
                                       scb_RX_WAKE_SDA_MOSI_HSIOM_MASK,
                                       scb_RX_WAKE_SDA_MOSI_HSIOM_POS,
                                       hsiomSel[scb_RX_WAKE_SDA_MOSI_PIN_INDEX]);

        scb_uart_rx_wake_i2c_sda_spi_mosi_SetDriveMode((uint8)
                                                               pinsDm[scb_RX_WAKE_SDA_MOSI_PIN_INDEX]);

        scb_SET_INP_DIS(scb_uart_rx_wake_i2c_sda_spi_mosi_INP_DIS,
                                     scb_uart_rx_wake_i2c_sda_spi_mosi_MASK,
                                     (0u != (pinsInBuf & scb_RX_WAKE_SDA_MOSI_PIN_MASK)));

         /* Set interrupt on falling edge */
        scb_SET_INCFG_TYPE(scb_RX_WAKE_SDA_MOSI_INTCFG_REG,
                                        scb_RX_WAKE_SDA_MOSI_INTCFG_TYPE_MASK,
                                        scb_RX_WAKE_SDA_MOSI_INTCFG_TYPE_POS,
                                        scb_INTCFG_TYPE_FALLING_EDGE);
    #else
    #endif /* (scb_RX_WAKE_SDA_MOSI_PIN) */

    #if (scb_TX_SCL_MISO_PIN)
        scb_SET_HSIOM_SEL(scb_TX_SCL_MISO_HSIOM_REG,
                                       scb_TX_SCL_MISO_HSIOM_MASK,
                                       scb_TX_SCL_MISO_HSIOM_POS,
                                        hsiomSel[scb_TX_SCL_MISO_PIN_INDEX]);

        scb_uart_tx_i2c_scl_spi_miso_SetDriveMode((uint8) pinsDm[scb_TX_SCL_MISO_PIN_INDEX]);

    #if (!scb_CY_SCBIP_V1)
        scb_SET_INP_DIS(scb_uart_tx_i2c_scl_spi_miso_INP_DIS,
                                     scb_uart_tx_i2c_scl_spi_miso_MASK,
                                    (0u != (pinsInBuf & scb_TX_SCL_MISO_PIN_MASK)));
    #endif /* (!scb_CY_SCBIP_V1) */
    #endif /* (scb_RX_SDA_MOSI_PIN) */

    #if (scb_CTS_SCLK_PIN)
        scb_SET_HSIOM_SEL(scb_CTS_SCLK_HSIOM_REG,
                                       scb_CTS_SCLK_HSIOM_MASK,
                                       scb_CTS_SCLK_HSIOM_POS,
                                       hsiomSel[scb_CTS_SCLK_PIN_INDEX]);

        scb_uart_cts_spi_sclk_SetDriveMode((uint8) pinsDm[scb_CTS_SCLK_PIN_INDEX]);

        scb_SET_INP_DIS(scb_uart_cts_spi_sclk_INP_DIS,
                                     scb_uart_cts_spi_sclk_MASK,
                                     (0u != (pinsInBuf & scb_CTS_SCLK_PIN_MASK)));
    #endif /* (scb_CTS_SCLK_PIN) */

    #if (scb_RTS_SS0_PIN)
        scb_SET_HSIOM_SEL(scb_RTS_SS0_HSIOM_REG,
                                       scb_RTS_SS0_HSIOM_MASK,
                                       scb_RTS_SS0_HSIOM_POS,
                                       hsiomSel[scb_RTS_SS0_PIN_INDEX]);

        scb_uart_rts_spi_ss0_SetDriveMode((uint8) pinsDm[scb_RTS_SS0_PIN_INDEX]);

        scb_SET_INP_DIS(scb_uart_rts_spi_ss0_INP_DIS,
                                     scb_uart_rts_spi_ss0_MASK,
                                     (0u != (pinsInBuf & scb_RTS_SS0_PIN_MASK)));
    #endif /* (scb_RTS_SS0_PIN) */

    #if (scb_SS1_PIN)
        scb_SET_HSIOM_SEL(scb_SS1_HSIOM_REG,
                                       scb_SS1_HSIOM_MASK,
                                       scb_SS1_HSIOM_POS,
                                       hsiomSel[scb_SS1_PIN_INDEX]);

        scb_spi_ss1_SetDriveMode((uint8) pinsDm[scb_SS1_PIN_INDEX]);

        scb_SET_INP_DIS(scb_spi_ss1_INP_DIS,
                                     scb_spi_ss1_MASK,
                                     (0u != (pinsInBuf & scb_SS1_PIN_MASK)));
    #endif /* (scb_SS1_PIN) */

    #if (scb_SS2_PIN)
        scb_SET_HSIOM_SEL(scb_SS2_HSIOM_REG,
                                       scb_SS2_HSIOM_MASK,
                                       scb_SS2_HSIOM_POS,
                                       hsiomSel[scb_SS2_PIN_INDEX]);

        scb_spi_ss2_SetDriveMode((uint8) pinsDm[scb_SS2_PIN_INDEX]);

        scb_SET_INP_DIS(scb_spi_ss2_INP_DIS,
                                     scb_spi_ss2_MASK,
                                     (0u != (pinsInBuf & scb_SS2_PIN_MASK)));
    #endif /* (scb_SS2_PIN) */

    #if (scb_SS3_PIN)
        scb_SET_HSIOM_SEL(scb_SS3_HSIOM_REG,
                                       scb_SS3_HSIOM_MASK,
                                       scb_SS3_HSIOM_POS,
                                       hsiomSel[scb_SS3_PIN_INDEX]);

        scb_spi_ss3_SetDriveMode((uint8) pinsDm[scb_SS3_PIN_INDEX]);

        scb_SET_INP_DIS(scb_spi_ss3_INP_DIS,
                                     scb_spi_ss3_MASK,
                                     (0u != (pinsInBuf & scb_SS3_PIN_MASK)));
    #endif /* (scb_SS3_PIN) */
    }

#endif /* (scb_SCB_MODE_UNCONFIG_CONST_CFG) */


#if (scb_CY_SCBIP_V0 || scb_CY_SCBIP_V1)
    /*******************************************************************************
    * Function Name: scb_I2CSlaveNackGeneration
    ****************************************************************************//**
    *
    *  Sets command to generate NACK to the address or data.
    *
    *******************************************************************************/
    void scb_I2CSlaveNackGeneration(void)
    {
        /* Check for EC_AM toggle condition: EC_AM and clock stretching for address are enabled */
        if ((0u != (scb_CTRL_REG & scb_CTRL_EC_AM_MODE)) &&
            (0u == (scb_I2C_CTRL_REG & scb_I2C_CTRL_S_NOT_READY_ADDR_NACK)))
        {
            /* Toggle EC_AM before NACK generation */
            scb_CTRL_REG &= ~scb_CTRL_EC_AM_MODE;
            scb_CTRL_REG |=  scb_CTRL_EC_AM_MODE;
        }

        scb_I2C_SLAVE_CMD_REG = scb_I2C_SLAVE_CMD_S_NACK;
    }
#endif /* (scb_CY_SCBIP_V0 || scb_CY_SCBIP_V1) */


/* [] END OF FILE */
