/***************************************************************************//**
* \file .h
* \version 4.0
*
* \brief
*  This private file provides constants and parameter values for the
*  SCB Component.
*  Please do not use this file or its content in your project.
*
* Note:
*
********************************************************************************
* \copyright
* Copyright 2013-2017, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SCB_PVT_scb_H)
#define CY_SCB_PVT_scb_H

#include "scb.h"


/***************************************
*     Private Function Prototypes
***************************************/

/* APIs to service INTR_I2C_EC register */
#define scb_SetI2CExtClkInterruptMode(interruptMask) scb_WRITE_INTR_I2C_EC_MASK(interruptMask)
#define scb_ClearI2CExtClkInterruptSource(interruptMask) scb_CLEAR_INTR_I2C_EC(interruptMask)
#define scb_GetI2CExtClkInterruptSource()                (scb_INTR_I2C_EC_REG)
#define scb_GetI2CExtClkInterruptMode()                  (scb_INTR_I2C_EC_MASK_REG)
#define scb_GetI2CExtClkInterruptSourceMasked()          (scb_INTR_I2C_EC_MASKED_REG)

#if (!scb_CY_SCBIP_V1)
    /* APIs to service INTR_SPI_EC register */
    #define scb_SetSpiExtClkInterruptMode(interruptMask) \
                                                                scb_WRITE_INTR_SPI_EC_MASK(interruptMask)
    #define scb_ClearSpiExtClkInterruptSource(interruptMask) \
                                                                scb_CLEAR_INTR_SPI_EC(interruptMask)
    #define scb_GetExtSpiClkInterruptSource()                 (scb_INTR_SPI_EC_REG)
    #define scb_GetExtSpiClkInterruptMode()                   (scb_INTR_SPI_EC_MASK_REG)
    #define scb_GetExtSpiClkInterruptSourceMasked()           (scb_INTR_SPI_EC_MASKED_REG)
#endif /* (!scb_CY_SCBIP_V1) */

#if(scb_SCB_MODE_UNCONFIG_CONST_CFG)
    extern void scb_SetPins(uint32 mode, uint32 subMode, uint32 uartEnableMask);
#endif /* (scb_SCB_MODE_UNCONFIG_CONST_CFG) */


/***************************************
*     Vars with External Linkage
***************************************/

#if (scb_SCB_IRQ_INTERNAL)
#if !defined (CY_REMOVE_scb_CUSTOM_INTR_HANDLER)
    extern cyisraddress scb_customIntrHandler;
#endif /* !defined (CY_REMOVE_scb_CUSTOM_INTR_HANDLER) */
#endif /* (scb_SCB_IRQ_INTERNAL) */

extern scb_BACKUP_STRUCT scb_backup;

#if(scb_SCB_MODE_UNCONFIG_CONST_CFG)
    /* Common configuration variables */
    extern uint8 scb_scbMode;
    extern uint8 scb_scbEnableWake;
    extern uint8 scb_scbEnableIntr;

    /* I2C configuration variables */
    extern uint8 scb_mode;
    extern uint8 scb_acceptAddr;

    /* SPI/UART configuration variables */
    extern volatile uint8 * scb_rxBuffer;
    extern uint8   scb_rxDataBits;
    extern uint32  scb_rxBufferSize;

    extern volatile uint8 * scb_txBuffer;
    extern uint8   scb_txDataBits;
    extern uint32  scb_txBufferSize;

    /* EZI2C configuration variables */
    extern uint8 scb_numberOfAddr;
    extern uint8 scb_subAddrSize;
#endif /* (scb_SCB_MODE_UNCONFIG_CONST_CFG) */

#if (! (scb_SCB_MODE_I2C_CONST_CFG || \
        scb_SCB_MODE_EZI2C_CONST_CFG))
    extern uint16 scb_IntrTxMask;
#endif /* (! (scb_SCB_MODE_I2C_CONST_CFG || \
              scb_SCB_MODE_EZI2C_CONST_CFG)) */


/***************************************
*        Conditional Macro
****************************************/

#if(scb_SCB_MODE_UNCONFIG_CONST_CFG)
    /* Defines run time operation mode */
    #define scb_SCB_MODE_I2C_RUNTM_CFG     (scb_SCB_MODE_I2C      == scb_scbMode)
    #define scb_SCB_MODE_SPI_RUNTM_CFG     (scb_SCB_MODE_SPI      == scb_scbMode)
    #define scb_SCB_MODE_UART_RUNTM_CFG    (scb_SCB_MODE_UART     == scb_scbMode)
    #define scb_SCB_MODE_EZI2C_RUNTM_CFG   (scb_SCB_MODE_EZI2C    == scb_scbMode)
    #define scb_SCB_MODE_UNCONFIG_RUNTM_CFG \
                                                        (scb_SCB_MODE_UNCONFIG == scb_scbMode)

    /* Defines wakeup enable */
    #define scb_SCB_WAKE_ENABLE_CHECK       (0u != scb_scbEnableWake)
#endif /* (scb_SCB_MODE_UNCONFIG_CONST_CFG) */

/* Defines maximum number of SCB pins */
#if (!scb_CY_SCBIP_V1)
    #define scb_SCB_PINS_NUMBER    (7u)
#else
    #define scb_SCB_PINS_NUMBER    (2u)
#endif /* (!scb_CY_SCBIP_V1) */

#endif /* (CY_SCB_PVT_scb_H) */


/* [] END OF FILE */
