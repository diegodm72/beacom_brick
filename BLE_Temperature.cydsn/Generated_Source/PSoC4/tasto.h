/*******************************************************************************
* File Name: tasto.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_tasto_H) /* Pins tasto_H */
#define CY_PINS_tasto_H

#include "cytypes.h"
#include "cyfitter.h"
#include "tasto_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} tasto_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   tasto_Read(void);
void    tasto_Write(uint8 value);
uint8   tasto_ReadDataReg(void);
#if defined(tasto__PC) || (CY_PSOC4_4200L) 
    void    tasto_SetDriveMode(uint8 mode);
#endif
void    tasto_SetInterruptMode(uint16 position, uint16 mode);
uint8   tasto_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void tasto_Sleep(void); 
void tasto_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(tasto__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define tasto_DRIVE_MODE_BITS        (3)
    #define tasto_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - tasto_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the tasto_SetDriveMode() function.
         *  @{
         */
        #define tasto_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define tasto_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define tasto_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define tasto_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define tasto_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define tasto_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define tasto_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define tasto_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define tasto_MASK               tasto__MASK
#define tasto_SHIFT              tasto__SHIFT
#define tasto_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in tasto_SetInterruptMode() function.
     *  @{
     */
        #define tasto_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define tasto_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define tasto_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define tasto_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(tasto__SIO)
    #define tasto_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(tasto__PC) && (CY_PSOC4_4200L)
    #define tasto_USBIO_ENABLE               ((uint32)0x80000000u)
    #define tasto_USBIO_DISABLE              ((uint32)(~tasto_USBIO_ENABLE))
    #define tasto_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define tasto_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define tasto_USBIO_ENTER_SLEEP          ((uint32)((1u << tasto_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << tasto_USBIO_SUSPEND_DEL_SHIFT)))
    #define tasto_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << tasto_USBIO_SUSPEND_SHIFT)))
    #define tasto_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << tasto_USBIO_SUSPEND_DEL_SHIFT)))
    #define tasto_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(tasto__PC)
    /* Port Configuration */
    #define tasto_PC                 (* (reg32 *) tasto__PC)
#endif
/* Pin State */
#define tasto_PS                     (* (reg32 *) tasto__PS)
/* Data Register */
#define tasto_DR                     (* (reg32 *) tasto__DR)
/* Input Buffer Disable Override */
#define tasto_INP_DIS                (* (reg32 *) tasto__PC2)

/* Interrupt configuration Registers */
#define tasto_INTCFG                 (* (reg32 *) tasto__INTCFG)
#define tasto_INTSTAT                (* (reg32 *) tasto__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define tasto_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(tasto__SIO)
    #define tasto_SIO_REG            (* (reg32 *) tasto__SIO)
#endif /* (tasto__SIO_CFG) */

/* USBIO registers */
#if !defined(tasto__PC) && (CY_PSOC4_4200L)
    #define tasto_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define tasto_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define tasto_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define tasto_DRIVE_MODE_SHIFT       (0x00u)
#define tasto_DRIVE_MODE_MASK        (0x07u << tasto_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins tasto_H */


/* [] END OF FILE */
