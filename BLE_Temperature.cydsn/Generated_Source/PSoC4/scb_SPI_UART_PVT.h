/***************************************************************************//**
* \file scb_SPI_UART_PVT.h
* \version 4.0
*
* \brief
*  This private file provides constants and parameter values for the
*  SCB Component in SPI and UART modes.
*  Please do not use this file or its content in your project.
*
* Note:
*
********************************************************************************
* \copyright
* Copyright 2013-2017, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SCB_SPI_UART_PVT_scb_H)
#define CY_SCB_SPI_UART_PVT_scb_H

#include "scb_SPI_UART.h"


/***************************************
*     Internal Global Vars
***************************************/

#if (scb_INTERNAL_RX_SW_BUFFER_CONST)
    extern volatile uint32  scb_rxBufferHead;
    extern volatile uint32  scb_rxBufferTail;
    
    /**
    * \addtogroup group_globals
    * @{
    */
    
    /** Sets when internal software receive buffer overflow
     *  was occurred.
    */  
    extern volatile uint8   scb_rxBufferOverflow;
    /** @} globals */
#endif /* (scb_INTERNAL_RX_SW_BUFFER_CONST) */

#if (scb_INTERNAL_TX_SW_BUFFER_CONST)
    extern volatile uint32  scb_txBufferHead;
    extern volatile uint32  scb_txBufferTail;
#endif /* (scb_INTERNAL_TX_SW_BUFFER_CONST) */

#if (scb_INTERNAL_RX_SW_BUFFER)
    extern volatile uint8 scb_rxBufferInternal[scb_INTERNAL_RX_BUFFER_SIZE];
#endif /* (scb_INTERNAL_RX_SW_BUFFER) */

#if (scb_INTERNAL_TX_SW_BUFFER)
    extern volatile uint8 scb_txBufferInternal[scb_TX_BUFFER_SIZE];
#endif /* (scb_INTERNAL_TX_SW_BUFFER) */


/***************************************
*     Private Function Prototypes
***************************************/

void scb_SpiPostEnable(void);
void scb_SpiStop(void);

#if (scb_SCB_MODE_SPI_CONST_CFG)
    void scb_SpiInit(void);
#endif /* (scb_SCB_MODE_SPI_CONST_CFG) */

#if (scb_SPI_WAKE_ENABLE_CONST)
    void scb_SpiSaveConfig(void);
    void scb_SpiRestoreConfig(void);
#endif /* (scb_SPI_WAKE_ENABLE_CONST) */

void scb_UartPostEnable(void);
void scb_UartStop(void);

#if (scb_SCB_MODE_UART_CONST_CFG)
    void scb_UartInit(void);
#endif /* (scb_SCB_MODE_UART_CONST_CFG) */

#if (scb_UART_WAKE_ENABLE_CONST)
    void scb_UartSaveConfig(void);
    void scb_UartRestoreConfig(void);
#endif /* (scb_UART_WAKE_ENABLE_CONST) */


/***************************************
*         UART API Constants
***************************************/

/* UART RX and TX position to be used in scb_SetPins() */
#define scb_UART_RX_PIN_ENABLE    (scb_UART_RX)
#define scb_UART_TX_PIN_ENABLE    (scb_UART_TX)

/* UART RTS and CTS position to be used in  scb_SetPins() */
#define scb_UART_RTS_PIN_ENABLE    (0x10u)
#define scb_UART_CTS_PIN_ENABLE    (0x20u)


/***************************************
* The following code is DEPRECATED and
* must not be used.
***************************************/

/* Interrupt processing */
#define scb_SpiUartEnableIntRx(intSourceMask)  scb_SetRxInterruptMode(intSourceMask)
#define scb_SpiUartEnableIntTx(intSourceMask)  scb_SetTxInterruptMode(intSourceMask)
uint32  scb_SpiUartDisableIntRx(void);
uint32  scb_SpiUartDisableIntTx(void);


#endif /* (CY_SCB_SPI_UART_PVT_scb_H) */


/* [] END OF FILE */
