/***************************************************************************//**
* \file scb_PM.c
* \version 4.0
*
* \brief
*  This file provides the source code to the Power Management support for
*  the SCB Component.
*
* Note:
*
********************************************************************************
* \copyright
* Copyright 2013-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "scb.h"
#include "scb_PVT.h"

#if(scb_SCB_MODE_I2C_INC)
    #include "scb_I2C_PVT.h"
#endif /* (scb_SCB_MODE_I2C_INC) */

#if(scb_SCB_MODE_EZI2C_INC)
    #include "scb_EZI2C_PVT.h"
#endif /* (scb_SCB_MODE_EZI2C_INC) */

#if(scb_SCB_MODE_SPI_INC || scb_SCB_MODE_UART_INC)
    #include "scb_SPI_UART_PVT.h"
#endif /* (scb_SCB_MODE_SPI_INC || scb_SCB_MODE_UART_INC) */


/***************************************
*   Backup Structure declaration
***************************************/

#if(scb_SCB_MODE_UNCONFIG_CONST_CFG || \
   (scb_SCB_MODE_I2C_CONST_CFG   && (!scb_I2C_WAKE_ENABLE_CONST))   || \
   (scb_SCB_MODE_EZI2C_CONST_CFG && (!scb_EZI2C_WAKE_ENABLE_CONST)) || \
   (scb_SCB_MODE_SPI_CONST_CFG   && (!scb_SPI_WAKE_ENABLE_CONST))   || \
   (scb_SCB_MODE_UART_CONST_CFG  && (!scb_UART_WAKE_ENABLE_CONST)))

    scb_BACKUP_STRUCT scb_backup =
    {
        0u, /* enableState */
    };
#endif


/*******************************************************************************
* Function Name: scb_Sleep
****************************************************************************//**
*
*  Prepares the scb component to enter Deep Sleep.
*  The “Enable wakeup from Deep Sleep Mode” selection has an influence on this 
*  function implementation:
*  - Checked: configures the component to be wakeup source from Deep Sleep.
*  - Unchecked: stores the current component state (enabled or disabled) and 
*    disables the component. See SCB_Stop() function for details about component 
*    disabling.
*
*  Call the scb_Sleep() function before calling the 
*  CyPmSysDeepSleep() function. 
*  Refer to the PSoC Creator System Reference Guide for more information about 
*  power management functions and Low power section of this document for the 
*  selected mode.
*
*  This function should not be called before entering Sleep.
*
*******************************************************************************/
void scb_Sleep(void)
{
#if(scb_SCB_MODE_UNCONFIG_CONST_CFG)

    if(scb_SCB_WAKE_ENABLE_CHECK)
    {
        if(scb_SCB_MODE_I2C_RUNTM_CFG)
        {
            scb_I2CSaveConfig();
        }
        else if(scb_SCB_MODE_EZI2C_RUNTM_CFG)
        {
            scb_EzI2CSaveConfig();
        }
    #if(!scb_CY_SCBIP_V1)
        else if(scb_SCB_MODE_SPI_RUNTM_CFG)
        {
            scb_SpiSaveConfig();
        }
        else if(scb_SCB_MODE_UART_RUNTM_CFG)
        {
            scb_UartSaveConfig();
        }
    #endif /* (!scb_CY_SCBIP_V1) */
        else
        {
            /* Unknown mode */
        }
    }
    else
    {
        scb_backup.enableState = (uint8) scb_GET_CTRL_ENABLED;

        if(0u != scb_backup.enableState)
        {
            scb_Stop();
        }
    }

#else

    #if (scb_SCB_MODE_I2C_CONST_CFG && scb_I2C_WAKE_ENABLE_CONST)
        scb_I2CSaveConfig();

    #elif (scb_SCB_MODE_EZI2C_CONST_CFG && scb_EZI2C_WAKE_ENABLE_CONST)
        scb_EzI2CSaveConfig();

    #elif (scb_SCB_MODE_SPI_CONST_CFG && scb_SPI_WAKE_ENABLE_CONST)
        scb_SpiSaveConfig();

    #elif (scb_SCB_MODE_UART_CONST_CFG && scb_UART_WAKE_ENABLE_CONST)
        scb_UartSaveConfig();

    #else

        scb_backup.enableState = (uint8) scb_GET_CTRL_ENABLED;

        if(0u != scb_backup.enableState)
        {
            scb_Stop();
        }

    #endif /* defined (scb_SCB_MODE_I2C_CONST_CFG) && (scb_I2C_WAKE_ENABLE_CONST) */

#endif /* (scb_SCB_MODE_UNCONFIG_CONST_CFG) */
}


/*******************************************************************************
* Function Name: scb_Wakeup
****************************************************************************//**
*
*  Prepares the scb component for Active mode operation after 
*  Deep Sleep.
*  The “Enable wakeup from Deep Sleep Mode” selection has influence on this 
*  function implementation:
*  - Checked: restores the component Active mode configuration.
*  - Unchecked: enables the component if it was enabled before enter Deep Sleep.
*
*  This function should not be called after exiting Sleep.
*
*  \sideeffect
*   Calling the scb_Wakeup() function without first calling the 
*   scb_Sleep() function may produce unexpected behavior.
*
*******************************************************************************/
void scb_Wakeup(void)
{
#if(scb_SCB_MODE_UNCONFIG_CONST_CFG)

    if(scb_SCB_WAKE_ENABLE_CHECK)
    {
        if(scb_SCB_MODE_I2C_RUNTM_CFG)
        {
            scb_I2CRestoreConfig();
        }
        else if(scb_SCB_MODE_EZI2C_RUNTM_CFG)
        {
            scb_EzI2CRestoreConfig();
        }
    #if(!scb_CY_SCBIP_V1)
        else if(scb_SCB_MODE_SPI_RUNTM_CFG)
        {
            scb_SpiRestoreConfig();
        }
        else if(scb_SCB_MODE_UART_RUNTM_CFG)
        {
            scb_UartRestoreConfig();
        }
    #endif /* (!scb_CY_SCBIP_V1) */
        else
        {
            /* Unknown mode */
        }
    }
    else
    {
        if(0u != scb_backup.enableState)
        {
            scb_Enable();
        }
    }

#else

    #if (scb_SCB_MODE_I2C_CONST_CFG  && scb_I2C_WAKE_ENABLE_CONST)
        scb_I2CRestoreConfig();

    #elif (scb_SCB_MODE_EZI2C_CONST_CFG && scb_EZI2C_WAKE_ENABLE_CONST)
        scb_EzI2CRestoreConfig();

    #elif (scb_SCB_MODE_SPI_CONST_CFG && scb_SPI_WAKE_ENABLE_CONST)
        scb_SpiRestoreConfig();

    #elif (scb_SCB_MODE_UART_CONST_CFG && scb_UART_WAKE_ENABLE_CONST)
        scb_UartRestoreConfig();

    #else

        if(0u != scb_backup.enableState)
        {
            scb_Enable();
        }

    #endif /* (scb_I2C_WAKE_ENABLE_CONST) */

#endif /* (scb_SCB_MODE_UNCONFIG_CONST_CFG) */
}


/* [] END OF FILE */
