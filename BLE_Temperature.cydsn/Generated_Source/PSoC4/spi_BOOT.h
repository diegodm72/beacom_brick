/***************************************************************************//**
* \file spi_BOOT.h
* \version 4.0
*
* \brief
*  This file provides constants and parameter values of the bootloader
*  communication APIs for the SCB Component.
*
* Note:
*
********************************************************************************
* \copyright
* Copyright 2014-2017, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SCB_BOOT_spi_H)
#define CY_SCB_BOOT_spi_H

#include "spi_PVT.h"

#if (spi_SCB_MODE_I2C_INC)
    #include "spi_I2C.h"
#endif /* (spi_SCB_MODE_I2C_INC) */

#if (spi_SCB_MODE_EZI2C_INC)
    #include "spi_EZI2C.h"
#endif /* (spi_SCB_MODE_EZI2C_INC) */

#if (spi_SCB_MODE_SPI_INC || spi_SCB_MODE_UART_INC)
    #include "spi_SPI_UART.h"
#endif /* (spi_SCB_MODE_SPI_INC || spi_SCB_MODE_UART_INC) */


/***************************************
*  Conditional Compilation Parameters
****************************************/

/* Bootloader communication interface enable */
#define spi_BTLDR_COMM_ENABLED ((CYDEV_BOOTLOADER_IO_COMP == CyBtldr_spi) || \
                                             (CYDEV_BOOTLOADER_IO_COMP == CyBtldr_Custom_Interface))

/* Enable I2C bootloader communication */
#if (spi_SCB_MODE_I2C_INC)
    #define spi_I2C_BTLDR_COMM_ENABLED     (spi_BTLDR_COMM_ENABLED && \
                                                            (spi_SCB_MODE_UNCONFIG_CONST_CFG || \
                                                             spi_I2C_SLAVE_CONST))
#else
     #define spi_I2C_BTLDR_COMM_ENABLED    (0u)
#endif /* (spi_SCB_MODE_I2C_INC) */

/* EZI2C does not support bootloader communication. Provide empty APIs */
#if (spi_SCB_MODE_EZI2C_INC)
    #define spi_EZI2C_BTLDR_COMM_ENABLED   (spi_BTLDR_COMM_ENABLED && \
                                                         spi_SCB_MODE_UNCONFIG_CONST_CFG)
#else
    #define spi_EZI2C_BTLDR_COMM_ENABLED   (0u)
#endif /* (spi_EZI2C_BTLDR_COMM_ENABLED) */

/* Enable SPI bootloader communication */
#if (spi_SCB_MODE_SPI_INC)
    #define spi_SPI_BTLDR_COMM_ENABLED     (spi_BTLDR_COMM_ENABLED && \
                                                            (spi_SCB_MODE_UNCONFIG_CONST_CFG || \
                                                             spi_SPI_SLAVE_CONST))
#else
        #define spi_SPI_BTLDR_COMM_ENABLED (0u)
#endif /* (spi_SPI_BTLDR_COMM_ENABLED) */

/* Enable UART bootloader communication */
#if (spi_SCB_MODE_UART_INC)
       #define spi_UART_BTLDR_COMM_ENABLED    (spi_BTLDR_COMM_ENABLED && \
                                                            (spi_SCB_MODE_UNCONFIG_CONST_CFG || \
                                                             (spi_UART_RX_DIRECTION && \
                                                              spi_UART_TX_DIRECTION)))
#else
     #define spi_UART_BTLDR_COMM_ENABLED   (0u)
#endif /* (spi_UART_BTLDR_COMM_ENABLED) */

/* Enable bootloader communication */
#define spi_BTLDR_COMM_MODE_ENABLED    (spi_I2C_BTLDR_COMM_ENABLED   || \
                                                     spi_SPI_BTLDR_COMM_ENABLED   || \
                                                     spi_EZI2C_BTLDR_COMM_ENABLED || \
                                                     spi_UART_BTLDR_COMM_ENABLED)


/***************************************
*        Function Prototypes
***************************************/

#if defined(CYDEV_BOOTLOADER_IO_COMP) && (spi_I2C_BTLDR_COMM_ENABLED)
    /* I2C Bootloader physical layer functions */
    void spi_I2CCyBtldrCommStart(void);
    void spi_I2CCyBtldrCommStop (void);
    void spi_I2CCyBtldrCommReset(void);
    cystatus spi_I2CCyBtldrCommRead       (uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
    cystatus spi_I2CCyBtldrCommWrite(const uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);

    /* Map I2C specific bootloader communication APIs to SCB specific APIs */
    #if (spi_SCB_MODE_I2C_CONST_CFG)
        #define spi_CyBtldrCommStart   spi_I2CCyBtldrCommStart
        #define spi_CyBtldrCommStop    spi_I2CCyBtldrCommStop
        #define spi_CyBtldrCommReset   spi_I2CCyBtldrCommReset
        #define spi_CyBtldrCommRead    spi_I2CCyBtldrCommRead
        #define spi_CyBtldrCommWrite   spi_I2CCyBtldrCommWrite
    #endif /* (spi_SCB_MODE_I2C_CONST_CFG) */

#endif /* defined(CYDEV_BOOTLOADER_IO_COMP) && (spi_I2C_BTLDR_COMM_ENABLED) */


#if defined(CYDEV_BOOTLOADER_IO_COMP) && (spi_EZI2C_BTLDR_COMM_ENABLED)
    /* Bootloader physical layer functions */
    void spi_EzI2CCyBtldrCommStart(void);
    void spi_EzI2CCyBtldrCommStop (void);
    void spi_EzI2CCyBtldrCommReset(void);
    cystatus spi_EzI2CCyBtldrCommRead       (uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
    cystatus spi_EzI2CCyBtldrCommWrite(const uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);

    /* Map EZI2C specific bootloader communication APIs to SCB specific APIs */
    #if (spi_SCB_MODE_EZI2C_CONST_CFG)
        #define spi_CyBtldrCommStart   spi_EzI2CCyBtldrCommStart
        #define spi_CyBtldrCommStop    spi_EzI2CCyBtldrCommStop
        #define spi_CyBtldrCommReset   spi_EzI2CCyBtldrCommReset
        #define spi_CyBtldrCommRead    spi_EzI2CCyBtldrCommRead
        #define spi_CyBtldrCommWrite   spi_EzI2CCyBtldrCommWrite
    #endif /* (spi_SCB_MODE_EZI2C_CONST_CFG) */

#endif /* defined(CYDEV_BOOTLOADER_IO_COMP) && (spi_EZI2C_BTLDR_COMM_ENABLED) */

#if defined(CYDEV_BOOTLOADER_IO_COMP) && (spi_SPI_BTLDR_COMM_ENABLED)
    /* SPI Bootloader physical layer functions */
    void spi_SpiCyBtldrCommStart(void);
    void spi_SpiCyBtldrCommStop (void);
    void spi_SpiCyBtldrCommReset(void);
    cystatus spi_SpiCyBtldrCommRead       (uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
    cystatus spi_SpiCyBtldrCommWrite(const uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);

    /* Map SPI specific bootloader communication APIs to SCB specific APIs */
    #if (spi_SCB_MODE_SPI_CONST_CFG)
        #define spi_CyBtldrCommStart   spi_SpiCyBtldrCommStart
        #define spi_CyBtldrCommStop    spi_SpiCyBtldrCommStop
        #define spi_CyBtldrCommReset   spi_SpiCyBtldrCommReset
        #define spi_CyBtldrCommRead    spi_SpiCyBtldrCommRead
        #define spi_CyBtldrCommWrite   spi_SpiCyBtldrCommWrite
    #endif /* (spi_SCB_MODE_SPI_CONST_CFG) */

#endif /* defined(CYDEV_BOOTLOADER_IO_COMP) && (spi_SPI_BTLDR_COMM_ENABLED) */

#if defined(CYDEV_BOOTLOADER_IO_COMP) && (spi_UART_BTLDR_COMM_ENABLED)
    /* UART Bootloader physical layer functions */
    void spi_UartCyBtldrCommStart(void);
    void spi_UartCyBtldrCommStop (void);
    void spi_UartCyBtldrCommReset(void);
    cystatus spi_UartCyBtldrCommRead       (uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
    cystatus spi_UartCyBtldrCommWrite(const uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);

    /* Map UART specific bootloader communication APIs to SCB specific APIs */
    #if (spi_SCB_MODE_UART_CONST_CFG)
        #define spi_CyBtldrCommStart   spi_UartCyBtldrCommStart
        #define spi_CyBtldrCommStop    spi_UartCyBtldrCommStop
        #define spi_CyBtldrCommReset   spi_UartCyBtldrCommReset
        #define spi_CyBtldrCommRead    spi_UartCyBtldrCommRead
        #define spi_CyBtldrCommWrite   spi_UartCyBtldrCommWrite
    #endif /* (spi_SCB_MODE_UART_CONST_CFG) */

#endif /* defined(CYDEV_BOOTLOADER_IO_COMP) && (spi_UART_BTLDR_COMM_ENABLED) */

/**
* \addtogroup group_bootloader
* @{
*/

#if defined(CYDEV_BOOTLOADER_IO_COMP) && (spi_BTLDR_COMM_ENABLED)
    #if (spi_SCB_MODE_UNCONFIG_CONST_CFG)
        /* Bootloader physical layer functions */
        void spi_CyBtldrCommStart(void);
        void spi_CyBtldrCommStop (void);
        void spi_CyBtldrCommReset(void);
        cystatus spi_CyBtldrCommRead       (uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
        cystatus spi_CyBtldrCommWrite(const uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
    #endif /* (spi_SCB_MODE_UNCONFIG_CONST_CFG) */

    /* Map SCB specific bootloader communication APIs to common APIs */
    #if (CYDEV_BOOTLOADER_IO_COMP == CyBtldr_spi)
        #define CyBtldrCommStart    spi_CyBtldrCommStart
        #define CyBtldrCommStop     spi_CyBtldrCommStop
        #define CyBtldrCommReset    spi_CyBtldrCommReset
        #define CyBtldrCommWrite    spi_CyBtldrCommWrite
        #define CyBtldrCommRead     spi_CyBtldrCommRead
    #endif /* (CYDEV_BOOTLOADER_IO_COMP == CyBtldr_spi) */

#endif /* defined(CYDEV_BOOTLOADER_IO_COMP) && (spi_BTLDR_COMM_ENABLED) */

/** @} group_bootloader */

/***************************************
*           API Constants
***************************************/

/* Timeout unit in milliseconds */
#define spi_WAIT_1_MS  (1u)

/* Return number of bytes to copy into bootloader buffer */
#define spi_BYTES_TO_COPY(actBufSize, bufSize) \
                            ( ((uint32)(actBufSize) < (uint32)(bufSize)) ? \
                                ((uint32) (actBufSize)) : ((uint32) (bufSize)) )

/* Size of Read/Write buffers for I2C bootloader  */
#define spi_I2C_BTLDR_SIZEOF_READ_BUFFER   (64u)
#define spi_I2C_BTLDR_SIZEOF_WRITE_BUFFER  (64u)

/* Byte to byte time interval: calculated basing on current component
* data rate configuration, can be defined in project if required.
*/
#ifndef spi_SPI_BYTE_TO_BYTE
    #define spi_SPI_BYTE_TO_BYTE   (16u)
#endif

/* Byte to byte time interval: calculated basing on current component
* baud rate configuration, can be defined in the project if required.
*/
#ifndef spi_UART_BYTE_TO_BYTE
    #define spi_UART_BYTE_TO_BYTE  (2500u)
#endif /* spi_UART_BYTE_TO_BYTE */

#endif /* (CY_SCB_BOOT_spi_H) */


/* [] END OF FILE */
