/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "lis2dh12.h"

#include "SPIM.h"
#include "SPIM_SPI_UART.h"
#include "SPIM_PINS.h"
#include "SPIM_SPI_UART_PVT.h"
#include "SPIM_PVT.h"
#include "SPIM_BOOT.h"

#include "cs_acc.h"

mems_s mems;

uint8_t lis2dh12_init()
{
    uint8_t val;
    
    mems.axis_x = 0;
    mems.axis_y = 0;
    mems.axis_z = 0;
    mems.temp   = 0;
    
    val = lis2dh12_read_reg( WHO_AM_I );
    
    if( val == 0b00110011 ) 
    {
        lis2dh12_power_on();
        return 1;
    }
    else 
        return 0;
}

void lis2dh12_power_on()
{
    
//    ODR    
//    0 0 0 0 Power-down mode
//    0 0 0 1 HR / Normal / Low-power mode (1 Hz)
//    0 0 1 0 HR / Normal / Low-power mode (10 Hz)
//    0 0 1 1 HR / Normal / Low-power mode (25 Hz)
//    0 1 0 0 HR / Normal / Low-power mode (50 Hz)
//    0 1 0 1 HR / Normal / Low-power mode (100 Hz)
//    0 1 1 0 HR / Normal / Low-power mode (200 Hz)
//    0 1 1 1 HR/ Normal / Low-power mode (400 Hz)
//    1 0 0 0 Low-power mode (1.620 kHz)
//    1 0 0 1 HR/ Normal (1.344 kHz);
//    Low-power mode (5.376 kHz)

    // Start in normal mode
    ctrl_reg1_u reg1;
    
    reg1.val = lis2dh12_read_reg( CTRL_REG1 );
    
    reg1.b.odr  = 4;
    reg1.b.lpen = 1;    // low power en
    reg1.b.xen  = 1;    
    reg1.b.yen  = 1;
    reg1.b.zen  = 1;    
    
    lis2dh12_write_reg(CTRL_REG1,reg1.val );
}

void lis2dh12_power_down()
{
    ctrl_reg1_u reg1;
    
    reg1.val = lis2dh12_read_reg( CTRL_REG1 );
    reg1.b.odr = 0;
    reg1.b.lpen = 1;    // low power en
    
    lis2dh12_write_reg(CTRL_REG1,reg1.val );
    
}

uint8_t lis2dh12_read_all()
{
    unsigned char buf[10];
    status_reg_u status;
    
    status.val = lis2dh12_read_reg(STATUS_REG);
    
    if( status.b.xyzda == 0 ) return 0;
        
    buf[0] = OUT_X_L | 0x40 | 0x80;
    
    SPIM_SpiUartClearRxBuffer();
    
    cs_acc_Write(0);
    
    CyDelayUs(10);
    
    SPIM_SpiUartPutArray(buf,7);
 
    while (SPIM_SpiUartGetRxBufferSize() != 7 )
    {
    }

    cs_acc_Write(1);

    buf[0] = SPIM_SpiUartReadRxData();
    buf[1] = SPIM_SpiUartReadRxData();
    buf[2] = SPIM_SpiUartReadRxData();
    buf[3] = SPIM_SpiUartReadRxData();
    buf[4] = SPIM_SpiUartReadRxData();
    buf[5] = SPIM_SpiUartReadRxData();
    buf[6] = SPIM_SpiUartReadRxData();
    
    mems.axis_x  = (int16_t)buf[2] << 8;
    mems.axis_x |= (int16_t)buf[1];
    mems.axis_y  = (int16_t)buf[4] << 8;
    mems.axis_y |= (int16_t)buf[3];
    mems.axis_z  = (int16_t)buf[6] << 8;
    mems.axis_z |= (int16_t)buf[5];
    
    SPIM_SpiUartClearTxBuffer();
    
    return 1;
}

uint8_t lis2dh12_read_temp()
{
    return 1;
}


uint8_t lis2dh12_read_reg( unsigned char reg )
{
    unsigned char buf[2];
    
    buf[0] = reg | 0x80;
    buf[1] = 0;
    
    SPIM_SpiUartClearRxBuffer();
    
    cs_acc_Write(0);
    
    CyDelayUs(10);
    
    SPIM_SpiUartPutArray(buf,2);
 
    while (SPIM_SpiUartGetRxBufferSize() != 2 )
    {
    }

    cs_acc_Write(1);

    buf[0] = SPIM_SpiUartReadRxData();
    buf[1] = SPIM_SpiUartReadRxData();
    
    SPIM_SpiUartClearTxBuffer();

    return buf[1];
}

uint8_t lis2dh12_write_reg( unsigned char reg , unsigned char val )
{
    unsigned char buf[2];
    
    buf[0] = reg;
    buf[1] = val;
    
    SPIM_SpiUartClearRxBuffer();
    
    cs_acc_Write(0);
    
    CyDelayUs(10);
    
    SPIM_SpiUartPutArray(buf,2);
 
    while (SPIM_SpiUartGetRxBufferSize() != 2 )
    {
    }

    cs_acc_Write(1);
    
    SPIM_SpiUartClearTxBuffer();

    return 1;
}




/* [] END OF FILE */
