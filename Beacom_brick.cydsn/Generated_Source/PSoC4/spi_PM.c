/***************************************************************************//**
* \file spi_PM.c
* \version 4.0
*
* \brief
*  This file provides the source code to the Power Management support for
*  the SCB Component.
*
* Note:
*
********************************************************************************
* \copyright
* Copyright 2013-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "spi.h"
#include "spi_PVT.h"

#if(spi_SCB_MODE_I2C_INC)
    #include "spi_I2C_PVT.h"
#endif /* (spi_SCB_MODE_I2C_INC) */

#if(spi_SCB_MODE_EZI2C_INC)
    #include "spi_EZI2C_PVT.h"
#endif /* (spi_SCB_MODE_EZI2C_INC) */

#if(spi_SCB_MODE_SPI_INC || spi_SCB_MODE_UART_INC)
    #include "spi_SPI_UART_PVT.h"
#endif /* (spi_SCB_MODE_SPI_INC || spi_SCB_MODE_UART_INC) */


/***************************************
*   Backup Structure declaration
***************************************/

#if(spi_SCB_MODE_UNCONFIG_CONST_CFG || \
   (spi_SCB_MODE_I2C_CONST_CFG   && (!spi_I2C_WAKE_ENABLE_CONST))   || \
   (spi_SCB_MODE_EZI2C_CONST_CFG && (!spi_EZI2C_WAKE_ENABLE_CONST)) || \
   (spi_SCB_MODE_SPI_CONST_CFG   && (!spi_SPI_WAKE_ENABLE_CONST))   || \
   (spi_SCB_MODE_UART_CONST_CFG  && (!spi_UART_WAKE_ENABLE_CONST)))

    spi_BACKUP_STRUCT spi_backup =
    {
        0u, /* enableState */
    };
#endif


/*******************************************************************************
* Function Name: spi_Sleep
****************************************************************************//**
*
*  Prepares the spi component to enter Deep Sleep.
*  The “Enable wakeup from Deep Sleep Mode” selection has an influence on this 
*  function implementation:
*  - Checked: configures the component to be wakeup source from Deep Sleep.
*  - Unchecked: stores the current component state (enabled or disabled) and 
*    disables the component. See SCB_Stop() function for details about component 
*    disabling.
*
*  Call the spi_Sleep() function before calling the 
*  CyPmSysDeepSleep() function. 
*  Refer to the PSoC Creator System Reference Guide for more information about 
*  power management functions and Low power section of this document for the 
*  selected mode.
*
*  This function should not be called before entering Sleep.
*
*******************************************************************************/
void spi_Sleep(void)
{
#if(spi_SCB_MODE_UNCONFIG_CONST_CFG)

    if(spi_SCB_WAKE_ENABLE_CHECK)
    {
        if(spi_SCB_MODE_I2C_RUNTM_CFG)
        {
            spi_I2CSaveConfig();
        }
        else if(spi_SCB_MODE_EZI2C_RUNTM_CFG)
        {
            spi_EzI2CSaveConfig();
        }
    #if(!spi_CY_SCBIP_V1)
        else if(spi_SCB_MODE_SPI_RUNTM_CFG)
        {
            spi_SpiSaveConfig();
        }
        else if(spi_SCB_MODE_UART_RUNTM_CFG)
        {
            spi_UartSaveConfig();
        }
    #endif /* (!spi_CY_SCBIP_V1) */
        else
        {
            /* Unknown mode */
        }
    }
    else
    {
        spi_backup.enableState = (uint8) spi_GET_CTRL_ENABLED;

        if(0u != spi_backup.enableState)
        {
            spi_Stop();
        }
    }

#else

    #if (spi_SCB_MODE_I2C_CONST_CFG && spi_I2C_WAKE_ENABLE_CONST)
        spi_I2CSaveConfig();

    #elif (spi_SCB_MODE_EZI2C_CONST_CFG && spi_EZI2C_WAKE_ENABLE_CONST)
        spi_EzI2CSaveConfig();

    #elif (spi_SCB_MODE_SPI_CONST_CFG && spi_SPI_WAKE_ENABLE_CONST)
        spi_SpiSaveConfig();

    #elif (spi_SCB_MODE_UART_CONST_CFG && spi_UART_WAKE_ENABLE_CONST)
        spi_UartSaveConfig();

    #else

        spi_backup.enableState = (uint8) spi_GET_CTRL_ENABLED;

        if(0u != spi_backup.enableState)
        {
            spi_Stop();
        }

    #endif /* defined (spi_SCB_MODE_I2C_CONST_CFG) && (spi_I2C_WAKE_ENABLE_CONST) */

#endif /* (spi_SCB_MODE_UNCONFIG_CONST_CFG) */
}


/*******************************************************************************
* Function Name: spi_Wakeup
****************************************************************************//**
*
*  Prepares the spi component for Active mode operation after 
*  Deep Sleep.
*  The “Enable wakeup from Deep Sleep Mode” selection has influence on this 
*  function implementation:
*  - Checked: restores the component Active mode configuration.
*  - Unchecked: enables the component if it was enabled before enter Deep Sleep.
*
*  This function should not be called after exiting Sleep.
*
*  \sideeffect
*   Calling the spi_Wakeup() function without first calling the 
*   spi_Sleep() function may produce unexpected behavior.
*
*******************************************************************************/
void spi_Wakeup(void)
{
#if(spi_SCB_MODE_UNCONFIG_CONST_CFG)

    if(spi_SCB_WAKE_ENABLE_CHECK)
    {
        if(spi_SCB_MODE_I2C_RUNTM_CFG)
        {
            spi_I2CRestoreConfig();
        }
        else if(spi_SCB_MODE_EZI2C_RUNTM_CFG)
        {
            spi_EzI2CRestoreConfig();
        }
    #if(!spi_CY_SCBIP_V1)
        else if(spi_SCB_MODE_SPI_RUNTM_CFG)
        {
            spi_SpiRestoreConfig();
        }
        else if(spi_SCB_MODE_UART_RUNTM_CFG)
        {
            spi_UartRestoreConfig();
        }
    #endif /* (!spi_CY_SCBIP_V1) */
        else
        {
            /* Unknown mode */
        }
    }
    else
    {
        if(0u != spi_backup.enableState)
        {
            spi_Enable();
        }
    }

#else

    #if (spi_SCB_MODE_I2C_CONST_CFG  && spi_I2C_WAKE_ENABLE_CONST)
        spi_I2CRestoreConfig();

    #elif (spi_SCB_MODE_EZI2C_CONST_CFG && spi_EZI2C_WAKE_ENABLE_CONST)
        spi_EzI2CRestoreConfig();

    #elif (spi_SCB_MODE_SPI_CONST_CFG && spi_SPI_WAKE_ENABLE_CONST)
        spi_SpiRestoreConfig();

    #elif (spi_SCB_MODE_UART_CONST_CFG && spi_UART_WAKE_ENABLE_CONST)
        spi_UartRestoreConfig();

    #else

        if(0u != spi_backup.enableState)
        {
            spi_Enable();
        }

    #endif /* (spi_I2C_WAKE_ENABLE_CONST) */

#endif /* (spi_SCB_MODE_UNCONFIG_CONST_CFG) */
}


/* [] END OF FILE */
