/***************************************************************************//**
* \file spi_PINS.h
* \version 4.0
*
* \brief
*  This file provides constants and parameter values for the pin components
*  buried into SCB Component.
*
* Note:
*
********************************************************************************
* \copyright
* Copyright 2013-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SCB_PINS_spi_H)
#define CY_SCB_PINS_spi_H

#include "cydevice_trm.h"
#include "cyfitter.h"
#include "cytypes.h"


/***************************************
*   Conditional Compilation Parameters
****************************************/

/* Unconfigured pins */
#define spi_REMOVE_RX_WAKE_SDA_MOSI_PIN  (1u)
#define spi_REMOVE_RX_SDA_MOSI_PIN      (1u)
#define spi_REMOVE_TX_SCL_MISO_PIN      (1u)
#define spi_REMOVE_CTS_SCLK_PIN      (1u)
#define spi_REMOVE_RTS_SS0_PIN      (1u)
#define spi_REMOVE_SS1_PIN                 (1u)
#define spi_REMOVE_SS2_PIN                 (1u)
#define spi_REMOVE_SS3_PIN                 (1u)

/* Mode defined pins */
#define spi_REMOVE_I2C_PINS                (1u)
#define spi_REMOVE_SPI_MASTER_PINS         (0u)
#define spi_REMOVE_SPI_MASTER_SCLK_PIN     (0u)
#define spi_REMOVE_SPI_MASTER_MOSI_PIN     (0u)
#define spi_REMOVE_SPI_MASTER_MISO_PIN     (0u)
#define spi_REMOVE_SPI_MASTER_SS0_PIN      (1u)
#define spi_REMOVE_SPI_MASTER_SS1_PIN      (1u)
#define spi_REMOVE_SPI_MASTER_SS2_PIN      (1u)
#define spi_REMOVE_SPI_MASTER_SS3_PIN      (1u)
#define spi_REMOVE_SPI_SLAVE_PINS          (1u)
#define spi_REMOVE_SPI_SLAVE_MOSI_PIN      (1u)
#define spi_REMOVE_SPI_SLAVE_MISO_PIN      (1u)
#define spi_REMOVE_UART_TX_PIN             (1u)
#define spi_REMOVE_UART_RX_TX_PIN          (1u)
#define spi_REMOVE_UART_RX_PIN             (1u)
#define spi_REMOVE_UART_RX_WAKE_PIN        (1u)
#define spi_REMOVE_UART_RTS_PIN            (1u)
#define spi_REMOVE_UART_CTS_PIN            (1u)

/* Unconfigured pins */
#define spi_RX_WAKE_SDA_MOSI_PIN (0u == spi_REMOVE_RX_WAKE_SDA_MOSI_PIN)
#define spi_RX_SDA_MOSI_PIN     (0u == spi_REMOVE_RX_SDA_MOSI_PIN)
#define spi_TX_SCL_MISO_PIN     (0u == spi_REMOVE_TX_SCL_MISO_PIN)
#define spi_CTS_SCLK_PIN     (0u == spi_REMOVE_CTS_SCLK_PIN)
#define spi_RTS_SS0_PIN     (0u == spi_REMOVE_RTS_SS0_PIN)
#define spi_SS1_PIN                (0u == spi_REMOVE_SS1_PIN)
#define spi_SS2_PIN                (0u == spi_REMOVE_SS2_PIN)
#define spi_SS3_PIN                (0u == spi_REMOVE_SS3_PIN)

/* Mode defined pins */
#define spi_I2C_PINS               (0u == spi_REMOVE_I2C_PINS)
#define spi_SPI_MASTER_PINS        (0u == spi_REMOVE_SPI_MASTER_PINS)
#define spi_SPI_MASTER_SCLK_PIN    (0u == spi_REMOVE_SPI_MASTER_SCLK_PIN)
#define spi_SPI_MASTER_MOSI_PIN    (0u == spi_REMOVE_SPI_MASTER_MOSI_PIN)
#define spi_SPI_MASTER_MISO_PIN    (0u == spi_REMOVE_SPI_MASTER_MISO_PIN)
#define spi_SPI_MASTER_SS0_PIN     (0u == spi_REMOVE_SPI_MASTER_SS0_PIN)
#define spi_SPI_MASTER_SS1_PIN     (0u == spi_REMOVE_SPI_MASTER_SS1_PIN)
#define spi_SPI_MASTER_SS2_PIN     (0u == spi_REMOVE_SPI_MASTER_SS2_PIN)
#define spi_SPI_MASTER_SS3_PIN     (0u == spi_REMOVE_SPI_MASTER_SS3_PIN)
#define spi_SPI_SLAVE_PINS         (0u == spi_REMOVE_SPI_SLAVE_PINS)
#define spi_SPI_SLAVE_MOSI_PIN     (0u == spi_REMOVE_SPI_SLAVE_MOSI_PIN)
#define spi_SPI_SLAVE_MISO_PIN     (0u == spi_REMOVE_SPI_SLAVE_MISO_PIN)
#define spi_UART_TX_PIN            (0u == spi_REMOVE_UART_TX_PIN)
#define spi_UART_RX_TX_PIN         (0u == spi_REMOVE_UART_RX_TX_PIN)
#define spi_UART_RX_PIN            (0u == spi_REMOVE_UART_RX_PIN)
#define spi_UART_RX_WAKE_PIN       (0u == spi_REMOVE_UART_RX_WAKE_PIN)
#define spi_UART_RTS_PIN           (0u == spi_REMOVE_UART_RTS_PIN)
#define spi_UART_CTS_PIN           (0u == spi_REMOVE_UART_CTS_PIN)


/***************************************
*             Includes
****************************************/

#if (spi_RX_WAKE_SDA_MOSI_PIN)
    #include "spi_uart_rx_wake_i2c_sda_spi_mosi.h"
#endif /* (spi_RX_SDA_MOSI) */

#if (spi_RX_SDA_MOSI_PIN)
    #include "spi_uart_rx_i2c_sda_spi_mosi.h"
#endif /* (spi_RX_SDA_MOSI) */

#if (spi_TX_SCL_MISO_PIN)
    #include "spi_uart_tx_i2c_scl_spi_miso.h"
#endif /* (spi_TX_SCL_MISO) */

#if (spi_CTS_SCLK_PIN)
    #include "spi_uart_cts_spi_sclk.h"
#endif /* (spi_CTS_SCLK) */

#if (spi_RTS_SS0_PIN)
    #include "spi_uart_rts_spi_ss0.h"
#endif /* (spi_RTS_SS0_PIN) */

#if (spi_SS1_PIN)
    #include "spi_spi_ss1.h"
#endif /* (spi_SS1_PIN) */

#if (spi_SS2_PIN)
    #include "spi_spi_ss2.h"
#endif /* (spi_SS2_PIN) */

#if (spi_SS3_PIN)
    #include "spi_spi_ss3.h"
#endif /* (spi_SS3_PIN) */

#if (spi_I2C_PINS)
    #include "spi_scl.h"
    #include "spi_sda.h"
#endif /* (spi_I2C_PINS) */

#if (spi_SPI_MASTER_PINS)
#if (spi_SPI_MASTER_SCLK_PIN)
    #include "spi_sclk_m.h"
#endif /* (spi_SPI_MASTER_SCLK_PIN) */

#if (spi_SPI_MASTER_MOSI_PIN)
    #include "spi_mosi_m.h"
#endif /* (spi_SPI_MASTER_MOSI_PIN) */

#if (spi_SPI_MASTER_MISO_PIN)
    #include "spi_miso_m.h"
#endif /*(spi_SPI_MASTER_MISO_PIN) */
#endif /* (spi_SPI_MASTER_PINS) */

#if (spi_SPI_SLAVE_PINS)
    #include "spi_sclk_s.h"
    #include "spi_ss_s.h"

#if (spi_SPI_SLAVE_MOSI_PIN)
    #include "spi_mosi_s.h"
#endif /* (spi_SPI_SLAVE_MOSI_PIN) */

#if (spi_SPI_SLAVE_MISO_PIN)
    #include "spi_miso_s.h"
#endif /*(spi_SPI_SLAVE_MISO_PIN) */
#endif /* (spi_SPI_SLAVE_PINS) */

#if (spi_SPI_MASTER_SS0_PIN)
    #include "spi_ss0_m.h"
#endif /* (spi_SPI_MASTER_SS0_PIN) */

#if (spi_SPI_MASTER_SS1_PIN)
    #include "spi_ss1_m.h"
#endif /* (spi_SPI_MASTER_SS1_PIN) */

#if (spi_SPI_MASTER_SS2_PIN)
    #include "spi_ss2_m.h"
#endif /* (spi_SPI_MASTER_SS2_PIN) */

#if (spi_SPI_MASTER_SS3_PIN)
    #include "spi_ss3_m.h"
#endif /* (spi_SPI_MASTER_SS3_PIN) */

#if (spi_UART_TX_PIN)
    #include "spi_tx.h"
#endif /* (spi_UART_TX_PIN) */

#if (spi_UART_RX_TX_PIN)
    #include "spi_rx_tx.h"
#endif /* (spi_UART_RX_TX_PIN) */

#if (spi_UART_RX_PIN)
    #include "spi_rx.h"
#endif /* (spi_UART_RX_PIN) */

#if (spi_UART_RX_WAKE_PIN)
    #include "spi_rx_wake.h"
#endif /* (spi_UART_RX_WAKE_PIN) */

#if (spi_UART_RTS_PIN)
    #include "spi_rts.h"
#endif /* (spi_UART_RTS_PIN) */

#if (spi_UART_CTS_PIN)
    #include "spi_cts.h"
#endif /* (spi_UART_CTS_PIN) */


/***************************************
*              Registers
***************************************/

#if (spi_RX_SDA_MOSI_PIN)
    #define spi_RX_SDA_MOSI_HSIOM_REG   (*(reg32 *) spi_uart_rx_i2c_sda_spi_mosi__0__HSIOM)
    #define spi_RX_SDA_MOSI_HSIOM_PTR   ( (reg32 *) spi_uart_rx_i2c_sda_spi_mosi__0__HSIOM)
    
    #define spi_RX_SDA_MOSI_HSIOM_MASK      (spi_uart_rx_i2c_sda_spi_mosi__0__HSIOM_MASK)
    #define spi_RX_SDA_MOSI_HSIOM_POS       (spi_uart_rx_i2c_sda_spi_mosi__0__HSIOM_SHIFT)
    #define spi_RX_SDA_MOSI_HSIOM_SEL_GPIO  (spi_uart_rx_i2c_sda_spi_mosi__0__HSIOM_GPIO)
    #define spi_RX_SDA_MOSI_HSIOM_SEL_I2C   (spi_uart_rx_i2c_sda_spi_mosi__0__HSIOM_I2C)
    #define spi_RX_SDA_MOSI_HSIOM_SEL_SPI   (spi_uart_rx_i2c_sda_spi_mosi__0__HSIOM_SPI)
    #define spi_RX_SDA_MOSI_HSIOM_SEL_UART  (spi_uart_rx_i2c_sda_spi_mosi__0__HSIOM_UART)
    
#elif (spi_RX_WAKE_SDA_MOSI_PIN)
    #define spi_RX_WAKE_SDA_MOSI_HSIOM_REG   (*(reg32 *) spi_uart_rx_wake_i2c_sda_spi_mosi__0__HSIOM)
    #define spi_RX_WAKE_SDA_MOSI_HSIOM_PTR   ( (reg32 *) spi_uart_rx_wake_i2c_sda_spi_mosi__0__HSIOM)
    
    #define spi_RX_WAKE_SDA_MOSI_HSIOM_MASK      (spi_uart_rx_wake_i2c_sda_spi_mosi__0__HSIOM_MASK)
    #define spi_RX_WAKE_SDA_MOSI_HSIOM_POS       (spi_uart_rx_wake_i2c_sda_spi_mosi__0__HSIOM_SHIFT)
    #define spi_RX_WAKE_SDA_MOSI_HSIOM_SEL_GPIO  (spi_uart_rx_wake_i2c_sda_spi_mosi__0__HSIOM_GPIO)
    #define spi_RX_WAKE_SDA_MOSI_HSIOM_SEL_I2C   (spi_uart_rx_wake_i2c_sda_spi_mosi__0__HSIOM_I2C)
    #define spi_RX_WAKE_SDA_MOSI_HSIOM_SEL_SPI   (spi_uart_rx_wake_i2c_sda_spi_mosi__0__HSIOM_SPI)
    #define spi_RX_WAKE_SDA_MOSI_HSIOM_SEL_UART  (spi_uart_rx_wake_i2c_sda_spi_mosi__0__HSIOM_UART)    
   
    #define spi_RX_WAKE_SDA_MOSI_INTCFG_REG (*(reg32 *) spi_uart_rx_wake_i2c_sda_spi_mosi__0__INTCFG)
    #define spi_RX_WAKE_SDA_MOSI_INTCFG_PTR ( (reg32 *) spi_uart_rx_wake_i2c_sda_spi_mosi__0__INTCFG)
    #define spi_RX_WAKE_SDA_MOSI_INTCFG_TYPE_POS  (spi_uart_rx_wake_i2c_sda_spi_mosi__SHIFT)
    #define spi_RX_WAKE_SDA_MOSI_INTCFG_TYPE_MASK ((uint32) spi_INTCFG_TYPE_MASK << \
                                                                           spi_RX_WAKE_SDA_MOSI_INTCFG_TYPE_POS)
#else
    /* None of pins spi_RX_SDA_MOSI_PIN or spi_RX_WAKE_SDA_MOSI_PIN present.*/
#endif /* (spi_RX_SDA_MOSI_PIN) */

#if (spi_TX_SCL_MISO_PIN)
    #define spi_TX_SCL_MISO_HSIOM_REG   (*(reg32 *) spi_uart_tx_i2c_scl_spi_miso__0__HSIOM)
    #define spi_TX_SCL_MISO_HSIOM_PTR   ( (reg32 *) spi_uart_tx_i2c_scl_spi_miso__0__HSIOM)
    
    #define spi_TX_SCL_MISO_HSIOM_MASK      (spi_uart_tx_i2c_scl_spi_miso__0__HSIOM_MASK)
    #define spi_TX_SCL_MISO_HSIOM_POS       (spi_uart_tx_i2c_scl_spi_miso__0__HSIOM_SHIFT)
    #define spi_TX_SCL_MISO_HSIOM_SEL_GPIO  (spi_uart_tx_i2c_scl_spi_miso__0__HSIOM_GPIO)
    #define spi_TX_SCL_MISO_HSIOM_SEL_I2C   (spi_uart_tx_i2c_scl_spi_miso__0__HSIOM_I2C)
    #define spi_TX_SCL_MISO_HSIOM_SEL_SPI   (spi_uart_tx_i2c_scl_spi_miso__0__HSIOM_SPI)
    #define spi_TX_SCL_MISO_HSIOM_SEL_UART  (spi_uart_tx_i2c_scl_spi_miso__0__HSIOM_UART)
#endif /* (spi_TX_SCL_MISO_PIN) */

#if (spi_CTS_SCLK_PIN)
    #define spi_CTS_SCLK_HSIOM_REG   (*(reg32 *) spi_uart_cts_spi_sclk__0__HSIOM)
    #define spi_CTS_SCLK_HSIOM_PTR   ( (reg32 *) spi_uart_cts_spi_sclk__0__HSIOM)
    
    #define spi_CTS_SCLK_HSIOM_MASK      (spi_uart_cts_spi_sclk__0__HSIOM_MASK)
    #define spi_CTS_SCLK_HSIOM_POS       (spi_uart_cts_spi_sclk__0__HSIOM_SHIFT)
    #define spi_CTS_SCLK_HSIOM_SEL_GPIO  (spi_uart_cts_spi_sclk__0__HSIOM_GPIO)
    #define spi_CTS_SCLK_HSIOM_SEL_I2C   (spi_uart_cts_spi_sclk__0__HSIOM_I2C)
    #define spi_CTS_SCLK_HSIOM_SEL_SPI   (spi_uart_cts_spi_sclk__0__HSIOM_SPI)
    #define spi_CTS_SCLK_HSIOM_SEL_UART  (spi_uart_cts_spi_sclk__0__HSIOM_UART)
#endif /* (spi_CTS_SCLK_PIN) */

#if (spi_RTS_SS0_PIN)
    #define spi_RTS_SS0_HSIOM_REG   (*(reg32 *) spi_uart_rts_spi_ss0__0__HSIOM)
    #define spi_RTS_SS0_HSIOM_PTR   ( (reg32 *) spi_uart_rts_spi_ss0__0__HSIOM)
    
    #define spi_RTS_SS0_HSIOM_MASK      (spi_uart_rts_spi_ss0__0__HSIOM_MASK)
    #define spi_RTS_SS0_HSIOM_POS       (spi_uart_rts_spi_ss0__0__HSIOM_SHIFT)
    #define spi_RTS_SS0_HSIOM_SEL_GPIO  (spi_uart_rts_spi_ss0__0__HSIOM_GPIO)
    #define spi_RTS_SS0_HSIOM_SEL_I2C   (spi_uart_rts_spi_ss0__0__HSIOM_I2C)
    #define spi_RTS_SS0_HSIOM_SEL_SPI   (spi_uart_rts_spi_ss0__0__HSIOM_SPI)
#if !(spi_CY_SCBIP_V0 || spi_CY_SCBIP_V1)
    #define spi_RTS_SS0_HSIOM_SEL_UART  (spi_uart_rts_spi_ss0__0__HSIOM_UART)
#endif /* !(spi_CY_SCBIP_V0 || spi_CY_SCBIP_V1) */
#endif /* (spi_RTS_SS0_PIN) */

#if (spi_SS1_PIN)
    #define spi_SS1_HSIOM_REG  (*(reg32 *) spi_spi_ss1__0__HSIOM)
    #define spi_SS1_HSIOM_PTR  ( (reg32 *) spi_spi_ss1__0__HSIOM)
    
    #define spi_SS1_HSIOM_MASK     (spi_spi_ss1__0__HSIOM_MASK)
    #define spi_SS1_HSIOM_POS      (spi_spi_ss1__0__HSIOM_SHIFT)
    #define spi_SS1_HSIOM_SEL_GPIO (spi_spi_ss1__0__HSIOM_GPIO)
    #define spi_SS1_HSIOM_SEL_I2C  (spi_spi_ss1__0__HSIOM_I2C)
    #define spi_SS1_HSIOM_SEL_SPI  (spi_spi_ss1__0__HSIOM_SPI)
#endif /* (spi_SS1_PIN) */

#if (spi_SS2_PIN)
    #define spi_SS2_HSIOM_REG     (*(reg32 *) spi_spi_ss2__0__HSIOM)
    #define spi_SS2_HSIOM_PTR     ( (reg32 *) spi_spi_ss2__0__HSIOM)
    
    #define spi_SS2_HSIOM_MASK     (spi_spi_ss2__0__HSIOM_MASK)
    #define spi_SS2_HSIOM_POS      (spi_spi_ss2__0__HSIOM_SHIFT)
    #define spi_SS2_HSIOM_SEL_GPIO (spi_spi_ss2__0__HSIOM_GPIO)
    #define spi_SS2_HSIOM_SEL_I2C  (spi_spi_ss2__0__HSIOM_I2C)
    #define spi_SS2_HSIOM_SEL_SPI  (spi_spi_ss2__0__HSIOM_SPI)
#endif /* (spi_SS2_PIN) */

#if (spi_SS3_PIN)
    #define spi_SS3_HSIOM_REG     (*(reg32 *) spi_spi_ss3__0__HSIOM)
    #define spi_SS3_HSIOM_PTR     ( (reg32 *) spi_spi_ss3__0__HSIOM)
    
    #define spi_SS3_HSIOM_MASK     (spi_spi_ss3__0__HSIOM_MASK)
    #define spi_SS3_HSIOM_POS      (spi_spi_ss3__0__HSIOM_SHIFT)
    #define spi_SS3_HSIOM_SEL_GPIO (spi_spi_ss3__0__HSIOM_GPIO)
    #define spi_SS3_HSIOM_SEL_I2C  (spi_spi_ss3__0__HSIOM_I2C)
    #define spi_SS3_HSIOM_SEL_SPI  (spi_spi_ss3__0__HSIOM_SPI)
#endif /* (spi_SS3_PIN) */

#if (spi_I2C_PINS)
    #define spi_SCL_HSIOM_REG  (*(reg32 *) spi_scl__0__HSIOM)
    #define spi_SCL_HSIOM_PTR  ( (reg32 *) spi_scl__0__HSIOM)
    
    #define spi_SCL_HSIOM_MASK     (spi_scl__0__HSIOM_MASK)
    #define spi_SCL_HSIOM_POS      (spi_scl__0__HSIOM_SHIFT)
    #define spi_SCL_HSIOM_SEL_GPIO (spi_sda__0__HSIOM_GPIO)
    #define spi_SCL_HSIOM_SEL_I2C  (spi_sda__0__HSIOM_I2C)
    
    #define spi_SDA_HSIOM_REG  (*(reg32 *) spi_sda__0__HSIOM)
    #define spi_SDA_HSIOM_PTR  ( (reg32 *) spi_sda__0__HSIOM)
    
    #define spi_SDA_HSIOM_MASK     (spi_sda__0__HSIOM_MASK)
    #define spi_SDA_HSIOM_POS      (spi_sda__0__HSIOM_SHIFT)
    #define spi_SDA_HSIOM_SEL_GPIO (spi_sda__0__HSIOM_GPIO)
    #define spi_SDA_HSIOM_SEL_I2C  (spi_sda__0__HSIOM_I2C)
#endif /* (spi_I2C_PINS) */

#if (spi_SPI_SLAVE_PINS)
    #define spi_SCLK_S_HSIOM_REG   (*(reg32 *) spi_sclk_s__0__HSIOM)
    #define spi_SCLK_S_HSIOM_PTR   ( (reg32 *) spi_sclk_s__0__HSIOM)
    
    #define spi_SCLK_S_HSIOM_MASK      (spi_sclk_s__0__HSIOM_MASK)
    #define spi_SCLK_S_HSIOM_POS       (spi_sclk_s__0__HSIOM_SHIFT)
    #define spi_SCLK_S_HSIOM_SEL_GPIO  (spi_sclk_s__0__HSIOM_GPIO)
    #define spi_SCLK_S_HSIOM_SEL_SPI   (spi_sclk_s__0__HSIOM_SPI)
    
    #define spi_SS0_S_HSIOM_REG    (*(reg32 *) spi_ss0_s__0__HSIOM)
    #define spi_SS0_S_HSIOM_PTR    ( (reg32 *) spi_ss0_s__0__HSIOM)
    
    #define spi_SS0_S_HSIOM_MASK       (spi_ss0_s__0__HSIOM_MASK)
    #define spi_SS0_S_HSIOM_POS        (spi_ss0_s__0__HSIOM_SHIFT)
    #define spi_SS0_S_HSIOM_SEL_GPIO   (spi_ss0_s__0__HSIOM_GPIO)  
    #define spi_SS0_S_HSIOM_SEL_SPI    (spi_ss0_s__0__HSIOM_SPI)
#endif /* (spi_SPI_SLAVE_PINS) */

#if (spi_SPI_SLAVE_MOSI_PIN)
    #define spi_MOSI_S_HSIOM_REG   (*(reg32 *) spi_mosi_s__0__HSIOM)
    #define spi_MOSI_S_HSIOM_PTR   ( (reg32 *) spi_mosi_s__0__HSIOM)
    
    #define spi_MOSI_S_HSIOM_MASK      (spi_mosi_s__0__HSIOM_MASK)
    #define spi_MOSI_S_HSIOM_POS       (spi_mosi_s__0__HSIOM_SHIFT)
    #define spi_MOSI_S_HSIOM_SEL_GPIO  (spi_mosi_s__0__HSIOM_GPIO)
    #define spi_MOSI_S_HSIOM_SEL_SPI   (spi_mosi_s__0__HSIOM_SPI)
#endif /* (spi_SPI_SLAVE_MOSI_PIN) */

#if (spi_SPI_SLAVE_MISO_PIN)
    #define spi_MISO_S_HSIOM_REG   (*(reg32 *) spi_miso_s__0__HSIOM)
    #define spi_MISO_S_HSIOM_PTR   ( (reg32 *) spi_miso_s__0__HSIOM)
    
    #define spi_MISO_S_HSIOM_MASK      (spi_miso_s__0__HSIOM_MASK)
    #define spi_MISO_S_HSIOM_POS       (spi_miso_s__0__HSIOM_SHIFT)
    #define spi_MISO_S_HSIOM_SEL_GPIO  (spi_miso_s__0__HSIOM_GPIO)
    #define spi_MISO_S_HSIOM_SEL_SPI   (spi_miso_s__0__HSIOM_SPI)
#endif /* (spi_SPI_SLAVE_MISO_PIN) */

#if (spi_SPI_MASTER_MISO_PIN)
    #define spi_MISO_M_HSIOM_REG   (*(reg32 *) spi_miso_m__0__HSIOM)
    #define spi_MISO_M_HSIOM_PTR   ( (reg32 *) spi_miso_m__0__HSIOM)
    
    #define spi_MISO_M_HSIOM_MASK      (spi_miso_m__0__HSIOM_MASK)
    #define spi_MISO_M_HSIOM_POS       (spi_miso_m__0__HSIOM_SHIFT)
    #define spi_MISO_M_HSIOM_SEL_GPIO  (spi_miso_m__0__HSIOM_GPIO)
    #define spi_MISO_M_HSIOM_SEL_SPI   (spi_miso_m__0__HSIOM_SPI)
#endif /* (spi_SPI_MASTER_MISO_PIN) */

#if (spi_SPI_MASTER_MOSI_PIN)
    #define spi_MOSI_M_HSIOM_REG   (*(reg32 *) spi_mosi_m__0__HSIOM)
    #define spi_MOSI_M_HSIOM_PTR   ( (reg32 *) spi_mosi_m__0__HSIOM)
    
    #define spi_MOSI_M_HSIOM_MASK      (spi_mosi_m__0__HSIOM_MASK)
    #define spi_MOSI_M_HSIOM_POS       (spi_mosi_m__0__HSIOM_SHIFT)
    #define spi_MOSI_M_HSIOM_SEL_GPIO  (spi_mosi_m__0__HSIOM_GPIO)
    #define spi_MOSI_M_HSIOM_SEL_SPI   (spi_mosi_m__0__HSIOM_SPI)
#endif /* (spi_SPI_MASTER_MOSI_PIN) */

#if (spi_SPI_MASTER_SCLK_PIN)
    #define spi_SCLK_M_HSIOM_REG   (*(reg32 *) spi_sclk_m__0__HSIOM)
    #define spi_SCLK_M_HSIOM_PTR   ( (reg32 *) spi_sclk_m__0__HSIOM)
    
    #define spi_SCLK_M_HSIOM_MASK      (spi_sclk_m__0__HSIOM_MASK)
    #define spi_SCLK_M_HSIOM_POS       (spi_sclk_m__0__HSIOM_SHIFT)
    #define spi_SCLK_M_HSIOM_SEL_GPIO  (spi_sclk_m__0__HSIOM_GPIO)
    #define spi_SCLK_M_HSIOM_SEL_SPI   (spi_sclk_m__0__HSIOM_SPI)
#endif /* (spi_SPI_MASTER_SCLK_PIN) */

#if (spi_SPI_MASTER_SS0_PIN)
    #define spi_SS0_M_HSIOM_REG    (*(reg32 *) spi_ss0_m__0__HSIOM)
    #define spi_SS0_M_HSIOM_PTR    ( (reg32 *) spi_ss0_m__0__HSIOM)
    
    #define spi_SS0_M_HSIOM_MASK       (spi_ss0_m__0__HSIOM_MASK)
    #define spi_SS0_M_HSIOM_POS        (spi_ss0_m__0__HSIOM_SHIFT)
    #define spi_SS0_M_HSIOM_SEL_GPIO   (spi_ss0_m__0__HSIOM_GPIO)
    #define spi_SS0_M_HSIOM_SEL_SPI    (spi_ss0_m__0__HSIOM_SPI)
#endif /* (spi_SPI_MASTER_SS0_PIN) */

#if (spi_SPI_MASTER_SS1_PIN)
    #define spi_SS1_M_HSIOM_REG    (*(reg32 *) spi_ss1_m__0__HSIOM)
    #define spi_SS1_M_HSIOM_PTR    ( (reg32 *) spi_ss1_m__0__HSIOM)
    
    #define spi_SS1_M_HSIOM_MASK       (spi_ss1_m__0__HSIOM_MASK)
    #define spi_SS1_M_HSIOM_POS        (spi_ss1_m__0__HSIOM_SHIFT)
    #define spi_SS1_M_HSIOM_SEL_GPIO   (spi_ss1_m__0__HSIOM_GPIO)
    #define spi_SS1_M_HSIOM_SEL_SPI    (spi_ss1_m__0__HSIOM_SPI)
#endif /* (spi_SPI_MASTER_SS1_PIN) */

#if (spi_SPI_MASTER_SS2_PIN)
    #define spi_SS2_M_HSIOM_REG    (*(reg32 *) spi_ss2_m__0__HSIOM)
    #define spi_SS2_M_HSIOM_PTR    ( (reg32 *) spi_ss2_m__0__HSIOM)
    
    #define spi_SS2_M_HSIOM_MASK       (spi_ss2_m__0__HSIOM_MASK)
    #define spi_SS2_M_HSIOM_POS        (spi_ss2_m__0__HSIOM_SHIFT)
    #define spi_SS2_M_HSIOM_SEL_GPIO   (spi_ss2_m__0__HSIOM_GPIO)
    #define spi_SS2_M_HSIOM_SEL_SPI    (spi_ss2_m__0__HSIOM_SPI)
#endif /* (spi_SPI_MASTER_SS2_PIN) */

#if (spi_SPI_MASTER_SS3_PIN)
    #define spi_SS3_M_HSIOM_REG    (*(reg32 *) spi_ss3_m__0__HSIOM)
    #define spi_SS3_M_HSIOM_PTR    ( (reg32 *) spi_ss3_m__0__HSIOM)
    
    #define spi_SS3_M_HSIOM_MASK      (spi_ss3_m__0__HSIOM_MASK)
    #define spi_SS3_M_HSIOM_POS       (spi_ss3_m__0__HSIOM_SHIFT)
    #define spi_SS3_M_HSIOM_SEL_GPIO  (spi_ss3_m__0__HSIOM_GPIO)
    #define spi_SS3_M_HSIOM_SEL_SPI   (spi_ss3_m__0__HSIOM_SPI)
#endif /* (spi_SPI_MASTER_SS3_PIN) */

#if (spi_UART_RX_PIN)
    #define spi_RX_HSIOM_REG   (*(reg32 *) spi_rx__0__HSIOM)
    #define spi_RX_HSIOM_PTR   ( (reg32 *) spi_rx__0__HSIOM)
    
    #define spi_RX_HSIOM_MASK      (spi_rx__0__HSIOM_MASK)
    #define spi_RX_HSIOM_POS       (spi_rx__0__HSIOM_SHIFT)
    #define spi_RX_HSIOM_SEL_GPIO  (spi_rx__0__HSIOM_GPIO)
    #define spi_RX_HSIOM_SEL_UART  (spi_rx__0__HSIOM_UART)
#endif /* (spi_UART_RX_PIN) */

#if (spi_UART_RX_WAKE_PIN)
    #define spi_RX_WAKE_HSIOM_REG   (*(reg32 *) spi_rx_wake__0__HSIOM)
    #define spi_RX_WAKE_HSIOM_PTR   ( (reg32 *) spi_rx_wake__0__HSIOM)
    
    #define spi_RX_WAKE_HSIOM_MASK      (spi_rx_wake__0__HSIOM_MASK)
    #define spi_RX_WAKE_HSIOM_POS       (spi_rx_wake__0__HSIOM_SHIFT)
    #define spi_RX_WAKE_HSIOM_SEL_GPIO  (spi_rx_wake__0__HSIOM_GPIO)
    #define spi_RX_WAKE_HSIOM_SEL_UART  (spi_rx_wake__0__HSIOM_UART)
#endif /* (spi_UART_WAKE_RX_PIN) */

#if (spi_UART_CTS_PIN)
    #define spi_CTS_HSIOM_REG   (*(reg32 *) spi_cts__0__HSIOM)
    #define spi_CTS_HSIOM_PTR   ( (reg32 *) spi_cts__0__HSIOM)
    
    #define spi_CTS_HSIOM_MASK      (spi_cts__0__HSIOM_MASK)
    #define spi_CTS_HSIOM_POS       (spi_cts__0__HSIOM_SHIFT)
    #define spi_CTS_HSIOM_SEL_GPIO  (spi_cts__0__HSIOM_GPIO)
    #define spi_CTS_HSIOM_SEL_UART  (spi_cts__0__HSIOM_UART)
#endif /* (spi_UART_CTS_PIN) */

#if (spi_UART_TX_PIN)
    #define spi_TX_HSIOM_REG   (*(reg32 *) spi_tx__0__HSIOM)
    #define spi_TX_HSIOM_PTR   ( (reg32 *) spi_tx__0__HSIOM)
    
    #define spi_TX_HSIOM_MASK      (spi_tx__0__HSIOM_MASK)
    #define spi_TX_HSIOM_POS       (spi_tx__0__HSIOM_SHIFT)
    #define spi_TX_HSIOM_SEL_GPIO  (spi_tx__0__HSIOM_GPIO)
    #define spi_TX_HSIOM_SEL_UART  (spi_tx__0__HSIOM_UART)
#endif /* (spi_UART_TX_PIN) */

#if (spi_UART_RX_TX_PIN)
    #define spi_RX_TX_HSIOM_REG   (*(reg32 *) spi_rx_tx__0__HSIOM)
    #define spi_RX_TX_HSIOM_PTR   ( (reg32 *) spi_rx_tx__0__HSIOM)
    
    #define spi_RX_TX_HSIOM_MASK      (spi_rx_tx__0__HSIOM_MASK)
    #define spi_RX_TX_HSIOM_POS       (spi_rx_tx__0__HSIOM_SHIFT)
    #define spi_RX_TX_HSIOM_SEL_GPIO  (spi_rx_tx__0__HSIOM_GPIO)
    #define spi_RX_TX_HSIOM_SEL_UART  (spi_rx_tx__0__HSIOM_UART)
#endif /* (spi_UART_RX_TX_PIN) */

#if (spi_UART_RTS_PIN)
    #define spi_RTS_HSIOM_REG      (*(reg32 *) spi_rts__0__HSIOM)
    #define spi_RTS_HSIOM_PTR      ( (reg32 *) spi_rts__0__HSIOM)
    
    #define spi_RTS_HSIOM_MASK     (spi_rts__0__HSIOM_MASK)
    #define spi_RTS_HSIOM_POS      (spi_rts__0__HSIOM_SHIFT)    
    #define spi_RTS_HSIOM_SEL_GPIO (spi_rts__0__HSIOM_GPIO)
    #define spi_RTS_HSIOM_SEL_UART (spi_rts__0__HSIOM_UART)    
#endif /* (spi_UART_RTS_PIN) */


/***************************************
*        Registers Constants
***************************************/

/* HSIOM switch values. */ 
#define spi_HSIOM_DEF_SEL      (0x00u)
#define spi_HSIOM_GPIO_SEL     (0x00u)
/* The HSIOM values provided below are valid only for spi_CY_SCBIP_V0 
* and spi_CY_SCBIP_V1. It is not recommended to use them for 
* spi_CY_SCBIP_V2. Use pin name specific HSIOM constants provided 
* above instead for any SCB IP block version.
*/
#define spi_HSIOM_UART_SEL     (0x09u)
#define spi_HSIOM_I2C_SEL      (0x0Eu)
#define spi_HSIOM_SPI_SEL      (0x0Fu)

/* Pins settings index. */
#define spi_RX_WAKE_SDA_MOSI_PIN_INDEX   (0u)
#define spi_RX_SDA_MOSI_PIN_INDEX       (0u)
#define spi_TX_SCL_MISO_PIN_INDEX       (1u)
#define spi_CTS_SCLK_PIN_INDEX       (2u)
#define spi_RTS_SS0_PIN_INDEX       (3u)
#define spi_SS1_PIN_INDEX                  (4u)
#define spi_SS2_PIN_INDEX                  (5u)
#define spi_SS3_PIN_INDEX                  (6u)

/* Pins settings mask. */
#define spi_RX_WAKE_SDA_MOSI_PIN_MASK ((uint32) 0x01u << spi_RX_WAKE_SDA_MOSI_PIN_INDEX)
#define spi_RX_SDA_MOSI_PIN_MASK     ((uint32) 0x01u << spi_RX_SDA_MOSI_PIN_INDEX)
#define spi_TX_SCL_MISO_PIN_MASK     ((uint32) 0x01u << spi_TX_SCL_MISO_PIN_INDEX)
#define spi_CTS_SCLK_PIN_MASK     ((uint32) 0x01u << spi_CTS_SCLK_PIN_INDEX)
#define spi_RTS_SS0_PIN_MASK     ((uint32) 0x01u << spi_RTS_SS0_PIN_INDEX)
#define spi_SS1_PIN_MASK                ((uint32) 0x01u << spi_SS1_PIN_INDEX)
#define spi_SS2_PIN_MASK                ((uint32) 0x01u << spi_SS2_PIN_INDEX)
#define spi_SS3_PIN_MASK                ((uint32) 0x01u << spi_SS3_PIN_INDEX)

/* Pin interrupt constants. */
#define spi_INTCFG_TYPE_MASK           (0x03u)
#define spi_INTCFG_TYPE_FALLING_EDGE   (0x02u)

/* Pin Drive Mode constants. */
#define spi_PIN_DM_ALG_HIZ  (0u)
#define spi_PIN_DM_DIG_HIZ  (1u)
#define spi_PIN_DM_OD_LO    (4u)
#define spi_PIN_DM_STRONG   (6u)


/***************************************
*          Macro Definitions
***************************************/

/* Return drive mode of the pin */
#define spi_DM_MASK    (0x7u)
#define spi_DM_SIZE    (3u)
#define spi_GET_P4_PIN_DM(reg, pos) \
    ( ((reg) & (uint32) ((uint32) spi_DM_MASK << (spi_DM_SIZE * (pos)))) >> \
                                                              (spi_DM_SIZE * (pos)) )

#if (spi_TX_SCL_MISO_PIN)
    #define spi_CHECK_TX_SCL_MISO_PIN_USED \
                (spi_PIN_DM_ALG_HIZ != \
                    spi_GET_P4_PIN_DM(spi_uart_tx_i2c_scl_spi_miso_PC, \
                                                   spi_uart_tx_i2c_scl_spi_miso_SHIFT))
#endif /* (spi_TX_SCL_MISO_PIN) */

#if (spi_RTS_SS0_PIN)
    #define spi_CHECK_RTS_SS0_PIN_USED \
                (spi_PIN_DM_ALG_HIZ != \
                    spi_GET_P4_PIN_DM(spi_uart_rts_spi_ss0_PC, \
                                                   spi_uart_rts_spi_ss0_SHIFT))
#endif /* (spi_RTS_SS0_PIN) */

/* Set bits-mask in register */
#define spi_SET_REGISTER_BITS(reg, mask, pos, mode) \
                    do                                           \
                    {                                            \
                        (reg) = (((reg) & ((uint32) ~(uint32) (mask))) | ((uint32) ((uint32) (mode) << (pos)))); \
                    }while(0)

/* Set bit in the register */
#define spi_SET_REGISTER_BIT(reg, mask, val) \
                    ((val) ? ((reg) |= (mask)) : ((reg) &= ((uint32) ~((uint32) (mask)))))

#define spi_SET_HSIOM_SEL(reg, mask, pos, sel) spi_SET_REGISTER_BITS(reg, mask, pos, sel)
#define spi_SET_INCFG_TYPE(reg, mask, pos, intType) \
                                                        spi_SET_REGISTER_BITS(reg, mask, pos, intType)
#define spi_SET_INP_DIS(reg, mask, val) spi_SET_REGISTER_BIT(reg, mask, val)

/* spi_SET_I2C_SCL_DR(val) - Sets I2C SCL DR register.
*  spi_SET_I2C_SCL_HSIOM_SEL(sel) - Sets I2C SCL HSIOM settings.
*/
/* SCB I2C: scl signal */
#if (spi_CY_SCBIP_V0)
#if (spi_I2C_PINS)
    #define spi_SET_I2C_SCL_DR(val) spi_scl_Write(val)

    #define spi_SET_I2C_SCL_HSIOM_SEL(sel) \
                          spi_SET_HSIOM_SEL(spi_SCL_HSIOM_REG,  \
                                                         spi_SCL_HSIOM_MASK, \
                                                         spi_SCL_HSIOM_POS,  \
                                                         (sel))
    #define spi_WAIT_SCL_SET_HIGH  (0u == spi_scl_Read())

/* Unconfigured SCB: scl signal */
#elif (spi_RX_WAKE_SDA_MOSI_PIN)
    #define spi_SET_I2C_SCL_DR(val) \
                            spi_uart_rx_wake_i2c_sda_spi_mosi_Write(val)

    #define spi_SET_I2C_SCL_HSIOM_SEL(sel) \
                    spi_SET_HSIOM_SEL(spi_RX_WAKE_SDA_MOSI_HSIOM_REG,  \
                                                   spi_RX_WAKE_SDA_MOSI_HSIOM_MASK, \
                                                   spi_RX_WAKE_SDA_MOSI_HSIOM_POS,  \
                                                   (sel))

    #define spi_WAIT_SCL_SET_HIGH  (0u == spi_uart_rx_wake_i2c_sda_spi_mosi_Read())

#elif (spi_RX_SDA_MOSI_PIN)
    #define spi_SET_I2C_SCL_DR(val) \
                            spi_uart_rx_i2c_sda_spi_mosi_Write(val)


    #define spi_SET_I2C_SCL_HSIOM_SEL(sel) \
                            spi_SET_HSIOM_SEL(spi_RX_SDA_MOSI_HSIOM_REG,  \
                                                           spi_RX_SDA_MOSI_HSIOM_MASK, \
                                                           spi_RX_SDA_MOSI_HSIOM_POS,  \
                                                           (sel))

    #define spi_WAIT_SCL_SET_HIGH  (0u == spi_uart_rx_i2c_sda_spi_mosi_Read())

#else
    #define spi_SET_I2C_SCL_DR(val)        do{ /* Does nothing */ }while(0)
    #define spi_SET_I2C_SCL_HSIOM_SEL(sel) do{ /* Does nothing */ }while(0)

    #define spi_WAIT_SCL_SET_HIGH  (0u)
#endif /* (spi_I2C_PINS) */

/* SCB I2C: sda signal */
#if (spi_I2C_PINS)
    #define spi_WAIT_SDA_SET_HIGH  (0u == spi_sda_Read())
/* Unconfigured SCB: sda signal */
#elif (spi_TX_SCL_MISO_PIN)
    #define spi_WAIT_SDA_SET_HIGH  (0u == spi_uart_tx_i2c_scl_spi_miso_Read())
#else
    #define spi_WAIT_SDA_SET_HIGH  (0u)
#endif /* (spi_MOSI_SCL_RX_PIN) */
#endif /* (spi_CY_SCBIP_V0) */

/* Clear UART wakeup source */
#if (spi_RX_SDA_MOSI_PIN)
    #define spi_CLEAR_UART_RX_WAKE_INTR        do{ /* Does nothing */ }while(0)
    
#elif (spi_RX_WAKE_SDA_MOSI_PIN)
    #define spi_CLEAR_UART_RX_WAKE_INTR \
            do{                                      \
                (void) spi_uart_rx_wake_i2c_sda_spi_mosi_ClearInterrupt(); \
            }while(0)

#elif(spi_UART_RX_WAKE_PIN)
    #define spi_CLEAR_UART_RX_WAKE_INTR \
            do{                                      \
                (void) spi_rx_wake_ClearInterrupt(); \
            }while(0)
#else
#endif /* (spi_RX_SDA_MOSI_PIN) */


/***************************************
* The following code is DEPRECATED and
* must not be used.
***************************************/

/* Unconfigured pins */
#define spi_REMOVE_MOSI_SCL_RX_WAKE_PIN    spi_REMOVE_RX_WAKE_SDA_MOSI_PIN
#define spi_REMOVE_MOSI_SCL_RX_PIN         spi_REMOVE_RX_SDA_MOSI_PIN
#define spi_REMOVE_MISO_SDA_TX_PIN         spi_REMOVE_TX_SCL_MISO_PIN
#ifndef spi_REMOVE_SCLK_PIN
#define spi_REMOVE_SCLK_PIN                spi_REMOVE_CTS_SCLK_PIN
#endif /* spi_REMOVE_SCLK_PIN */
#ifndef spi_REMOVE_SS0_PIN
#define spi_REMOVE_SS0_PIN                 spi_REMOVE_RTS_SS0_PIN
#endif /* spi_REMOVE_SS0_PIN */

/* Unconfigured pins */
#define spi_MOSI_SCL_RX_WAKE_PIN   spi_RX_WAKE_SDA_MOSI_PIN
#define spi_MOSI_SCL_RX_PIN        spi_RX_SDA_MOSI_PIN
#define spi_MISO_SDA_TX_PIN        spi_TX_SCL_MISO_PIN
#ifndef spi_SCLK_PIN
#define spi_SCLK_PIN               spi_CTS_SCLK_PIN
#endif /* spi_SCLK_PIN */
#ifndef spi_SS0_PIN
#define spi_SS0_PIN                spi_RTS_SS0_PIN
#endif /* spi_SS0_PIN */

#if (spi_MOSI_SCL_RX_WAKE_PIN)
    #define spi_MOSI_SCL_RX_WAKE_HSIOM_REG     spi_RX_WAKE_SDA_MOSI_HSIOM_REG
    #define spi_MOSI_SCL_RX_WAKE_HSIOM_PTR     spi_RX_WAKE_SDA_MOSI_HSIOM_REG
    #define spi_MOSI_SCL_RX_WAKE_HSIOM_MASK    spi_RX_WAKE_SDA_MOSI_HSIOM_REG
    #define spi_MOSI_SCL_RX_WAKE_HSIOM_POS     spi_RX_WAKE_SDA_MOSI_HSIOM_REG

    #define spi_MOSI_SCL_RX_WAKE_INTCFG_REG    spi_RX_WAKE_SDA_MOSI_HSIOM_REG
    #define spi_MOSI_SCL_RX_WAKE_INTCFG_PTR    spi_RX_WAKE_SDA_MOSI_HSIOM_REG

    #define spi_MOSI_SCL_RX_WAKE_INTCFG_TYPE_POS   spi_RX_WAKE_SDA_MOSI_HSIOM_REG
    #define spi_MOSI_SCL_RX_WAKE_INTCFG_TYPE_MASK  spi_RX_WAKE_SDA_MOSI_HSIOM_REG
#endif /* (spi_RX_WAKE_SDA_MOSI_PIN) */

#if (spi_MOSI_SCL_RX_PIN)
    #define spi_MOSI_SCL_RX_HSIOM_REG      spi_RX_SDA_MOSI_HSIOM_REG
    #define spi_MOSI_SCL_RX_HSIOM_PTR      spi_RX_SDA_MOSI_HSIOM_PTR
    #define spi_MOSI_SCL_RX_HSIOM_MASK     spi_RX_SDA_MOSI_HSIOM_MASK
    #define spi_MOSI_SCL_RX_HSIOM_POS      spi_RX_SDA_MOSI_HSIOM_POS
#endif /* (spi_MOSI_SCL_RX_PIN) */

#if (spi_MISO_SDA_TX_PIN)
    #define spi_MISO_SDA_TX_HSIOM_REG      spi_TX_SCL_MISO_HSIOM_REG
    #define spi_MISO_SDA_TX_HSIOM_PTR      spi_TX_SCL_MISO_HSIOM_REG
    #define spi_MISO_SDA_TX_HSIOM_MASK     spi_TX_SCL_MISO_HSIOM_REG
    #define spi_MISO_SDA_TX_HSIOM_POS      spi_TX_SCL_MISO_HSIOM_REG
#endif /* (spi_MISO_SDA_TX_PIN_PIN) */

#if (spi_SCLK_PIN)
    #ifndef spi_SCLK_HSIOM_REG
    #define spi_SCLK_HSIOM_REG     spi_CTS_SCLK_HSIOM_REG
    #define spi_SCLK_HSIOM_PTR     spi_CTS_SCLK_HSIOM_PTR
    #define spi_SCLK_HSIOM_MASK    spi_CTS_SCLK_HSIOM_MASK
    #define spi_SCLK_HSIOM_POS     spi_CTS_SCLK_HSIOM_POS
    #endif /* spi_SCLK_HSIOM_REG */
#endif /* (spi_SCLK_PIN) */

#if (spi_SS0_PIN)
    #ifndef spi_SS0_HSIOM_REG
    #define spi_SS0_HSIOM_REG      spi_RTS_SS0_HSIOM_REG
    #define spi_SS0_HSIOM_PTR      spi_RTS_SS0_HSIOM_PTR
    #define spi_SS0_HSIOM_MASK     spi_RTS_SS0_HSIOM_MASK
    #define spi_SS0_HSIOM_POS      spi_RTS_SS0_HSIOM_POS
    #endif /* spi_SS0_HSIOM_REG */
#endif /* (spi_SS0_PIN) */

#define spi_MOSI_SCL_RX_WAKE_PIN_INDEX spi_RX_WAKE_SDA_MOSI_PIN_INDEX
#define spi_MOSI_SCL_RX_PIN_INDEX      spi_RX_SDA_MOSI_PIN_INDEX
#define spi_MISO_SDA_TX_PIN_INDEX      spi_TX_SCL_MISO_PIN_INDEX
#ifndef spi_SCLK_PIN_INDEX
#define spi_SCLK_PIN_INDEX             spi_CTS_SCLK_PIN_INDEX
#endif /* spi_SCLK_PIN_INDEX */
#ifndef spi_SS0_PIN_INDEX
#define spi_SS0_PIN_INDEX              spi_RTS_SS0_PIN_INDEX
#endif /* spi_SS0_PIN_INDEX */

#define spi_MOSI_SCL_RX_WAKE_PIN_MASK spi_RX_WAKE_SDA_MOSI_PIN_MASK
#define spi_MOSI_SCL_RX_PIN_MASK      spi_RX_SDA_MOSI_PIN_MASK
#define spi_MISO_SDA_TX_PIN_MASK      spi_TX_SCL_MISO_PIN_MASK
#ifndef spi_SCLK_PIN_MASK
#define spi_SCLK_PIN_MASK             spi_CTS_SCLK_PIN_MASK
#endif /* spi_SCLK_PIN_MASK */
#ifndef spi_SS0_PIN_MASK
#define spi_SS0_PIN_MASK              spi_RTS_SS0_PIN_MASK
#endif /* spi_SS0_PIN_MASK */

#endif /* (CY_SCB_PINS_spi_H) */


/* [] END OF FILE */
