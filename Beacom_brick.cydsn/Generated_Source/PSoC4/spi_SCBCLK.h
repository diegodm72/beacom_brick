/*******************************************************************************
* File Name: spi_SCBCLK.h
* Version 2.20
*
*  Description:
*   Provides the function and constant definitions for the clock component.
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_CLOCK_spi_SCBCLK_H)
#define CY_CLOCK_spi_SCBCLK_H

#include <cytypes.h>
#include <cyfitter.h>


/***************************************
*        Function Prototypes
***************************************/
#if defined CYREG_PERI_DIV_CMD

void spi_SCBCLK_StartEx(uint32 alignClkDiv);
#define spi_SCBCLK_Start() \
    spi_SCBCLK_StartEx(spi_SCBCLK__PA_DIV_ID)

#else

void spi_SCBCLK_Start(void);

#endif/* CYREG_PERI_DIV_CMD */

void spi_SCBCLK_Stop(void);

void spi_SCBCLK_SetFractionalDividerRegister(uint16 clkDivider, uint8 clkFractional);

uint16 spi_SCBCLK_GetDividerRegister(void);
uint8  spi_SCBCLK_GetFractionalDividerRegister(void);

#define spi_SCBCLK_Enable()                         spi_SCBCLK_Start()
#define spi_SCBCLK_Disable()                        spi_SCBCLK_Stop()
#define spi_SCBCLK_SetDividerRegister(clkDivider, reset)  \
    spi_SCBCLK_SetFractionalDividerRegister((clkDivider), 0u)
#define spi_SCBCLK_SetDivider(clkDivider)           spi_SCBCLK_SetDividerRegister((clkDivider), 1u)
#define spi_SCBCLK_SetDividerValue(clkDivider)      spi_SCBCLK_SetDividerRegister((clkDivider) - 1u, 1u)


/***************************************
*             Registers
***************************************/
#if defined CYREG_PERI_DIV_CMD

#define spi_SCBCLK_DIV_ID     spi_SCBCLK__DIV_ID

#define spi_SCBCLK_CMD_REG    (*(reg32 *)CYREG_PERI_DIV_CMD)
#define spi_SCBCLK_CTRL_REG   (*(reg32 *)spi_SCBCLK__CTRL_REGISTER)
#define spi_SCBCLK_DIV_REG    (*(reg32 *)spi_SCBCLK__DIV_REGISTER)

#define spi_SCBCLK_CMD_DIV_SHIFT          (0u)
#define spi_SCBCLK_CMD_PA_DIV_SHIFT       (8u)
#define spi_SCBCLK_CMD_DISABLE_SHIFT      (30u)
#define spi_SCBCLK_CMD_ENABLE_SHIFT       (31u)

#define spi_SCBCLK_CMD_DISABLE_MASK       ((uint32)((uint32)1u << spi_SCBCLK_CMD_DISABLE_SHIFT))
#define spi_SCBCLK_CMD_ENABLE_MASK        ((uint32)((uint32)1u << spi_SCBCLK_CMD_ENABLE_SHIFT))

#define spi_SCBCLK_DIV_FRAC_MASK  (0x000000F8u)
#define spi_SCBCLK_DIV_FRAC_SHIFT (3u)
#define spi_SCBCLK_DIV_INT_MASK   (0xFFFFFF00u)
#define spi_SCBCLK_DIV_INT_SHIFT  (8u)

#else 

#define spi_SCBCLK_DIV_REG        (*(reg32 *)spi_SCBCLK__REGISTER)
#define spi_SCBCLK_ENABLE_REG     spi_SCBCLK_DIV_REG
#define spi_SCBCLK_DIV_FRAC_MASK  spi_SCBCLK__FRAC_MASK
#define spi_SCBCLK_DIV_FRAC_SHIFT (16u)
#define spi_SCBCLK_DIV_INT_MASK   spi_SCBCLK__DIVIDER_MASK
#define spi_SCBCLK_DIV_INT_SHIFT  (0u)

#endif/* CYREG_PERI_DIV_CMD */

#endif /* !defined(CY_CLOCK_spi_SCBCLK_H) */

/* [] END OF FILE */
