/*******************************************************************************
* File Name: Vt.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Vt_ALIASES_H) /* Pins Vt_ALIASES_H */
#define CY_PINS_Vt_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define Vt_0			(Vt__0__PC)
#define Vt_0_PS		(Vt__0__PS)
#define Vt_0_PC		(Vt__0__PC)
#define Vt_0_DR		(Vt__0__DR)
#define Vt_0_SHIFT	(Vt__0__SHIFT)
#define Vt_0_INTR	((uint16)((uint16)0x0003u << (Vt__0__SHIFT*2u)))

#define Vt_INTR_ALL	 ((uint16)(Vt_0_INTR))


#endif /* End Pins Vt_ALIASES_H */


/* [] END OF FILE */
