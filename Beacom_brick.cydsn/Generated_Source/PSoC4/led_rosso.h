/*******************************************************************************
* File Name: led_rosso.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_led_rosso_H) /* Pins led_rosso_H */
#define CY_PINS_led_rosso_H

#include "cytypes.h"
#include "cyfitter.h"
#include "led_rosso_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} led_rosso_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   led_rosso_Read(void);
void    led_rosso_Write(uint8 value);
uint8   led_rosso_ReadDataReg(void);
#if defined(led_rosso__PC) || (CY_PSOC4_4200L) 
    void    led_rosso_SetDriveMode(uint8 mode);
#endif
void    led_rosso_SetInterruptMode(uint16 position, uint16 mode);
uint8   led_rosso_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void led_rosso_Sleep(void); 
void led_rosso_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(led_rosso__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define led_rosso_DRIVE_MODE_BITS        (3)
    #define led_rosso_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - led_rosso_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the led_rosso_SetDriveMode() function.
         *  @{
         */
        #define led_rosso_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define led_rosso_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define led_rosso_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define led_rosso_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define led_rosso_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define led_rosso_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define led_rosso_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define led_rosso_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define led_rosso_MASK               led_rosso__MASK
#define led_rosso_SHIFT              led_rosso__SHIFT
#define led_rosso_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in led_rosso_SetInterruptMode() function.
     *  @{
     */
        #define led_rosso_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define led_rosso_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define led_rosso_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define led_rosso_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(led_rosso__SIO)
    #define led_rosso_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(led_rosso__PC) && (CY_PSOC4_4200L)
    #define led_rosso_USBIO_ENABLE               ((uint32)0x80000000u)
    #define led_rosso_USBIO_DISABLE              ((uint32)(~led_rosso_USBIO_ENABLE))
    #define led_rosso_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define led_rosso_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define led_rosso_USBIO_ENTER_SLEEP          ((uint32)((1u << led_rosso_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << led_rosso_USBIO_SUSPEND_DEL_SHIFT)))
    #define led_rosso_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << led_rosso_USBIO_SUSPEND_SHIFT)))
    #define led_rosso_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << led_rosso_USBIO_SUSPEND_DEL_SHIFT)))
    #define led_rosso_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(led_rosso__PC)
    /* Port Configuration */
    #define led_rosso_PC                 (* (reg32 *) led_rosso__PC)
#endif
/* Pin State */
#define led_rosso_PS                     (* (reg32 *) led_rosso__PS)
/* Data Register */
#define led_rosso_DR                     (* (reg32 *) led_rosso__DR)
/* Input Buffer Disable Override */
#define led_rosso_INP_DIS                (* (reg32 *) led_rosso__PC2)

/* Interrupt configuration Registers */
#define led_rosso_INTCFG                 (* (reg32 *) led_rosso__INTCFG)
#define led_rosso_INTSTAT                (* (reg32 *) led_rosso__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define led_rosso_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(led_rosso__SIO)
    #define led_rosso_SIO_REG            (* (reg32 *) led_rosso__SIO)
#endif /* (led_rosso__SIO_CFG) */

/* USBIO registers */
#if !defined(led_rosso__PC) && (CY_PSOC4_4200L)
    #define led_rosso_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define led_rosso_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define led_rosso_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define led_rosso_DRIVE_MODE_SHIFT       (0x00u)
#define led_rosso_DRIVE_MODE_MASK        (0x07u << led_rosso_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins led_rosso_H */


/* [] END OF FILE */
