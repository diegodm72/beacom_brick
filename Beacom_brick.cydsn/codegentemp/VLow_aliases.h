/*******************************************************************************
* File Name: VLow.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_VLow_ALIASES_H) /* Pins VLow_ALIASES_H */
#define CY_PINS_VLow_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define VLow_0			(VLow__0__PC)
#define VLow_0_PS		(VLow__0__PS)
#define VLow_0_PC		(VLow__0__PC)
#define VLow_0_DR		(VLow__0__DR)
#define VLow_0_SHIFT	(VLow__0__SHIFT)
#define VLow_0_INTR	((uint16)((uint16)0x0003u << (VLow__0__SHIFT*2u)))

#define VLow_INTR_ALL	 ((uint16)(VLow_0_INTR))


#endif /* End Pins VLow_ALIASES_H */


/* [] END OF FILE */
