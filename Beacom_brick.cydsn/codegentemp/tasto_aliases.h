/*******************************************************************************
* File Name: tasto.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_tasto_ALIASES_H) /* Pins tasto_ALIASES_H */
#define CY_PINS_tasto_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define tasto_0			(tasto__0__PC)
#define tasto_0_PS		(tasto__0__PS)
#define tasto_0_PC		(tasto__0__PC)
#define tasto_0_DR		(tasto__0__DR)
#define tasto_0_SHIFT	(tasto__0__SHIFT)
#define tasto_0_INTR	((uint16)((uint16)0x0003u << (tasto__0__SHIFT*2u)))

#define tasto_INTR_ALL	 ((uint16)(tasto_0_INTR))


#endif /* End Pins tasto_ALIASES_H */


/* [] END OF FILE */
