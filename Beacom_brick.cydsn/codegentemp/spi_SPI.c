/***************************************************************************//**
* \file spi_SPI.c
* \version 4.0
*
* \brief
*  This file provides the source code to the API for the SCB Component in
*  SPI mode.
*
* Note:
*
*******************************************************************************
* \copyright
* Copyright 2013-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "spi_PVT.h"
#include "spi_SPI_UART_PVT.h"

#if(spi_SCB_MODE_UNCONFIG_CONST_CFG)

    /***************************************
    *  Configuration Structure Initialization
    ***************************************/

    const spi_SPI_INIT_STRUCT spi_configSpi =
    {
        spi_SPI_MODE,
        spi_SPI_SUB_MODE,
        spi_SPI_CLOCK_MODE,
        spi_SPI_OVS_FACTOR,
        spi_SPI_MEDIAN_FILTER_ENABLE,
        spi_SPI_LATE_MISO_SAMPLE_ENABLE,
        spi_SPI_WAKE_ENABLE,
        spi_SPI_RX_DATA_BITS_NUM,
        spi_SPI_TX_DATA_BITS_NUM,
        spi_SPI_BITS_ORDER,
        spi_SPI_TRANSFER_SEPARATION,
        0u,
        NULL,
        0u,
        NULL,
        (uint32) spi_SCB_IRQ_INTERNAL,
        spi_SPI_INTR_RX_MASK,
        spi_SPI_RX_TRIGGER_LEVEL,
        spi_SPI_INTR_TX_MASK,
        spi_SPI_TX_TRIGGER_LEVEL,
        (uint8) spi_SPI_BYTE_MODE_ENABLE,
        (uint8) spi_SPI_FREE_RUN_SCLK_ENABLE,
        (uint8) spi_SPI_SS_POLARITY
    };


    /*******************************************************************************
    * Function Name: spi_SpiInit
    ****************************************************************************//**
    *
    *  Configures the spi for SPI operation.
    *
    *  This function is intended specifically to be used when the spi 
    *  configuration is set to “Unconfigured spi” in the customizer. 
    *  After initializing the spi in SPI mode using this function, 
    *  the component can be enabled using the spi_Start() or 
    * spi_Enable() function.
    *  This function uses a pointer to a structure that provides the configuration 
    *  settings. This structure contains the same information that would otherwise 
    *  be provided by the customizer settings.
    *
    *  \param config: pointer to a structure that contains the following list of 
    *   fields. These fields match the selections available in the customizer. 
    *   Refer to the customizer for further description of the settings.
    *
    *******************************************************************************/
    void spi_SpiInit(const spi_SPI_INIT_STRUCT *config)
    {
        if(NULL == config)
        {
            CYASSERT(0u != 0u); /* Halt execution due to bad function parameter */
        }
        else
        {
            /* Configure pins */
            spi_SetPins(spi_SCB_MODE_SPI, config->mode, spi_DUMMY_PARAM);

            /* Store internal configuration */
            spi_scbMode       = (uint8) spi_SCB_MODE_SPI;
            spi_scbEnableWake = (uint8) config->enableWake;
            spi_scbEnableIntr = (uint8) config->enableInterrupt;

            /* Set RX direction internal variables */
            spi_rxBuffer      =         config->rxBuffer;
            spi_rxDataBits    = (uint8) config->rxDataBits;
            spi_rxBufferSize  =         config->rxBufferSize;

            /* Set TX direction internal variables */
            spi_txBuffer      =         config->txBuffer;
            spi_txDataBits    = (uint8) config->txDataBits;
            spi_txBufferSize  =         config->txBufferSize;

            /* Configure SPI interface */
            spi_CTRL_REG     = spi_GET_CTRL_OVS(config->oversample)           |
                                            spi_GET_CTRL_BYTE_MODE(config->enableByteMode) |
                                            spi_GET_CTRL_EC_AM_MODE(config->enableWake)    |
                                            spi_CTRL_SPI;

            spi_SPI_CTRL_REG = spi_GET_SPI_CTRL_CONTINUOUS    (config->transferSeperation)  |
                                            spi_GET_SPI_CTRL_SELECT_PRECEDE(config->submode &
                                                                          spi_SPI_MODE_TI_PRECEDES_MASK) |
                                            spi_GET_SPI_CTRL_SCLK_MODE     (config->sclkMode)            |
                                            spi_GET_SPI_CTRL_LATE_MISO_SAMPLE(config->enableLateSampling)|
                                            spi_GET_SPI_CTRL_SCLK_CONTINUOUS(config->enableFreeRunSclk)  |
                                            spi_GET_SPI_CTRL_SSEL_POLARITY (config->polaritySs)          |
                                            spi_GET_SPI_CTRL_SUB_MODE      (config->submode)             |
                                            spi_GET_SPI_CTRL_MASTER_MODE   (config->mode);

            /* Configure RX direction */
            spi_RX_CTRL_REG     =  spi_GET_RX_CTRL_DATA_WIDTH(config->rxDataBits)         |
                                                spi_GET_RX_CTRL_BIT_ORDER (config->bitOrder)           |
                                                spi_GET_RX_CTRL_MEDIAN    (config->enableMedianFilter) |
                                                spi_SPI_RX_CTRL;

            spi_RX_FIFO_CTRL_REG = spi_GET_RX_FIFO_CTRL_TRIGGER_LEVEL(config->rxTriggerLevel);

            /* Configure TX direction */
            spi_TX_CTRL_REG      = spi_GET_TX_CTRL_DATA_WIDTH(config->txDataBits) |
                                                spi_GET_TX_CTRL_BIT_ORDER (config->bitOrder)   |
                                                spi_SPI_TX_CTRL;

            spi_TX_FIFO_CTRL_REG = spi_GET_TX_FIFO_CTRL_TRIGGER_LEVEL(config->txTriggerLevel);

            /* Configure interrupt with SPI handler but do not enable it */
            CyIntDisable    (spi_ISR_NUMBER);
            CyIntSetPriority(spi_ISR_NUMBER, spi_ISR_PRIORITY);
            (void) CyIntSetVector(spi_ISR_NUMBER, &spi_SPI_UART_ISR);

            /* Configure interrupt sources */
            spi_INTR_I2C_EC_MASK_REG = spi_NO_INTR_SOURCES;
            spi_INTR_SPI_EC_MASK_REG = spi_NO_INTR_SOURCES;
            spi_INTR_SLAVE_MASK_REG  = spi_GET_SPI_INTR_SLAVE_MASK(config->rxInterruptMask);
            spi_INTR_MASTER_MASK_REG = spi_GET_SPI_INTR_MASTER_MASK(config->txInterruptMask);
            spi_INTR_RX_MASK_REG     = spi_GET_SPI_INTR_RX_MASK(config->rxInterruptMask);
            spi_INTR_TX_MASK_REG     = spi_GET_SPI_INTR_TX_MASK(config->txInterruptMask);
            
            /* Configure TX interrupt sources to restore. */
            spi_IntrTxMask = LO16(spi_INTR_TX_MASK_REG);

            /* Set active SS0 */
            spi_SpiSetActiveSlaveSelect(spi_SPI_SLAVE_SELECT0);

            /* Clear RX buffer indexes */
            spi_rxBufferHead     = 0u;
            spi_rxBufferTail     = 0u;
            spi_rxBufferOverflow = 0u;

            /* Clear TX buffer indexes */
            spi_txBufferHead = 0u;
            spi_txBufferTail = 0u;
        }
    }

#else

    /*******************************************************************************
    * Function Name: spi_SpiInit
    ****************************************************************************//**
    *
    *  Configures the SCB for the SPI operation.
    *
    *******************************************************************************/
    void spi_SpiInit(void)
    {
        /* Configure SPI interface */
        spi_CTRL_REG     = spi_SPI_DEFAULT_CTRL;
        spi_SPI_CTRL_REG = spi_SPI_DEFAULT_SPI_CTRL;

        /* Configure TX and RX direction */
        spi_RX_CTRL_REG      = spi_SPI_DEFAULT_RX_CTRL;
        spi_RX_FIFO_CTRL_REG = spi_SPI_DEFAULT_RX_FIFO_CTRL;

        /* Configure TX and RX direction */
        spi_TX_CTRL_REG      = spi_SPI_DEFAULT_TX_CTRL;
        spi_TX_FIFO_CTRL_REG = spi_SPI_DEFAULT_TX_FIFO_CTRL;

        /* Configure interrupt with SPI handler but do not enable it */
    #if(spi_SCB_IRQ_INTERNAL)
            CyIntDisable    (spi_ISR_NUMBER);
            CyIntSetPriority(spi_ISR_NUMBER, spi_ISR_PRIORITY);
            (void) CyIntSetVector(spi_ISR_NUMBER, &spi_SPI_UART_ISR);
    #endif /* (spi_SCB_IRQ_INTERNAL) */

        /* Configure interrupt sources */
        spi_INTR_I2C_EC_MASK_REG = spi_SPI_DEFAULT_INTR_I2C_EC_MASK;
        spi_INTR_SPI_EC_MASK_REG = spi_SPI_DEFAULT_INTR_SPI_EC_MASK;
        spi_INTR_SLAVE_MASK_REG  = spi_SPI_DEFAULT_INTR_SLAVE_MASK;
        spi_INTR_MASTER_MASK_REG = spi_SPI_DEFAULT_INTR_MASTER_MASK;
        spi_INTR_RX_MASK_REG     = spi_SPI_DEFAULT_INTR_RX_MASK;
        spi_INTR_TX_MASK_REG     = spi_SPI_DEFAULT_INTR_TX_MASK;

        /* Configure TX interrupt sources to restore. */
        spi_IntrTxMask = LO16(spi_INTR_TX_MASK_REG);
            
        /* Set active SS0 for master */
    #if (spi_SPI_MASTER_CONST)
        spi_SpiSetActiveSlaveSelect(spi_SPI_SLAVE_SELECT0);
    #endif /* (spi_SPI_MASTER_CONST) */

    #if(spi_INTERNAL_RX_SW_BUFFER_CONST)
        spi_rxBufferHead     = 0u;
        spi_rxBufferTail     = 0u;
        spi_rxBufferOverflow = 0u;
    #endif /* (spi_INTERNAL_RX_SW_BUFFER_CONST) */

    #if(spi_INTERNAL_TX_SW_BUFFER_CONST)
        spi_txBufferHead = 0u;
        spi_txBufferTail = 0u;
    #endif /* (spi_INTERNAL_TX_SW_BUFFER_CONST) */
    }
#endif /* (spi_SCB_MODE_UNCONFIG_CONST_CFG) */


/*******************************************************************************
* Function Name: spi_SpiPostEnable
****************************************************************************//**
*
*  Restores HSIOM settings for the SPI master output pins (SCLK and/or SS0-SS3) 
*  to be controlled by the SCB SPI.
*
*******************************************************************************/
void spi_SpiPostEnable(void)
{
#if(spi_SCB_MODE_UNCONFIG_CONST_CFG)

    if (spi_CHECK_SPI_MASTER)
    {
    #if (spi_CTS_SCLK_PIN)
        /* Set SCB SPI to drive output pin */
        spi_SET_HSIOM_SEL(spi_CTS_SCLK_HSIOM_REG, spi_CTS_SCLK_HSIOM_MASK,
                                       spi_CTS_SCLK_HSIOM_POS, spi_CTS_SCLK_HSIOM_SEL_SPI);
    #endif /* (spi_CTS_SCLK_PIN) */

    #if (spi_RTS_SS0_PIN)
        /* Set SCB SPI to drive output pin */
        spi_SET_HSIOM_SEL(spi_RTS_SS0_HSIOM_REG, spi_RTS_SS0_HSIOM_MASK,
                                       spi_RTS_SS0_HSIOM_POS, spi_RTS_SS0_HSIOM_SEL_SPI);
    #endif /* (spi_RTS_SS0_PIN) */

    #if (spi_SS1_PIN)
        /* Set SCB SPI to drive output pin */
        spi_SET_HSIOM_SEL(spi_SS1_HSIOM_REG, spi_SS1_HSIOM_MASK,
                                       spi_SS1_HSIOM_POS, spi_SS1_HSIOM_SEL_SPI);
    #endif /* (spi_SS1_PIN) */

    #if (spi_SS2_PIN)
        /* Set SCB SPI to drive output pin */
        spi_SET_HSIOM_SEL(spi_SS2_HSIOM_REG, spi_SS2_HSIOM_MASK,
                                       spi_SS2_HSIOM_POS, spi_SS2_HSIOM_SEL_SPI);
    #endif /* (spi_SS2_PIN) */

    #if (spi_SS3_PIN)
        /* Set SCB SPI to drive output pin */
        spi_SET_HSIOM_SEL(spi_SS3_HSIOM_REG, spi_SS3_HSIOM_MASK,
                                       spi_SS3_HSIOM_POS, spi_SS3_HSIOM_SEL_SPI);
    #endif /* (spi_SS3_PIN) */
    }

#else

    #if (spi_SPI_MASTER_SCLK_PIN)
        /* Set SCB SPI to drive output pin */
        spi_SET_HSIOM_SEL(spi_SCLK_M_HSIOM_REG, spi_SCLK_M_HSIOM_MASK,
                                       spi_SCLK_M_HSIOM_POS, spi_SCLK_M_HSIOM_SEL_SPI);
    #endif /* (spi_MISO_SDA_TX_PIN_PIN) */

    #if (spi_SPI_MASTER_SS0_PIN)
        /* Set SCB SPI to drive output pin */
        spi_SET_HSIOM_SEL(spi_SS0_M_HSIOM_REG, spi_SS0_M_HSIOM_MASK,
                                       spi_SS0_M_HSIOM_POS, spi_SS0_M_HSIOM_SEL_SPI);
    #endif /* (spi_SPI_MASTER_SS0_PIN) */

    #if (spi_SPI_MASTER_SS1_PIN)
        /* Set SCB SPI to drive output pin */
        spi_SET_HSIOM_SEL(spi_SS1_M_HSIOM_REG, spi_SS1_M_HSIOM_MASK,
                                       spi_SS1_M_HSIOM_POS, spi_SS1_M_HSIOM_SEL_SPI);
    #endif /* (spi_SPI_MASTER_SS1_PIN) */

    #if (spi_SPI_MASTER_SS2_PIN)
        /* Set SCB SPI to drive output pin */
        spi_SET_HSIOM_SEL(spi_SS2_M_HSIOM_REG, spi_SS2_M_HSIOM_MASK,
                                       spi_SS2_M_HSIOM_POS, spi_SS2_M_HSIOM_SEL_SPI);
    #endif /* (spi_SPI_MASTER_SS2_PIN) */

    #if (spi_SPI_MASTER_SS3_PIN)
        /* Set SCB SPI to drive output pin */
        spi_SET_HSIOM_SEL(spi_SS3_M_HSIOM_REG, spi_SS3_M_HSIOM_MASK,
                                       spi_SS3_M_HSIOM_POS, spi_SS3_M_HSIOM_SEL_SPI);
    #endif /* (spi_SPI_MASTER_SS3_PIN) */

#endif /* (spi_SCB_MODE_UNCONFIG_CONST_CFG) */

    /* Restore TX interrupt sources. */
    spi_SetTxInterruptMode(spi_IntrTxMask);
}


/*******************************************************************************
* Function Name: spi_SpiStop
****************************************************************************//**
*
*  Changes the HSIOM settings for the SPI master output pins 
*  (SCLK and/or SS0-SS3) to keep them inactive after the block is disabled. 
*  The output pins are controlled by the GPIO data register.
*
*******************************************************************************/
void spi_SpiStop(void)
{
#if(spi_SCB_MODE_UNCONFIG_CONST_CFG)

    if (spi_CHECK_SPI_MASTER)
    {
    #if (spi_CTS_SCLK_PIN)
        /* Set output pin state after block is disabled */
        spi_uart_cts_spi_sclk_Write(spi_GET_SPI_SCLK_INACTIVE);

        /* Set GPIO to drive output pin */
        spi_SET_HSIOM_SEL(spi_CTS_SCLK_HSIOM_REG, spi_CTS_SCLK_HSIOM_MASK,
                                       spi_CTS_SCLK_HSIOM_POS, spi_CTS_SCLK_HSIOM_SEL_GPIO);
    #endif /* (spi_uart_cts_spi_sclk_PIN) */

    #if (spi_RTS_SS0_PIN)
        /* Set output pin state after block is disabled */
        spi_uart_rts_spi_ss0_Write(spi_GET_SPI_SS0_INACTIVE);

        /* Set GPIO to drive output pin */
        spi_SET_HSIOM_SEL(spi_RTS_SS0_HSIOM_REG, spi_RTS_SS0_HSIOM_MASK,
                                       spi_RTS_SS0_HSIOM_POS, spi_RTS_SS0_HSIOM_SEL_GPIO);
    #endif /* (spi_uart_rts_spi_ss0_PIN) */

    #if (spi_SS1_PIN)
        /* Set output pin state after block is disabled */
        spi_spi_ss1_Write(spi_GET_SPI_SS1_INACTIVE);

        /* Set GPIO to drive output pin */
        spi_SET_HSIOM_SEL(spi_SS1_HSIOM_REG, spi_SS1_HSIOM_MASK,
                                       spi_SS1_HSIOM_POS, spi_SS1_HSIOM_SEL_GPIO);
    #endif /* (spi_SS1_PIN) */

    #if (spi_SS2_PIN)
        /* Set output pin state after block is disabled */
        spi_spi_ss2_Write(spi_GET_SPI_SS2_INACTIVE);

        /* Set GPIO to drive output pin */
        spi_SET_HSIOM_SEL(spi_SS2_HSIOM_REG, spi_SS2_HSIOM_MASK,
                                       spi_SS2_HSIOM_POS, spi_SS2_HSIOM_SEL_GPIO);
    #endif /* (spi_SS2_PIN) */

    #if (spi_SS3_PIN)
        /* Set output pin state after block is disabled */
        spi_spi_ss3_Write(spi_GET_SPI_SS3_INACTIVE);

        /* Set GPIO to drive output pin */
        spi_SET_HSIOM_SEL(spi_SS3_HSIOM_REG, spi_SS3_HSIOM_MASK,
                                       spi_SS3_HSIOM_POS, spi_SS3_HSIOM_SEL_GPIO);
    #endif /* (spi_SS3_PIN) */
    
        /* Store TX interrupt sources (exclude level triggered) for master. */
        spi_IntrTxMask = LO16(spi_GetTxInterruptMode() & spi_INTR_SPIM_TX_RESTORE);
    }
    else
    {
        /* Store TX interrupt sources (exclude level triggered) for slave. */
        spi_IntrTxMask = LO16(spi_GetTxInterruptMode() & spi_INTR_SPIS_TX_RESTORE);
    }

#else

#if (spi_SPI_MASTER_SCLK_PIN)
    /* Set output pin state after block is disabled */
    spi_sclk_m_Write(spi_GET_SPI_SCLK_INACTIVE);

    /* Set GPIO to drive output pin */
    spi_SET_HSIOM_SEL(spi_SCLK_M_HSIOM_REG, spi_SCLK_M_HSIOM_MASK,
                                   spi_SCLK_M_HSIOM_POS, spi_SCLK_M_HSIOM_SEL_GPIO);
#endif /* (spi_MISO_SDA_TX_PIN_PIN) */

#if (spi_SPI_MASTER_SS0_PIN)
    /* Set output pin state after block is disabled */
    spi_ss0_m_Write(spi_GET_SPI_SS0_INACTIVE);

    /* Set GPIO to drive output pin */
    spi_SET_HSIOM_SEL(spi_SS0_M_HSIOM_REG, spi_SS0_M_HSIOM_MASK,
                                   spi_SS0_M_HSIOM_POS, spi_SS0_M_HSIOM_SEL_GPIO);
#endif /* (spi_SPI_MASTER_SS0_PIN) */

#if (spi_SPI_MASTER_SS1_PIN)
    /* Set output pin state after block is disabled */
    spi_ss1_m_Write(spi_GET_SPI_SS1_INACTIVE);

    /* Set GPIO to drive output pin */
    spi_SET_HSIOM_SEL(spi_SS1_M_HSIOM_REG, spi_SS1_M_HSIOM_MASK,
                                   spi_SS1_M_HSIOM_POS, spi_SS1_M_HSIOM_SEL_GPIO);
#endif /* (spi_SPI_MASTER_SS1_PIN) */

#if (spi_SPI_MASTER_SS2_PIN)
    /* Set output pin state after block is disabled */
    spi_ss2_m_Write(spi_GET_SPI_SS2_INACTIVE);

    /* Set GPIO to drive output pin */
    spi_SET_HSIOM_SEL(spi_SS2_M_HSIOM_REG, spi_SS2_M_HSIOM_MASK,
                                   spi_SS2_M_HSIOM_POS, spi_SS2_M_HSIOM_SEL_GPIO);
#endif /* (spi_SPI_MASTER_SS2_PIN) */

#if (spi_SPI_MASTER_SS3_PIN)
    /* Set output pin state after block is disabled */
    spi_ss3_m_Write(spi_GET_SPI_SS3_INACTIVE);

    /* Set GPIO to drive output pin */
    spi_SET_HSIOM_SEL(spi_SS3_M_HSIOM_REG, spi_SS3_M_HSIOM_MASK,
                                   spi_SS3_M_HSIOM_POS, spi_SS3_M_HSIOM_SEL_GPIO);
#endif /* (spi_SPI_MASTER_SS3_PIN) */

    #if (spi_SPI_MASTER_CONST)
        /* Store TX interrupt sources (exclude level triggered). */
        spi_IntrTxMask = LO16(spi_GetTxInterruptMode() & spi_INTR_SPIM_TX_RESTORE);
    #else
        /* Store TX interrupt sources (exclude level triggered). */
        spi_IntrTxMask = LO16(spi_GetTxInterruptMode() & spi_INTR_SPIS_TX_RESTORE);
    #endif /* (spi_SPI_MASTER_CONST) */

#endif /* (spi_SCB_MODE_UNCONFIG_CONST_CFG) */
}


#if (spi_SPI_MASTER_CONST)
    /*******************************************************************************
    * Function Name: spi_SetActiveSlaveSelect
    ****************************************************************************//**
    *
    *  Selects one of the four slave select lines to be active during the transfer.
    *  After initialization the active slave select line is 0.
    *  The component should be in one of the following states to change the active
    *  slave select signal source correctly:
    *   - The component is disabled.
    *   - The component has completed transfer (TX FIFO is empty and the
    *     SCB_INTR_MASTER_SPI_DONE status is set).
    *
    *  This function does not check that these conditions are met.
    *  This function is only applicable to SPI Master mode of operation.
    *
    *  \param slaveSelect: slave select line which will be active while the following
    *   transfer.
    *   - spi_SPI_SLAVE_SELECT0 - Slave select 0.
    *   - spi_SPI_SLAVE_SELECT1 - Slave select 1.
    *   - spi_SPI_SLAVE_SELECT2 - Slave select 2.
    *   - spi_SPI_SLAVE_SELECT3 - Slave select 3.
    *
    *******************************************************************************/
    void spi_SpiSetActiveSlaveSelect(uint32 slaveSelect)
    {
        uint32 spiCtrl;

        spiCtrl = spi_SPI_CTRL_REG;

        spiCtrl &= (uint32) ~spi_SPI_CTRL_SLAVE_SELECT_MASK;
        spiCtrl |= (uint32)  spi_GET_SPI_CTRL_SS(slaveSelect);

        spi_SPI_CTRL_REG = spiCtrl;
    }
#endif /* (spi_SPI_MASTER_CONST) */


#if !(spi_CY_SCBIP_V0 || spi_CY_SCBIP_V1)
    /*******************************************************************************
    * Function Name: spi_SpiSetSlaveSelectPolarity
    ****************************************************************************//**
    *
    *  Sets active polarity for slave select line.
    *  The component should be in one of the following states to change the active
    *  slave select signal source correctly:
    *   - The component is disabled.
    *   - The component has completed transfer.
    *  
    *  This function does not check that these conditions are met.
    *
    *  \param slaveSelect: slave select line to change active polarity.
    *   - spi_SPI_SLAVE_SELECT0 - Slave select 0.
    *   - spi_SPI_SLAVE_SELECT1 - Slave select 1.
    *   - spi_SPI_SLAVE_SELECT2 - Slave select 2.
    *   - spi_SPI_SLAVE_SELECT3 - Slave select 3.
    *
    *  \param polarity: active polarity of slave select line.
    *   - spi_SPI_SS_ACTIVE_LOW  - Slave select is active low.
    *   - spi_SPI_SS_ACTIVE_HIGH - Slave select is active high.
    *
    *******************************************************************************/
    void spi_SpiSetSlaveSelectPolarity(uint32 slaveSelect, uint32 polarity)
    {
        uint32 ssPolarity;

        /* Get position of the polarity bit associated with slave select line */
        ssPolarity = spi_GET_SPI_CTRL_SSEL_POLARITY((uint32) 1u << slaveSelect);

        if (0u != polarity)
        {
            spi_SPI_CTRL_REG |= (uint32)  ssPolarity;
        }
        else
        {
            spi_SPI_CTRL_REG &= (uint32) ~ssPolarity;
        }
    }
#endif /* !(spi_CY_SCBIP_V0 || spi_CY_SCBIP_V1) */


#if(spi_SPI_WAKE_ENABLE_CONST)
    /*******************************************************************************
    * Function Name: spi_SpiSaveConfig
    ****************************************************************************//**
    *
    *  Clears INTR_SPI_EC.WAKE_UP and enables it. This interrupt
    *  source triggers when the master assigns the SS line and wakes up the device.
    *
    *******************************************************************************/
    void spi_SpiSaveConfig(void)
    {
        /* Clear and enable SPI wakeup interrupt source */
        spi_ClearSpiExtClkInterruptSource(spi_INTR_SPI_EC_WAKE_UP);
        spi_SetSpiExtClkInterruptMode(spi_INTR_SPI_EC_WAKE_UP);
    }


    /*******************************************************************************
    * Function Name: spi_SpiRestoreConfig
    ****************************************************************************//**
    *
    *  Disables the INTR_SPI_EC.WAKE_UP interrupt source. After wakeup
    *  slave does not drive the MISO line and the master receives 0xFF.
    *
    *******************************************************************************/
    void spi_SpiRestoreConfig(void)
    {
        /* Disable SPI wakeup interrupt source */
        spi_SetSpiExtClkInterruptMode(spi_NO_INTR_SOURCES);
    }
#endif /* (spi_SPI_WAKE_ENABLE_CONST) */


/* [] END OF FILE */
