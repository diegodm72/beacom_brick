# 2 Progetti ( ***ble beacom e ble normale*** )

## Brick Ble Beacom 1.0.0
---

Misura della temperatura corporea e invio dati attraverso il Ble Beacom, da testare con l'applicazione CySmart Beacom

Possibilita :

* Invio tramite Beacom Google (  Eddystone )
* Invio tramite iBeacom  ( Apple )

Attualmente invia con Eddystone ( UUID + TLM )

* Identificativo
* Dati telemetrici

---

## Ble temperature

Misura della temperatura e utilizza i servizi con caratteristiche, da testare con l'applicazione CySmart

---
Link utili

* [Wiky](https://en.wikipedia.org/wiki/Bluetooth_low_energy_beacon)
* [iBeacon](https://support.kontakt.io/hc/en-gb/articles/201492492-iBeacon-advertising-packet-structure)
* [BLE Caratteristiche](https://www.bluetooth.com/specifications/gatt/characteristics/)
* [Caratteristica per la temperatura](https://www.bluetooth.com/xml-viewer/?src=https://www.bluetooth.com/wp-content/uploads/Sitecore-Media-Library/Gatt/Xml/Characteristics/org.bluetooth.characteristic.temperature_measurement.xml)

---

Apunti

Packet Structure Byte Map **iBeacom**:

* **Byte 0-2: Standard BLE Flags**
* Byte 0: Length :  0x02
* Byte 1: Type: 0x01 (Flags)
* Byte 2: Value: 0x06 (Typical Flags)
* **Byte 3-29: Apple Defined iBeacon Data**
* Byte 3: Length: 0x1a
* Byte 4: Type: 0xff (Custom Manufacturer Packet)
* Byte 5-6: Manufacturer ID : 0x4c00 (Apple)
* Byte 7: SubType: 0x02 (iBeacon)
* Byte 8: SubType Length: 0x15
* Byte 9-24: Proximity UUID
* Byte 25-26: Major
* Byte 27-28: Minor
* Byte 29: Signal Power

---

Lavoro

* 4 ore 11-06-20
* 4 ore 12-06-20
* 6 ore 25-06-20 ( aggiunto l'accelerometro e la eeprom )

